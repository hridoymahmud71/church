document.addEventListener("DOMContentLoaded", function () {
    $register_btn = $("#register-btn");
    $bookingform_modal = $("#bookingform-modal");
    $event_booking_form = $("#event-booking-form");

    if ($bookingform_modal.attr("preload-modal") == "yes") {
        $bookingform_modal.show();
    }

    $register_btn.on("click", function (e) {
        $bookingform_modal.show();
    });

})
//------------------------------------------------------------------
var map;

initMap();

function initMap() {
    $event_map = document.getElementById('event-map');
    event_lat = parseFloat($event_map.getAttribute("evt-lat"));
    event_lng = parseFloat($event_map.getAttribute("evt-lng"));
    event_zoom = parseInt($event_map.getAttribute("evt-zoom"));
    event_title = $event_map.getAttribute("evt-title");

    lat_lang = {lat: event_lat, lng: event_lng};

    console.log(event_lat);
    console.log(event_lng);
    console.log(event_zoom);
    map = new google.maps.Map($event_map, {
        center: lat_lang,
        zoom: event_zoom
    });

    var infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
        position: lat_lang,
        map: map,
        title: event_title,

        animation: google.maps.Animation.DROP,

    });
    show_info_window();
    function show_info_window(){
        infowindow.setContent('<div><strong>' + event_title + '</strong><br>');
        infowindow.open(map, marker);
    }


    marker.addListener('click', toggleBounce,show_info_window);
    marker.addListener('mouseover', toggleBounce,show_info_window);

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }




}

//------------------------------------------------------------------