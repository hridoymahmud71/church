// delete gallery
$(document).on('click', '.btn_delete_gallery_item', function(){         
         var id=$(this).data("delete_gallery_item");
         var x=$(this);
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-gallery-image",  
             method:"post",  
             data:{image_key:id},
             dataType:"text",  
             success:function(data){  
                      x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 


// edit gallery
  $(document).on('click','.edit_gallery_item' ,function(){
        var gallery_id   = $(this).attr('image-key'); 
       $.ajax({
            url:'admin/render_edit_gallery',
            method:'POST',
            data:{id:gallery_id},

            success:function(data){
                var hhh = JSON.parse(data);
                $('#edit_gallery_div').html(hhh.edit_gallery_div);
            }
        })
    });


 // add gallery form validation
 $(document).ready(function(){      
      $("#addGallery").validate({
        rules: {    
          image_description: {
            required: true
          },
          gallery_category_id: {
            required: true
          },
          image_order: {
            required: true,  
            number: true,
          },
          is_featured_image: {
            required: true
          },
        }
    });
  })

   $('#addGallery').on('submit', function(){
     $("#addGallery").valid();
   });



// gallery image upload by dropzone
  Dropzone.autoDiscover = false;
  $( document ).ready(function() { 
    var photo_upload = new Dropzone(".dropzone", {
        url: "AdminGalleryController/multi",
        maxFilesize: 20,
        maxFiles: 1,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').
                        append(
                            "<input type='hidden' name='image' value='" + obj + "'>\n\
                           <input type='hidden' name='width' value='" + file.width + "'>\n\
                           <input type='hidden' name='height' value='" + file.height + "'>"
                                );

            });
        } 
    });
  });




  // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#galleryImage').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-gallery-image-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });
