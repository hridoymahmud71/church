// edit form validation
$(document).ready(function(){      
        $("#editGallery").validate({
          rules: {    
            image_description: {
              required: true
            },
            gallery_category_id: {
              required: true
            },
            image_order: {
              required: true,  
              number: true,
            },
            is_featured_image: {
              required: true
            },
          }
      });
    })


     $('#editGallery').on('submit', function(){
       $("#editGallery").valid();
     });



    // image upload script by dropzone
     Dropzone.autoDiscover = false;
          $( document ).ready(function() { 
            var photo_upload2 = new Dropzone("#edit_dropzone", {
                url: "AdminGalleryController/multi",
                maxFilesize: 20,
                maxFiles: 1,
                method: "post",
                acceptedFiles: ".jpg,.jpeg,.png",
                paramName: "userfile",
                dictInvalidFileType: "Type file ini tidak dizinkan",
                addRemoveLinks: true,
                init: function () {
                    thisDropzone = this;
                    this.on("success", function (file, json) {
                        var obj = json;
                        $('.previews2').
                                append(
                                    "<input type='hidden' name='image2' value='" + obj + "'>\n\ "
                                        );

                    });
                } 
            });
          });