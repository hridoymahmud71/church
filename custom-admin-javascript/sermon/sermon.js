  // sermon video
  $(function(){
     $('#sermonVideoGlobal').on('click',function(){ 
      $('#sermonVideoUploadDiv').hide();
      $('#sermonVideoUrlDiv').show();
      
    });
  });   
  
 $(function(){
      $('#sermonVideoLocal').on('click',function(){ 
      $('#sermonVideoUrlDiv').hide();
      $('#sermonVideoUploadDiv').show();
       
      });
  }); 


 // sermon audio
 $(function(){
     $('#sermonAudioGlobal').on('click',function(){ 
      $('#sermonAudioUploadDiv').hide();
      $('#sermonAudioUrlDiv').show();
      
    });
  });   
  
 $(function(){
      $('#sermonAudioLocal').on('click',function(){ 
      $('#sermonAudioUrlDiv').hide();
      $('#sermonAudioUploadDiv').show();
       
      });
  }); 


 // edit sermon
 $('.edit_sermon').on('click', function(){
        var sermon_key   = $(this).closest('tr').attr('id');
       $.ajax({
            url:'admin/render_edit_sermon',
            method:'POST',
            data:{sermon_key:sermon_key},

            success:function(data){
               var result = JSON.parse(data);
               $('#edit_sermon_div').html(result.edit_sermon_div);
            }
        })
    });



  // add sermon form validation
 $(document).ready(function(){      
      $("#addSermon").validate();
  })

   $('#addSermon').on('submit', function(){
     $("#addSermon").valid();
   });



   // delete staff
$(document).on('click', '.btn_delete_sermon', function(){      
         var id = $(this).data("delete_sermon");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-sermon",  
             method:"post",  
             data:{sermon_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        });