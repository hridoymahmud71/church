// edit form validation
  $(document).ready(function(){      
      $("#editUser").validate({
        rules: {    
          user_username: {
            required: true
          }, 
          user_email: {
            required: true,
            email: true,
          },
                 
        }
    });
  })
   $('#editUser').on('submit', function(){
     $("#editUser").valid();
   });



   // image upload script by dropzone
     Dropzone.autoDiscover = false;
          $( document ).ready(function() { 
            var photo_upload2 = new Dropzone("#edit_dropzone", {
                url: "AdminUserController/saveImage",
                maxFilesize: 20,
                maxFiles: 1,
                method: "post",
                acceptedFiles: ".jpg,.jpeg,.png",
                paramName: "userfile",
                dictInvalidFileType: "Type file ini tidak dizinkan",
                addRemoveLinks: true,
                init: function () {
                    thisDropzone = this;
                    this.on("success", function (file, json) {
                        var obj = json;
                        $('.previews2').
                                append(
                                    "<input type='hidden' name='image2' value='" + obj + "'>\n\ "
                                        );

                    });
                } 
            });
          });


