


   // dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#blogCommentTable').DataTable({
          "ordering": false
      });
  });


   // edit blog
 $(document).on('click','.view_comment', function(){
        var blog_comment_key   = $(this).attr('blog_comment_key');
       $.ajax({
            url:'admin/render_edit_blog_comment',
            method:'POST',
            data:{blog_comment_key:blog_comment_key},
            success:function(data){
               var result = JSON.parse(data);
               $('#edit_comment_div').html(result.edit_comment_div);
            }
        })
    });


 // delete Blog
$(document).on('click', '.btn_delete_blog_comment', function(){         
         var id=$(this).data("delete_comment");
         var x=$(this); 
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-blog-comment",  
             method:"post",  
             data:{blog_comment_key:id},
             dataType:"text",  
             success:function(data){
                       x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 




//mass select
   jQuery('#master').on('click', function(e) {
    if($(this).is(':checked',true))  
    {
      $(".sub_chk").prop('checked', true);  
    }  
    else  
    {  
      $(".sub_chk").prop('checked',false);  
    }  
  });


   //mass delete
    $(document).on('click', '.delete_all', function(){         
         var allVals = [];  
          $(".sub_chk:checked").each(function() {  
              allVals.push($(this).attr('data-id'));
            });   
            if(allVals.length ==0) 
            {  
              alert("Please select atleast one row.");  
            }  
             else {  
      WRN_PROFILE_DELETE = "Are you sure you want to delete all the selected row?";  
      var check = confirm(WRN_PROFILE_DELETE);  
      if(check == true){  
              //for server side        
              var join_selected_values = allVals.join(",");        
              $.ajax({ 
                type: "POST",  
                url: "admin/mass-blog-delete",  
                cache:false,  
                data: 'ids='+join_selected_values,  
                success: function(response)  
                {  
                  location.reload();
                }   
              });       

            }  
          }
      }); 