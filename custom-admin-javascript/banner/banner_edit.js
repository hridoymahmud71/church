// multiple banner button
//Clone the hidden element and shows it
  $( document ).ready(function() {
      attach_delete();
  });

  $('.add-one2').click(function(){
    $('.dynamic-element2').first().clone().appendTo('.dynamic-anchor2').show();
    attach_delete();
  });

  //Attach functionality to delete buttons
  function attach_delete(){
    $('.delete').off();
    $('.delete').click(function(){
      console.log("click");
      $(this).closest('.form-group2').remove();
    });
  }



  // banner edit form validation
  $(document).ready(function(){      
      $("#editSlideshow").validate({
        rules: {    
          banner_image_title: {
            required: true
          }, 
          banner_image_subtitle: {
            required: true
          },
          banner_order: {
            required: true
          },         
        }
    });
  })
   $('#editSlideshow').on('submit', function(){
     $("#editSlideshow").valid();
   });


   // edit form dropzone
    Dropzone.autoDiscover = false;
      $( document ).ready(function() { 
        var photo_upload2 = new Dropzone("#edit_dropzone", {
            url: "AdminBannerController/saveImage",
            maxFilesize: 20,
            maxFiles: 1,
            method: "post",
            acceptedFiles: ".jpg,.jpeg,.png",
            paramName: "userfile",
            dictInvalidFileType: "Type file ini tidak dizinkan",
            addRemoveLinks: true,
            init: function () {
                thisDropzone = this;
                this.on("success", function (file, json) {
                    var obj = json;
                    $('.previews2').
                            append(
                                "<input type='hidden' name='image2' value='" + obj + "'>\n\
                               <input type='hidden' name='width2' value='" + file.width + "'>\n\
                               <input type='hidden' name='height2' value='" + file.height + "'>"
                                    );

                });
            } 
        });
      });