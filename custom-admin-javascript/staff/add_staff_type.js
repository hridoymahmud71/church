  // add form validation
  $(document).ready(function(){      
      $("#addStaffType").validate({
        rules: {    
          role_name: {
            required: true
          }, 
                
        }
    });
  })
   $('#addStaffType').on('submit', function(){
     $("#addStaffType").valid();
   });


// delete staff
$(document).on('click', '.btn_delete_staff_type', function(){      
         var staff_type_key=$(this).data("delete_staff_type");
         var x=$(this);         
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"admin/delete-staff-type",  
             method:"post",  
             data:{staff_type_key:staff_type_key},
             dataType:"text",  
             success:function(data){
                      x.closest('tr').fadeOut(); 
                    }  
            });  
          }  
        }); 



// dataTable    
  document.addEventListener('DOMContentLoaded', function () { 
    var table;
      table = $('#staff_type_table').DataTable({

          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          //order: [], //Initial no order.
          // Load data for the table's content from an Ajax source
          ajax: {
              url: "admin/load-staff-type-table",
              type: "post",
              complete: function (res) {}
          },
          //Set column definition initialisation properties.
          columnDefs: [
              {
                  "targets": [-1], //last column
                  "orderable": false, //set not orderable
              },
          ],
          "ordering": false

      });
  });