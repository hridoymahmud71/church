<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminAuthController extends CI_Controller {
	

	public function __construct()
	{

		parent::__construct();
		$this->load->model('AdminAuthModel');
		$this->load->library('form_validation');  
	}

	
	
	public function login()
	{	
		$loggedUserId = $this->session->userdata('id');
		if(isset($loggedUserId))redirect('admin/dashboard');	
		$this->load->view('admin_views/auth/login');

	}


	//////////////////////
	//admin site login //
	/////////////////////
	public function check_login()
	{	
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');    
		$this->form_validation->set_rules('password', 'Password', 'required'); 

		if($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error_msg', 'The login was unsucessful');
			redirect('admin/login');
		}
		else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);           
            $clean['password']=md5($clean['password']);
            $userInfo = $this->AdminAuthModel->checkLogin($clean);
            if ($userInfo) {
                	foreach ($userInfo as $key => $val) {
                    $this->session->set_userdata($key, $val);
                }

                redirect('admin/dashboard');
            } else {
                $this->session->set_flashdata('error_msg', 'Incorrect Email or Password!');
                redirect('admin/login');
            }
        }
	}	

	

	public function logout()
	{   
		$this->session->sess_destroy();
		redirect('admin/login');
	}


    ///////////////////////
	// Forgot Passowrd ///
	/////////////////////
	public function forgot_password()
	{
		$this->load->view('admin_views/auth/forgot_password');	

	}
	
	public function sendResetPassEmail()
    {    
    	//validate and check input email
    	$email = $this->input->post('email') ? $this->input->post('email') : '';

    	//if email not exists in db redirect with error msg
    	if(!$this->emailExist($email)){
    		$this->session->set_flashdata('error_msg', 'This email not recorded');
    		redirect('admin/forgot_password');
    	}    
    	//unique authentication code for reset pass
    	$authCode = md5(uniqid(mt_rand(), true));
    	$resetLink =  $this->config->item('admin_site_url')."passwordresetform?authCode={$authCode}&email={$email}";
    	
    			//valid email exist in DB
    		$email = htmlspecialchars($email);
    		$user  = $this->AdminAuthModel->infoByEmail($email);	
    		
    		//save the authCode against the requested email
    		if(!empty($user)){
    		    $this->AdminAuthModel->saveAuthCode($authCode, $email);
				$Mail_Data['email']     = $email;
				$Mail_Data['username']  = $user['staff_name'];
				$Mail_Data['resetLink'] = $resetLink;
				$this->User_Pass_Reset($Mail_Data);
    		}
    		$this->session->set_flashdata('success_msg', 'Password reset link sent to your email');
    		redirect('admin/login');
    	
    }
	
	public function emailExist($email)
    {
    	return $this->AdminAuthModel->emailCheck($email);	//false->not exist //true->exist	
    }
	
	public function validateInputEmail($email)
    {
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
    	if($this->form_validation->run() == true){
    		return true;
    	}else{
    		return false;
    	}
    }
	
	 /* Data process for user password reset */
    
    public function User_Pass_Reset($Mail_Data)
    {  

    	$mail_data['to']            =  $Mail_Data['email'];
    	$username                   =  $Mail_Data['username'];
    	$template = $this->AdminAuthModel->getEmailTempltateById('2');
    	if ($template) {
    		$subject                = $template->email_template_subject;
    		$template_message       = $template->email_template;
    		$actual_link            = $Mail_Data['resetLink'];
    		$find                   = array( "{{actual_link}}","{{username}}");
    		$replace                = array($actual_link,$username);
    		$message                = str_replace($find, $replace, $template_message);
    		$mail_data['subject']   = $subject;
    		$mail_data['message']   = $message;
    		$this->sendEmail($mail_data);
    		
    	}    
    }
	
	 /* Send Email Function */
    
    public function sendEmail($mail_data)
    {   
    	$mailTo        =   $mail_data['to'];
    	$mailSubject   =   $mail_data['subject'];
    	$message       =   $mail_data['message'];
    	$this->load->library('email');
    	$this->email->set_mailtype('html');
    	$this->email->from("info@church.com", "Chirst Church");
    	$this->email->to($mailTo);
    	$this->email->subject($mailSubject);
    	$this->email->message($message);
    	$this->email->send();
    
    }
	
	public function passwordResetForm()
    {		
    	//if already loggedIn redirect to home page
    	if( isset($this->session->userdata['id']) ) redirect('admin/login');    
    	$arr['authCode'] = $_GET['authCode'];
    	$arr['email'] 	 = $_GET['email'];
    	$this->load->view('admin_views/auth/password_reset',$arr);
    }
	
	 public function updatePassword()
    {
    	$email = $_POST['email'];
    	$password = md5($_POST['new_pass']);
    	$authCode = $_POST['authCode'];
    	$arr['email'] =  $email;
    	$arr['authCode'] = $authCode;
    
    	$this->form_validation->set_rules('new_pass', 'New Password', 'required|min_length[6]');
    	$this->form_validation->set_rules('email', 'email', 'required|valid_email');
    	
    		//if form validation fails return to form again with error message
    	if ($this->form_validation->run() == FALSE)
    	{
    		$this->session->set_flashdata('error_msg', 'Something May Wrong! Please try again.');
    		$this->load->view('admin_views/auth/password_reset', $arr);    
    
    	}else{    
    		$user = $this->AdminAuthModel->infoByEmail($email);
    		$update_data = array(
    			'authcode' => '',
    			'password' => $password
    			);
    			//if user exists and authCode matched then reset password & authcode
    		if( $user && strlen($authCode)>1 ){
    			$this->db->where('email', $email);
    			$this->db->update('staff', $update_data) ;
    			$this->session->set_flashdata('success_msg', 'You have successfully changed your password.');
    			redirect('admin/login');
    		}else{
    			$this->session->set_flashdata('error_msg', 'Your password doesn\'t changed !! Try another reset link');
    			redirect('admin/login');
    		}
    	}  
    
    }
}
