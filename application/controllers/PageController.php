<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
       
        $this->load->model('Page_model');
    }

    public function pages($slug)
    {   
        $data = array();
        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $content_params['slug'] = $slug;
        $data['content'] = $this->page_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function page_content($content_params)
    {
        $data = array();
        $data['page_contents'] = $this->Page_model->getPageContent($content_params['slug']);
        if(empty($data['page_contents']) ){
            redirect('404_override');
        }
        $content = $this->template->render('contents/page/page', $data, true);
        return $content;
    }


   
}