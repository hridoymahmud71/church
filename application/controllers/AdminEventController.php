<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminEventController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Event_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view Event
    // *********************
    public function viewEvent()
    { 
        $permission = $this->permission->hasAccess(array('view_event'));
        if($permission['view_event']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Events';
            $data['active_link']        = 'event';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);                        
            //$data['view_event']     = $this->Event_model->getAllData('event');            
            $view_event         = $this->Event_model->getAllData('event');  

            $i=0;
            foreach ($view_event as $event) {                
                $event_id = $event['event_id'];                
                $feature_image = $this->Event_model->viewEventFeatureImage($event_id);
                $view_event[$i++]['feature_image'] = $feature_image;
            }
            $data['view_event'] = $view_event;           
            $data['content']        = $this->load->view('admin_views/event/event', $data, true);
            $dataJS['custom_js']    = array('event/event');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $permission = $this->permission->hasAccess(array('add_event'));
            if($permission['add_event']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $this->form_validation->set_rules('event_name', 'Event Title', 'required');   
            $this->form_validation->set_rules('event_location', 'Event Location', 'required'); 
            $this->form_validation->set_rules('event_start_date', 'Event Start', 'required'); 

            if($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_msg', 'Fill all required fields');
              redirect('admin/event');
            }
            $post  = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $key    = $this->Important_model->generate_key('event', 'event_key', 'evt');
           
            $data['event_key']                  = $key; 
            $data['event_name']                 = $clean['event_name'];
            $data['event_description']          = $clean['event_description'];           
            $data['event_location']             = $clean['event_location'];  

            $eventStart = strtotime($clean['event_start_date']); 
            $event_start = date("Y-m-d H:i:s", $eventStart);
            $data['event_starts']               = $event_start;

            if(!empty($clean['event_end_date'])){
                $eventEnd = strtotime($clean['event_end_date']); 
                $event_end = date("Y-m-d H:i:s", $eventEnd);
                $data['event_ends']             = $event_end;
            }
            
            $data['event_created_at']           = date('Y-m-d H:i:s');
            $data['is_event_active']            = 1;
            $data['is_event_featured']          = $clean['is_event_featured'];
            $data['event_order']                = 0;
            $data['event_phone']                = $clean['event_phone'];
            $data['event_phone']                = $clean['event_phone'];
            $data['event_website']              = $clean['event_website'];
            $data['event_email']                = $clean['event_email'];
            $data['event_seat']                 = $clean['event_seat'];
            $data['is_event_booking_enabled']   = $clean['is_event_booking_enable'];
            $data['event_google_map_latitude']  = $clean['latitude'];
            $data['event_google_map_longitude'] = $clean['longitude'];
            $data['event_google_map_location']  = $clean['location'];
            // echo '<pre>'; print_r($data); die;
            $result = $this->Event_model->insert('event', $data);

            ////////////////////////
            
             if(isset($clean['image'])){
                $image = $clean['image'];
                // echo "<pre>"; print_r($image); die;
                for($j=0;$j<count($image);$j++){
                     $data3['event_image_name']=$image[$j];
                    if($j==0){
                        $data3['is_event_image_featured']=1;
                    }
                    else{
                        $data3['is_event_image_featured']=0;
                    }
                    $data3['event_id']                  = $result; 
                    $data3['event_image_created_at']    = date('Y-m-d H:i:s');
                    $result3 = $this->Event_model->insert('event_image', $data3);
                }
             }

            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/event');
            }
        }
    }



//**************************
// image upload by dropzone
// *************************
public function saveImage() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('event_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }



    //*******************************
    // render Staff for edit
    // ******************************
    public function renderEditEvent()
    {
        $event_key = $this->input->post('id');  
        $data['event_info']   =   $this->Event_model->get_event_info($event_key);               
        $id = $data['event_info'][0]['event_id'];
        $data['view_event_image']    = $this->Event_model->getEventImage($id);
        $json = array();
        $json['event_info']            = $data['event_info'];
        $json['view_event_image']      = $data['view_event_image'];
        $json['edit_event_div'] = $this->load->view('admin_views/event/edit_event_div', $data, TRUE);
        echo json_encode($json);
    }



    //**********************
    // update Event
    // *********************
    public function updateEvent()
    { 
        $permission = $this->permission->hasAccess(array('edit_event'));
        if($permission['edit_event']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }    
        
        $this->form_validation->set_rules('event_name', 'Event Title', 'required');   
        $this->form_validation->set_rules('event_location', 'Event Location', 'required'); 
        $this->form_validation->set_rules('event_start_date', 'Event Start', 'required'); 

        if($this->form_validation->run() == FALSE) {
          $this->session->set_flashdata('error_msg', 'Fill all required fields');
          redirect('admin/event');
        }

        else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post); 
            // echo "<pre>"; print_r($clean); die;
             ////////////////////////             
             if(isset($clean['image2'])){ 
                $image  = $clean['image2']; 
                 if(!empty($image)){
                    $result2 = $this->Event_model->deletePrevEventImage($clean['event_id']);
                    for($j=0;$j<count($image);$j++){
                         $data3['event_image_name']=$image[$j];
                        if($clean['event_feature_image']==$data3['event_image_name']){
                            $data3['is_event_image_featured']=1;
                        }
                        else{
                            $data3['is_event_image_featured']=0;
                        }
                        $data3['event_id']                  = $clean['event_id']; 
                        $data3['event_image_created_at']    = date('Y-m-d H:i:s');

                        $result3 = $this->Event_model->insert('event_image', $data3);
                    }
                 } 
            }          

           
            $data['event_name']                 = $clean['event_name'];
            $data['event_description']          = $clean['event_description'];           
            $data['event_location']             = $clean['event_location'];  

            $eventStart = strtotime($clean['event_start_date']); 
            $event_start = date("Y-m-d H:i:s", $eventStart);
            $data['event_starts']               = $event_start;

            if(!empty($clean['event_end_date'])){
                $eventEnd = strtotime($clean['event_end_date']); 
                $event_end = date("Y-m-d H:i:s", $eventEnd);
                $data['event_ends']             = $event_end;
            }
            
            $data['event_updated_at']           = date('Y-m-d H:i:s');
            $data['is_event_active']            = 1;
            $data['is_event_featured']          = $clean['is_event_featured'];
            $data['event_order']                = 0;
            $data['event_phone']                = $clean['event_phone'];
            $data['event_website']              = $clean['event_website'];
            $data['event_email']                = $clean['event_email'];
            $data['event_seat']                 = $clean['event_seat'];
            $data['is_event_booking_enabled']   = $clean['is_event_booking_enable'];
            $data['event_google_map_latitude']  = $clean['latitude'];
            $data['event_google_map_longitude'] = $clean['longitude'];
            $data['event_google_map_location']  = $clean['location']; 
            $data['event_id']                   = $clean['event_id']; 
            // echo "<pre>"; print_r($data); die;
            $result = $this->Event_model->updateEvent($data);            
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/event');
            }

        }
    }


    //**********************
    // delete Event Image
    // *********************
    public function deleteEventImage()
    {
        $id = $this->input->post('event_image_id'); 
        $result = $this->Event_model->deleteEventImage($id);
        if (count($result) > 0) {                    
            echo "Deleted Successfully";
        }
    }


    //**********************
    // delete Event All Info
    // *********************
    public function deleteEvent()
    {
        $permission = $this->permission->hasAccess(array('delete_event'));
        if($permission['delete_event']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $event_key = $this->input->post('event_key');
        $get_event_info = $this->Event_model->getEventInfo($event_key);       
        $event_id = $get_event_info[0]['event_id'];
        $get_event_image = $this->Event_model->eventImage($event_id);
        if(!empty($get_event_image)){
           for($i=0; $i<count($get_event_image); $i++){
            $image_name = $get_event_image[$i]['event_image_name'];  
            $event_image_id = $get_event_image[$i]['event_image_id'];  

            $path = $this->config->item('event_source_path').'/'.$image_name;
                if(!empty($image_name) && file_exists($path)){
                    unlink($path);
                }
            $result = $this->Event_model->deleteEventImage($event_image_id); 
           } 
        }
        $result2 = $this->Event_model->deleteEvent($event_key);
         if (count($result2) > 0) {                    
            echo "Deleted Successfully";
        }        
    }

    public function viewBookingForEvent($event_key){
        $permission = $this->permission->hasAccess(array('view_booking'));
        if($permission['view_booking']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $result = $this->Event_model->get_event_info($event_key);
        if(empty($result)){
            $this->session->set_flashdata('error_msg', 'Something May Wrong!!');
            redirect('admin/event');

        }
        $event_id = $result[0]['event_id'];

        $data['page_title']         = 'View Event Booking';
        $data['active_link']        = 'view_event_booking';
        $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
        $data['header']             = $this->load->view('admin_views/templates/header', '', true);
        $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
        $data['view_booking'] = $this->Event_model->get_booking_for_event($event_id);
        $data['content']        = $this->load->view('admin_views/event/view_event_booking', $data, true);
        $dataJS['custom_js']    = array('event/view_event_booking');
        $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
        $this->load->view('admin_views/index', $data);
        
    }

    //**********************
    // Mass Event Delete
    // *********************
    // *********************
    public function massEventDelete()
    {
        $permission = $this->permission->hasAccess(array('delete_event'));
        if($permission['delete_event']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $ids = $this->input->post('ids'); 
        $all_id = explode(",",$ids);
        $result = $this->Event_model->deleteMassEvent($all_id);
        if($result){
            $this->session->set_flashdata('success_msg', 'Deleted Successfully');
            echo "Delete Successfully";
        }
    }




    // Event Booking Part --------------------------------------------------------------
    
    //**********************
    // view event Booking
    //*********************

  public function viewEventBooking()
    {       
            $permission = $this->permission->hasAccess(array('view_booking'));
            if($permission['view_booking']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            }  

            $data['page_title']     = 'Event Booking';
            $data['active_link']    = 'event_booking';
            $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']         = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true); 
            $data['view_booking']   = $this->Event_model->getEventBooking();

            $data['content']        = $this->load->view('admin_views/event/event_booking', $data, true);
            $dataJS['custom_js']    = array('event/event_booking');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
       
    }

    //*******************************
    // render View Booking Info
    // ******************************
    public function renderViewBooking()
    {
        $booking_key = $this->input->post('booking_key');
        $data['booking_info']   =   $this->Event_model->get_booking_info($booking_key);
        $json = array();
        $json['booking_info']            = $data['booking_info'];
        $json['view_booking_div'] = $this->load->view('admin_views/event/view_booking_div', $data, TRUE);
        echo json_encode($json);
    }

    //*******************************
    // Approve/Unapproved Booking Status
    // ******************************
    public function updateBookingStatus()
    {
        $post = $this->input->post();
        $booking_key          = $post['event_booking_key'];
        $event_booking_status = $post['event_booking_status'];

        if($event_booking_status==0){
             $result2     = $this->Event_model->unapprove_booking($booking_key);
             if($result2){
                 $this->session->set_flashdata('success_msg', 'Booking status update successfully');
                redirect('admin/event-booking');
             }
        }

        $booking_info       = $this->Event_model->get_booking_info($booking_key);
        $event_id           = $booking_info[0]['event_id'];
        $requested_seat     = $booking_info[0]['event_booking_seat'];

        $event_info         = $this->Event_model->get_eventInfo($event_id);
        $total_event_seat   = $event_info[0]['event_seat'];
        $total_seat_booking     = $this->Event_model->total_seat_booking($event_id);

        $total_available_seat = $total_event_seat - $total_seat_booking;

        if($requested_seat>$total_available_seat){
          $this->session->set_flashdata('error_msg', 'Booking not approved!! Seat not available');
          redirect('admin/event-booking');
        }
        
        $result     = $this->Event_model->approve_booking($booking_key);
        if($result){
            $this->session->set_flashdata('success_msg', 'Booking status update successfully');
            redirect('admin/event-booking');
        }
    }


    //**********************
    // delete Event Booking
    // *********************
    public function deleteEventBooking()
    {
        $booking_key = $this->input->post('booking_key'); 
        $result = $this->Event_model->deleteEventBooking($booking_key);
        if (count($result) > 0) {                    
            echo "Deleted Successfully";
        }
    }



     //************************
    // load Event table by ajax
    //************************
    public function loadEventTable()
    {
        $list = $this->Event_model->get_datatables('event');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_event','delete_event','view_booking'));
        foreach ($list as $event) {
            $no++;  
            $row = array();
            $row[]="<input type='checkbox' class='sub_chk' data-id='{$event["event_key"]}'>";
            $row[] = $no;  
            $row[] = $event['event_name'];          
            $row[] = $event['event_seat']; 

            $event_id = $event['event_id'];
            $feature_image = $this->Event_model->viewEventFeatureImage($event_id);
            if(!empty($feature_image)){
                $image_name = $feature_image[0]['event_image_name'];
            }
            if(isset($image_name)){
                $row[] = "<img src=".$this->config->item('event_source_path').$image_name." style='height: 40px; width: 40px'>";
            }
            if(!isset($image_name)){ $row[] = "";}  

            $role['action']="";
            if($permission['edit_event']==1){
            $role['action'].="<a event-key='{$event["event_key"]}' data-toggle='modal' class='edit_event' data-target='#exampleModal2' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['delete_event']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_event='{$event["event_key"]}' style='margin-right:10px; font-size: 16px;' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_event'></i></a>";
            }
            if($permission['view_booking']==1){
            $role['action'].="<a href='admin/view-event-booking/{$event["event_key"]}'><i style='font-size: 16px' data-toggle='tooltip' title='View Booking' class='fa fa-ticket'></i></a>";
            }
            
           

            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Event_model->count_all('event'),
            "recordsFiltered" => $this->Event_model->count_filtered('event'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }


     //************************
    // load Event Booking table by ajax
    //************************
    public function loadEventBookingTable()
    {
        $list = $this->Event_model->get_datatables('event_booking');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('view_booking','delete_booking'));
        foreach ($list as $booking) {
            $no++;  
            $row = array();
            $row[] = $no;  
            $row[] = $booking['event_booking_user_name'];          
            $row[] = $booking['event_booking_seat']; 
            $row[] = $booking['event_name']; 

            if($booking['event_booking_status']==0){
                $row[] = '<span class="btn btn-danger"> Unapproved </span>';
            }
            if($booking['event_booking_status']==1){
                $row[] = '<span class="btn btn-success"> Approved </span>';
            }
            
            $role['action']="";
            if($permission['view_booking']==1){
            $role['action'].="<a event-booking-key='{$booking["event_booking_key"]}' data-toggle='modal' class='view_event_booking_item' data-target='#exampleModal2' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-eye'></i></a>";
            }
            
            if($permission['delete_booking']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_booking='{$booking["event_booking_key"]}' style='margin-right:10px; font-size: 16px;' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_booking'></i></a>";
            }

            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Event_model->count_all('event_booking'),
            "recordsFiltered" => $this->Event_model->count_filtered('event_booking'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }









}
