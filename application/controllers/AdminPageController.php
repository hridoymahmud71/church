<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminPageController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Page_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
        
    }

    //**********************
    // view page
    // *********************
    public function viewPage()
    {         
        if ($this->input->server('REQUEST_METHOD') != 'POST') {
            $data['page_title']         = 'Pages';
            $data['active_link']        = 'pages';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);           
            $data['view_page']      = $this->Page_model->getAllData('page'); 
            $data['content']        = $this->load->view('admin_views/page/page', $data, true);
            $dataJS['custom_js']    = array('page/page');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);  
        }
        else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            // echo "<pre>"; print_r($clean); die;
            $this->form_validation->set_rules('page_title', 'Page Title', 'required');
            $this->form_validation->set_rules('page_slug', 'Page Slug', 'required');
            if($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_msg', 'Fill all required fields');
              redirect('admin/page');
            }
            $key    = $this->Important_model->generate_key('page', 'page_key', 'pg');
            //page header image upload code start
            if($_FILES){ 
                $image_name = $_FILES['page_header_image']['name'];
                $config['encrypt_name']  = TRUE;
                $config['upload_path']   = $this->config->item('page_image_upload_path');
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('page_header_image')) {
                    $status = 'error';
                    $data2['msg'] = $this->upload->display_errors('', '');
                } else {
                    $fileName=$this->upload->data();
                    $data['page_header_image']=$fileName['file_name'];
                } 
            }

            $slug = $clean['page_slug'];
            $checkSlug = $this->Page_model->checkSlug($slug);
            if(!empty($checkSlug)){
                 $this->session->set_flashdata('error_msg', 'This page slug already exist!! try with different slug.');
                redirect('admin/page');
            }
            $data['page_key']            = $key;
            $data['page_title']          = $clean['page_title'];
            $data['page_slug']           = $clean['page_slug'];
            $data['page_content']        = $clean['page_content'];
            $data['page_status']         = 1;
            $data['page_created_at']     = date('Y-m-d H:i:s');            
            $result = $this->Page_model->insert('page', $data);
            if($result){
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/page');
            }

        }      
    }

    //*******************
    // render edit blog
    // ******************
    public function renderEditPage()
    {
        $page_key = $this->input->post('page_key');
        $data['page_info']   =   $this->Page_model->getPageInfo($page_key);
        $json = array();
        $json['page_info']          = $data['page_info'];
        $json['edit_page_div']      = $this->load->view('admin_views/page/edit_page_div', $data, TRUE);
        echo json_encode($json);
    }


     //**********************
    // delete Page
    // *********************
    public function deletePage()
    {       
        $page_key  = $this->input->post('page_key');
        $page_info = $this->Page_model->getPageInfo($page_key);   
        if(empty($page_info)){
            echo "Something May Wrong"; die;
        }    
        $page_id   = $page_info[0]['page_id'];
        
        $image_name = $page_info[0]['page_header_image'];
        $result = $this->Page_model->deletePage($page_key);
        if (count($result) > 0) {
             $path = $this->config->item('page_image_upload_path').'/'.$image_name;
                if(!empty($image_name) && file_exists($path)){
                    unlink($path);
                }                  
            echo "Deleted Successfully";
        }
    }


    //******************
    // update page
    // *****************
    public function updatePage()
    {       
        $this->form_validation->set_rules('page_title', 'Page Title', 'required');
        $this->form_validation->set_rules('page_slug', 'Page Slug', 'required');
        if($this->form_validation->run() == FALSE) {
          $this->session->set_flashdata('error_msg', 'Fill all required fields');
          redirect('admin/page');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

             if($_FILES){ 
                $image_name = $_FILES['page_header_image']['name'];
                $config['encrypt_name']  = TRUE;
                $config['upload_path']   = $this->config->item('page_image_upload_path');
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('page_header_image')) {
                    $status = 'error';
                    $data2['msg'] = $this->upload->display_errors('', '');
                } else {
                    $fileName=$this->upload->data();
                    $data['page_header_image']=$fileName['file_name'];
                } 
            }

            $page_id                     = $clean['page_id'];
            $previousPageInfo            = $this->Page_model->getPrevPageInfo($page_id);
            if(empty($previousPageInfo)){
                $this->session->set_flashdata('error_msg', 'Something May Wrong');
                redirect('admin/page');
            }

            $prevSlugName  = $previousPageInfo[0]['page_slug'];
            $current_slug_name = $clean['page_slug'];

            if($prevSlugName != $current_slug_name){
                $checkSlug = $this->Page_model->checkSlug($current_slug_name);
                if(!empty($checkSlug)){
                     $this->session->set_flashdata('error_msg', 'This page slug already exist!! try with different slug.');
                    redirect('admin/page');
                }
            }

            $data['page_title']          = $clean['page_title'];
            $data['page_slug']           = $clean['page_slug'];
            $data['page_content']        = $clean['page_content'];
            $data['page_status']         = $clean['page_status'];
            $data['page_updated_at']     = date('Y-m-d H:i:s');  

            $result = $this->Page_model->updatePage($data, $page_id);            
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/page');
            }

        }
    }

    
   

}
