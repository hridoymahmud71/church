<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StaffController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model("staff_model");
    }

    public function getStaff($staff_key)
    {
        $data = array();

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $content_params = array();
        $content_params['staff_key'] = $staff_key;
        $data['content'] = $this->staff_content($content_params);

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }


    public function staff_content($content_params)
    {
        $data = array();

        $data['staff'] = $this->staff_model->getStaffByKey($content_params['staff_key']);

        if(!empty($data['staff'])){
            $data['title'] = $data['staff']["staff_name"];
        }

        /*echo "<pre>";
        print_r($data['staff']);
        echo "</pre>";
        echo"<br>";
        echo "<code>";
        echo json_encode($data['staff']);
        echo "</code>";

        exit;*/

        $content = $this->template->render('contents/staff/single_staff_page', $data, true);

        return $content;
    }

    public function getStaffs()
    {
        $data = array();

        $data['title'] = "Staffs";

        $data['meta'] = $this->template->render('segments/meta', array(), true);
        $data['headlink'] = $this->template->render('segments/headlink', array(), true);
        $data['navbar'] = $this->template->render('segments/navbar', array(), true);
        $data['footer'] = $this->template->footer(array());
        $data['footlink'] = $this->template->render('segments/footlink', array(), true);

        $data['content'] = $this->staff_list_content(array());

        $view = $this->load->view('master', $data, true);
        echo $view;
        exit;
    }

    public function staff_list_content($content_params)
    {
        $data = array();

        $data['staffs'] = $this->staff_model->getActiveStaffs($limit = 500);

        $content = $this->template->render('contents/staff/staff_list_page', $data, true);

        return $content;
    }

}