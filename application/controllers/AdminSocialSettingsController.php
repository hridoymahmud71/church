<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSocialSettingsController extends CI_Controller
{
//https://mbahcoding.com/tutorial/php/codeigniter/codeigniter-ajax-crud-using-bootstrap-modals-and-datatable.html

    public function __construct()
    {
        parent::__construct();

        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }

    public function index()
    {
        $social_settings = $this->settings->prepared_social_settings();

        if ($this->input->post() || !empty($_FILES)) {
            $this->process_settings();
            exit;
        }

        $headerlink_data['page_title'] = 'Social Settings';
        $headerlink_data['active_link'] = 'social_settings';
        $data['headerlink'] = $this->load->view('admin_views/templates/headerlink', $headerlink_data, true);

        $data['header'] = $this->load->view('admin_views/templates/header', '', true);
        $data['sidebar'] = $this->load->view('admin_views/templates/sidebar', '', true);

        $content_data['social_settings'] = $social_settings;
        $data['content'] = $this->load->view('admin_views/settings/social_settings', $content_data, true);

        $dataJS['custom_js'] = array('settings/social_settings'); //social_settings.js
        $data['footer'] = $this->load->view('admin_views/templates/footer', $dataJS, true);

        $this->load->view('admin_views/index', $data);

    }


    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }


    public function process_settings()
    {
        $image_errors = array();
        $image_successes = array();

        /*echo "<pre>";
        print_r($this->input->post());
        print_r($_FILES);
        echo "</pre>";
        exit;*/

        if (isset($_FILES)) {
            if (!empty($_FILES)) {


                foreach ($_FILES as $file_field_name => $file_field) {

                    if ($file_field['name'] != "" && $file_field['error'] != 4) {
                        $upload = $this->upload($file_field_name, $file_field);

                        if ($upload['error']) {
                            $image_errors[] = "$file_field_name upload error:{$upload['error_message']}";
                        } else if (!$upload['error']) {
                            $image_successes[] = "$file_field_name uploaded successfully";

                            $settings_key = $file_field_name;
                            $settings_val = $upload['filename'];
                            if (!empty($settings_val)) {
                                $this->settings->setSettings($settings_type = "social", $settings_key, trim($settings_val));
                            }
                        }
                    }


                }

            }
        }

        $this->session->set_flashdata("image_errors", implode("<br>", $image_errors));
        $this->session->set_flashdata("image_successes", implode("<br>", $image_successes));


        //--------------------------------------------------------------

        $this->form_validation->set_rules('site_name', 'site_name', 'trim|max_length[50]');
        $this->form_validation->set_rules('site_description', 'site_description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('admin/social-settings');
        }


        $p_data = $this->input->post();
        if (!empty($p_data)) {
            foreach ($p_data as $settings_key => $settings_val) {

                if (!empty($settings_val)) {
                    $this->settings->setSettings($settings_type = "social", $settings_key, trim($settings_val));
                }

                if (empty($settings_val)) {
                    $this->settings->setSettings($settings_type = "social", $settings_key, null);
                }
            }
        }

        $this->session->set_flashdata('update_success', "Social settings updated");
        redirect('admin/social-settings');
    }

    public function upload($file_field_name, $file_field)
    {
        $upload = array();
        $upload['error'] = false;
        $upload['error_msg'] = "";
        $upload['filename'] = "";
        $upload['fileinfo'] = array();

        if (isset($_FILES[$file_field_name]) && !empty($_FILES[$file_field_name]['name'])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = $this->config->item('settings_upload_path');
            $config['allowed_types'] = 'jpg|png|jpeg|gif';
            $config['max_size'] = '0';

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload($file_field_name)) {
                $upload['error'] = true;
                $upload['error_msg'] = $this->upload->display_errors();
            } else {
                $upload['fileinfo'] = $this->upload->data();
                $upload['filename'] = $upload['fileinfo']['file_name'];
            }


        }

        return $upload;
    }


}