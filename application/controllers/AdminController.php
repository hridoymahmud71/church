<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
        
    }

    public function allAdmin(){
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Administrator';
            $data['active_link']        = 'administrator';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
            $data['all_admin'] = $this->Admin_model->getAllAdmin(); 
            $data['content']        = $this->load->view('admin_views/admin/all_admin', $data, true);
            $dataJS['custom_js']    = array('admin/admin');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Validation Error');
                redirect('admin/all-admin');
            }

            $key = $this->Important_model->generate_key('admin', 'admin_key', 'admin');
            
            if(isset($clean['image'])){
                $data['image']   = $clean['image'];
            }  
            $data['admin_key']      = $key;
            $data['username']       = $clean['username'];
            $data['email']          = $clean['email'];
            $data['password']       = md5($clean['password']);
            $data['role']           = 1;
            $data['created_at']     = date('Y-m-d H:i:s');

            $checkResult = $this->Admin_model->checkAdmin($data['email']);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Email already exist');
                redirect('admin/all-admin');
            }

            $checkResult2 = $this->Admin_model->checkUsername($data['username']);
            if (count($checkResult2) > 0) {
                $this->session->set_flashdata('error_msg', 'This Username already exist');
                redirect('admin/all-admin');
            }


            $result = $this->Admin_model->insert('admin', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/all-admin');
            }
        }
    }


    //**************************
// image upload by dropzone
// *************************
public function saveImage() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('admin_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }

    //////////////////


   

    public function updateAdmin()
    {   

        $this->form_validation->set_rules('email', 'Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/all-admin');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $data['username'] = $clean['username'];
            $data['email'] = $clean['email'];
            $data['id'] = $clean['id'];
            if (!empty($clean['password'])) {
                $data['password'] = md5($clean['password']);
            }

            if(isset($clean['image2'])){                
              $data['image'] =   $clean['image2'];
            }

            $admin_info = $this->Admin_model->getAdminInfo($data['id']);

            $admin_email = $admin_info[0]['email'];
            if ($admin_email != $data['email']) {
                $checkResult = $this->Admin_model->checkAdmin($data['email']);
                if (count($checkResult) > 0) {
                    $this->session->set_flashdata('error_msg', 'This email already exist!');
                    redirect('admin/all-admin');
                }
            }

            $admin_username = $admin_info[0]['username'];
            if ($admin_username != $data['username']) {
                $checkResult2 = $this->Admin_model->checkUsername($data['username']);
                if (count($checkResult2) > 0) {
                    $this->session->set_flashdata('error_msg', 'This username already exist!');
                    redirect('admin/all-admin');
                }
            }


            $result = $this->Admin_model->updateAdmin($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Succesfully');
                redirect('admin/all-admin');
            }
        }
    }


    public function deleteAdmin()
    {
        $id = $this->input->post('admin_key');
        $result = $this->Admin_model->deleteAdmin($id);
        if (count($result) > 0) {
            echo "Deleted Successfully";
        }
    }


     //*********************
    // render admin
    //********************
    public function renderEditAdmin()
    {
        $admin_id = $this->input->post('id');
        $data['adminInfo']   =   $this->Admin_model->adminInfo($admin_id);        
        $json = array();
        $json['adminInfo']   =   $data['adminInfo'];
        $json['edit_admin_div']  =   $this->load->view('admin_views/admin/edit_admin_div', $data, TRUE);
        echo json_encode($json);
    }



     //***************
    // Admin Profile
    // ***************
    public function adminProfile()
    { 
        $id = $this->session->userdata('id');

        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']     = 'Admin Profile';
            $data['active_link']    = 'admin_profile';
            $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']         = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true); 
            $data['admin_info']   = $this->Admin_model->get_admin_info($id);
            // print_r($data['admin_info']); die;
            
            $data['content']        = $this->load->view('admin_views/admin/admin_profile', $data, true);
            $dataJS['custom_js']    = array('admin/admin_profile');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            $data['admin_info']   = $this->Admin_model->get_admin_info($id);
            $admin_username = $data['admin_info'][0]['username'];

            if($admin_username!=$clean['username']){
                $checkResult = $this->Admin_model->checkUsername($clean['username']);
                if (count($checkResult) > 0) {
                    $this->session->set_flashdata('error_msg', 'This Username already exist');
                    redirect('admin/admin-profile');
                }
            }
            if(isset($clean['image'])){
                $data['image']   = $clean['image'];
            }
            $data['username']   = $clean['username'];
            $data['admin_key']  = $clean['admin_key'];
            $data['updated_at'] = date('Y-m-d H:i:s');
            if(!empty($clean['password'])){
                $data['password']  = $clean['password'];
            }
            $result = $this->Admin_model->updateAdminProfile($data);
            if ($result) {
                if(isset($clean['image'])){
                    $this->session->set_userdata('image', $clean['image']);
                }
                $this->session->set_userdata('username', $data['username']);
                $this->session->set_flashdata('success_msg', 'Profile Updated Successfully');
                redirect('admin/admin-profile');
            }
        }
    }


}
