<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminBannerController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Banner_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view Banner image
    // *********************
    public function viewBanner()
    {  
        $permission = $this->permission->hasAccess(array('view_banner'));
        if($permission['view_banner']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Banner';
            $data['active_link']        = 'banner';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);                              
            $data['content']        = $this->load->view('admin_views/banner/banner', $data, true);
            $dataJS['custom_js']    = array('banner/banner');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {

            $permission = $this->permission->hasAccess(array('add_banner'));
            if($permission['add_banner']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $this->form_validation->set_rules('banner_image_title', 'Banner Title', 'required');   
            $this->form_validation->set_rules('banner_order', 'Banner Order', 'required'); 

            if($this->form_validation->run() == FALSE) {
              $this->session->set_flashdata('error_msg', 'Fill all required fields');
              redirect('admin/slideshow');
            }

            $post  = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $key    = $this->Important_model->generate_key('banner', 'banner_key', 'image');
            $image  = $this->input->post('image');
            if(empty($image)){
                $this->session->set_flashdata('error_msg', 'Please select a photo');
                redirect('admin/slideshow');
            }
            $data['banner_image_name']          = $image;
            $data['banner_image_title']         = $clean['banner_image_title'];
            $data['banner_image_subtitle']      = $clean['banner_image_subtitle'];
            $data['banner_image_description']   = $clean['banner_image_description'];
            $data['banner_order']               = $clean['banner_order'];
            $data['banner_key']                 = $key;            
            $data['is_banner_active']     = 1;
            $data['banner_created_at']    = date('Y-m-d H:i:s');

            $result = $this->Banner_model->insert('banner', $data);

            if(count($clean['banner_anchor_text'])>1){
              for($i=1; $i<count($clean['banner_anchor_text']); $i++){
                $data2['banner_anchor_text']        = $clean['banner_anchor_text'][$i]; 
                $data2['banner_anchor_url']         = $clean['banner_anchor_url'][$i]; 
                $data2['banner_anchor_class']       = $clean['banner_anchor_class'][$i]; 
                $data2['banner_id']                 = $result; 
                $data2['banner_anchor_created_at']  = date('Y-m-d H:i:s');
                $result2 = $this->Banner_model->insert('banner_anchor', $data2);
              }
            }

            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/slideshow');
            }
        }
    }



//**************************
// image upload by dropzone
// *************************
public function saveImage() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('banner_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }



    //**********************
    // delete Banner
    // *********************
    public function deleteBanner()
    {   
        $permission = $this->permission->hasAccess(array('delete_banner'));
        if($permission['delete_banner']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        $id = $this->input->post('banner_key');
        $get_image_info = $this->Banner_model->getBannerInfo($id);
        $image_name = $get_image_info[0]['banner_image_name'];
        $banner_id = $get_image_info[0]['banner_id'];
        $path = $this->config->item('banner_upload_path').$image_name;

        $result = $this->Banner_model->deleteBanner($id);
        $result2 = $this->Banner_model->deleteBannerAnchor($banner_id);
        if (count($result) > 0) {
            if(!empty($image_name) && file_exists($path)){
                unlink($path);
            }            
            echo "Deleted Successfully";
        }
    }


    //*******************************
    // render banner for edit
    // ******************************
    public function renderEditBanner()
    {
        $banner_id = $this->input->post('id');       
        $data['banner_info']   =   $this->Banner_model->get_banner_info($banner_id);
        $id = $data['banner_info'][0]['banner_id'];
        $data['banner_anchor_info']   =   $this->Banner_model->get_banner_anchor_info($id);
        $json = array();
        $json['banner_info']            = $data['banner_info'];
        $json['banner_anchor_info']     = $data['banner_anchor_info'];
        $json['edit_banner_div'] = $this->load->view('admin_views/banner/edit_banner_div', $data, TRUE);
        echo json_encode($json);
    }


     //**********************
    // update Banner
    // *********************
    public function updateBanner()
    { 
        $permission = $this->permission->hasAccess(array('edit_banner'));
        if($permission['edit_banner']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }     
        
        $this->form_validation->set_rules('banner_image_title', 'Banner Title', 'required');   
        $this->form_validation->set_rules('banner_order', 'Banner Order', 'required'); 

        if($this->form_validation->run() == FALSE) {
          $this->session->set_flashdata('error_msg', 'Fill all required fields');
          redirect('admin/slideshow');
        } 

        else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);                   
            if(isset($clean['image2'])){
              $data['banner_image_name']           =   $clean['image2'];  
            }
            $data['banner_image_title']          = $clean['banner_image_title'];
            $data['banner_image_subtitle']       = $clean['banner_image_subtitle'];
            $data['banner_image_description']    = $clean['banner_image_description'];
            $data['banner_order']                = $clean['banner_order'];
            $data['banner_id']                   = $clean['banner_id'];
            $data['banner_updated_at']           = date('Y-m-d H:i:s');
            $data['is_banner_active']            = $clean['is_banner_active'];            $result = $this->Banner_model->updateBanner($data);            
            if ($result) {

              //banner key code start              
                $deleteResult = $this->Banner_model->deletePrevAnchor($clean['banner_id']);
              
                if(count($clean['banner_anchor_text'])>1){
                for($i=0; $i<count($clean['banner_anchor_text']); $i++){

                  if(!empty($clean['banner_anchor_text'][$i]) && !empty($clean['banner_anchor_url'][$i]) && !empty($clean['banner_anchor_class'][$i])){
                    $data2['banner_anchor_text']        = $clean['banner_anchor_text'][$i]; 
                    $data2['banner_anchor_url']         = $clean['banner_anchor_url'][$i]; 
                    $data2['banner_anchor_class']       = $clean['banner_anchor_class'][$i]; 
                    $data2['banner_id']                 = $clean['banner_id']; 
                    $data2['banner_anchor_created_at']  = date('Y-m-d H:i:s');
                    $result2 = $this->Banner_model->insert('banner_anchor', $data2);
                  }
                }
              }

              // banner code update end
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/slideshow');
            }

        }
    }




    //*************************************
    // load Banner/Slideshow table by ajax
    //************************************
    public function loadBannerTable()
    {
        $list = $this->Banner_model->get_datatables('banner');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_banner','delete_banner'));
        foreach ($list as $banner) {
            $no++;          
            $row = array();            
            $row[] = $no;            
            if(!empty($banner['banner_image_name'])){
                $row[] = "<img src=".$this->config->item('banner_source_path').$banner['banner_image_name']." style='height: 40px; width: 40px'>";
            }
            if(empty($banner['banner_image_name'])){ $row[] = "";}            
            $row[] = $banner['banner_image_title'];
            $role['action']="";

            if($permission['edit_banner']==1){
            $role['action'].="<a banner-key='{$banner["banner_key"]}' data-toggle='modal' class='edit_banner_item' data-target='#edit_banner' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>"; 
            }

            if($permission['edit_banner']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_banner='{$banner["banner_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_banner'></i></a>"; 
            }

            $row[] = $role['action'];
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Banner_model->count_all('banner'),
            "recordsFiltered" => $this->Banner_model->count_filtered('banner'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }

}
