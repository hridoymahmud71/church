<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminTestimonialController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Testimonial_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view Banner image
    // *********************
    public function viewTestimonial()
    {
        $permission = $this->permission->hasAccess(array('view_testimonial'));
        if($permission['view_testimonial']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title'] = 'Testimonial';
            $data['active_link'] = 'testimonial';
            $data['headerlink'] = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header'] = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar'] = $this->load->view('admin_views/templates/sidebar', '', true);
            $data['view_testimonial'] = $this->Testimonial_model->getAllData('testimonial');
            $data['content'] = $this->load->view('admin_views/testimonial/testimonial', $data, true);
            $dataJS['custom_js'] = array('testimonial/testimonial');
            $data['footer'] = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {

            $permission = $this->permission->hasAccess(array('add_testimonial'));
            if($permission['add_testimonial']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $this->form_validation->set_rules('testimonial_user_name', 'Testimonial User Name', 'required');
            $this->form_validation->set_rules('testimonial_description', 'Description', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Fill all required fields');
                redirect('admin/testimonial');
            }

            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $key = $this->Important_model->generate_key('testimonial', 'testimonial_key', 'testimonial');

            if (isset($clean['image'])) {
                $data['testimonial_user_image'] = $clean['image'];
            }

            $data['testimonial_user_name'] = $clean['testimonial_user_name'];
            $data['testimonial_description'] = $clean['testimonial_description'];
            $data['testimonial_key'] = $key;
            $data['testimonial_created_at'] = date('Y-m-d H:i:s');

            $result = $this->Testimonial_model->insert('testimonial', $data);

            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/testimonial');
            }
        }
    }



//**************************
// image upload by dropzone
// *************************
    public function saveImage()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = $this->config->item('testimonial_user_upload_path');
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    //************************
    // delete testimonial
    // ***********************
    public function deleteTestimonial()
    {
        $permission = $this->permission->hasAccess(array('delete_testimonial'));
        if($permission['delete_testimonial']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $id = $this->input->post('testimonial_key');

        $get_testimonial_info = $this->Testimonial_model->getTestimonialInfo($id);
        $image_name = $get_testimonial_info[0]['testimonial_user_image'];
        $path = $this->config->item('testimonial_user_upload_path') . '/' . $image_name;

        $result = $this->Testimonial_model->deleteTestimonial($id);
        if (count($result) > 0) {
            if (!empty($image_name) && file_exists($path)) {
                unlink($path);
            }
            echo "Deleted Successfully";
        }
    }


    //*********************
    // render testimonial
    // ********************
    public function renderEditTestimonial()
    {
        $testimonial_id = $this->input->post('id');

        $data['testimonialInfo'] = $this->Testimonial_model->getTestimonialInfo($testimonial_id);

        $json = array();
        $json['testimonialInfo'] = $data['testimonialInfo'];
        $json['edit_testimonial_div'] = $this->load->view('admin_views/testimonial/edit_testimonial_div', $data, TRUE);
        echo json_encode($json);
    }

    //**********************
    // update Testimonial
    // *********************
    public function updateTestimonial()
    {
        $permission = $this->permission->hasAccess(array('edit_testimonial'));
        if($permission['edit_testimonial']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $this->form_validation->set_rules('testimonial_user_name', 'Testimonial User Name', 'required');
        $this->form_validation->set_rules('testimonial_description', 'Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/testimonial');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            if (isset($clean['image2'])) {
                $data['testimonial_user_image'] = $clean['image2'];
            }
            $data['testimonial_key'] = $clean['testimonial_key'];
            $data['testimonial_user_name'] = $clean['testimonial_user_name'];
            $data['testimonial_description'] = $clean['testimonial_description'];
            $data['testimonial_updated_at'] = date('Y-m-d H:i:s');

            $result = $this->Testimonial_model->updateTestimonial($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/testimonial');
            }

        }
    }

    //**********************
    // Mass Testimonial Delete
    // *********************
    public function massTestimonialDelete()
    {
        $permission = $this->permission->hasAccess(array('delete_testimonial'));
        if($permission['delete_testimonial']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $ids = $this->input->post('ids');
        $all_id = explode(",", $ids);
        $result = $this->Testimonial_model->deleteMassTestimonial($all_id);
        if ($result) {
            $this->session->set_flashdata('success_msg', 'Deleted Successfully');
            echo "Delete Successfully";
        }
    }



    //************************
    // load Testimonial table by ajax
    //************************
    public function loadTestimonialTable()
    {
        $list = $this->Testimonial_model->get_datatables('testimonial');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_testimonial','delete_testimonial'));

        foreach ($list as $testimonial) {
            $no++;  
            $row = array();
            $row[]="<input type='checkbox' class='sub_chk' data-id='{$testimonial["testimonial_key"]}'>";
            $row[] = $no;            

            if(!empty($testimonial['testimonial_user_image'])){
                $row[] = "<img src=".$this->config->item('testimonial_user_source_path').$testimonial['testimonial_user_image']." style='height: 40px; width: 40px'>";
            }
            if(empty($testimonial['testimonial_user_image'])){ $row[] = "";}

            $row[] = $testimonial['testimonial_user_name']; 

            $count = strlen($testimonial['testimonial_description']);
            if($count>50){ $more = "...";}
            else{$more='';}
            $description = substr($testimonial['testimonial_description'], 0, 50);
            $row[] = $description.$more;

            $role['action']="";
            if($permission['edit_testimonial']==1){
            $role['action'].="<a testimonial-key='{$testimonial["testimonial_key"]}' data-toggle='modal' class='edit_testimonial' data-target='#exampleModal2' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            if($permission['delete_testimonial']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_testimonial='{$testimonial["testimonial_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_testimonial'></i></a>";
            }

            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Testimonial_model->count_all('testimonial'),
            "recordsFiltered" => $this->Testimonial_model->count_filtered('testimonial'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }


}
