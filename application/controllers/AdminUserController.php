<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminUserController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
        
    }

    public function viewUser(){
        $permission = $this->permission->hasAccess(array('view_user'));
        if($permission['view_user']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }   
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'User';
            $data['active_link']        = 'user';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
            //$data['all_user']       = $this->User_model->getAllUser(); 
            $data['all_role']       = $this->User_model->getAllRole();
            $data['content']        = $this->load->view('admin_views/user/all_user', $data, true);
            $dataJS['custom_js']    = array('user/user');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {

            $permission = $this->permission->hasAccess(array('add_user'));
            if($permission['add_user']==0){
                $this->session->set_flashdata('error_msg', 'Access Denied');
                redirect('admin/dashboard');
            } 

            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $this->form_validation->set_rules('user_username', 'Username', 'required');
            $this->form_validation->set_rules('user_email', 'Email', 'required');
            $this->form_validation->set_rules('user_password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Validation Error');
                redirect('admin/user');
            }

            $key = $this->Important_model->generate_key('user', 'user_key', 'user');            
            if(isset($clean['image'])){
                $data['user_image']   = $clean['image'];
            }  
            $data['user_key']            = $key;
            $data['user_username']       = $clean['user_username'];
            $data['user_email']          = $clean['user_email'];
            $data['user_phone']          = $clean['user_phone'];
            $data['user_password']       = md5($clean['user_password']);
            $data['user_role']           = $clean['user_role'];
            $data['user_status']         = 1;
            $data['user_created_at']     = date('Y-m-d H:i:s');

            $checkResult = $this->User_model->checkUser($data['user_email']);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Email already exist');
                redirect('admin/user');
            }

            $checkResult2 = $this->User_model->checkUserusername($data['user_username']);
            if (count($checkResult2) > 0) {
                $this->session->set_flashdata('error_msg', 'This Username already exist');
                redirect('admin/user');
            }

            $result = $this->User_model->insert('user', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/user');
            }
        }
    }


    //**************************
// image upload by dropzone
// *************************
public function saveImage() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('user_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }

     //*************
    // delete user
    //**************    
    public function deleteUser()
    { 
        $permission = $this->permission->hasAccess(array('delete_user'));
        if($permission['delete_user']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }     
        $id = $this->input->post('user_key');
        $get_user_info = $this->User_model->getUserInfo($id);
        $image_name = $get_user_info[0]['user_image'];
        $path = $this->config->item('user_upload_path').'/'.$image_name;
        $result = $this->User_model->deleteUser($id);
        if (count($result) > 0) {
             if(!empty($image_name) && file_exists($path)){
                unlink($path);
            }  
            echo "Deleted Successfully";
        }
    }


    //*********************
    // render User for Edit
    // ********************
    public function renderEditUser()
    {
        $user_key = $this->input->post('id');
        $data['userInfo']   =   $this->User_model->getUserInfo($user_key);
        $data['all_role']   = $this->User_model->getAllRole();        
        $json = array();
        $json['userInfo']   =   $data['userInfo'];
        $json['all_role']   =   $data['all_role'];
        $json['edit_user_div']  =   $this->load->view('admin_views/user/edit_user_div', $data, TRUE);
        echo json_encode($json);
    } 

    public function updateUser()
    {   
        $permission = $this->permission->hasAccess(array('edit_user'));
        if($permission['edit_user']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }   

        $this->form_validation->set_rules('user_username', 'Username', 'required');
        $this->form_validation->set_rules('user_email', 'Email', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/user');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            $data['user_username'] = $clean['user_username'];
            $data['user_phone'] = $clean['user_phone'];
            $data['user_email'] = $clean['user_email'];
            $data['user_id'] = $clean['user_id'];
            if (!empty($clean['user_password'])) {
                $data['user_password'] = md5($clean['user_password']);
            }
            if(isset($clean['image2'])){                
              $data['user_image'] =   $clean['image2'];
            }
            $user_info = $this->User_model->get_user_info($data['user_id']);
            $user_email = $user_info[0]['user_email'];
            if ($user_email != $data['user_email']) {
                $checkResult = $this->User_model->checkUser($data['user_email']);
                if (count($checkResult) > 0) {
                    $this->session->set_flashdata('error_msg', 'This email already exist!');
                    redirect('admin/user');
                }
            }

            $user_username = $user_info[0]['user_username'];
            if ($user_username != $data['user_username']) {
                $checkResult2 = $this->User_model->checkUserusername($data['user_username']);
                if (count($checkResult2) > 0) {
                    $this->session->set_flashdata('error_msg', 'This username already exist!');
                    redirect('admin/user');
                }
            }


            $result = $this->User_model->updateUser($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Succesfully');
                redirect('admin/user');
            }
        }
    }










    //////////////////////////////// user role part start////////////////////////////////
    
     public function viewUserRole(){
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'User Role';
            $data['active_link']        = 'user_role';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);       
           // $data['all_role']       = $this->User_model->getAllRole(); 
            $data['content']        = $this->load->view('admin_views/user/user_role', $data, true);
            $dataJS['custom_js']    = array('user/user_role');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post); 
            $this->form_validation->set_rules('user_role_name', 'User Role Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error_msg', 'Validation Error');
                redirect('admin/user-role');
            }

            $key = $this->Important_model->generate_key('user_role', 'user_role_key', 'userrole');
            $permission = array('add_sermon','view_sermon','edit_sermon','delete_sermon');
            $role_name = $clean['user_role_name'];
            $temp = $clean;
            unset($temp['user_role_name']);
            foreach($permission as $access){
                if( $this->input->post($access) ){
                    $temp[$access] = 1;
                }else{
                    $temp[$access] = 0;
                }
            }
            unset($clean);
            $data['user_role_permission']= json_encode($temp);
            $data['user_role_name']=$role_name;
            $data['user_role_key']            = $key;
            $data['user_role_created_at']     = date('Y-m-d H:i:s');
            $permission = array('view_event','view_gallery');
            $checkResult = $this->User_model->checkUserRole($clean['user_role_name']);
            if (count($checkResult) > 0) {
                $this->session->set_flashdata('error_msg', 'This Role Name already exist');
                redirect('admin/user-role');
            }

            $result = $this->User_model->insert('user_role', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/user-role');
            }
        }
    }


    //***************************
    // render User Role for Edit
    //**************************
    public function renderEditUserRole()
    {
        $user_role_key = $this->input->post('id');
        $roleInfo   =   $this->User_model->getUserRoleInfo($user_role_key); 
        $data['user_role_name'] = $roleInfo[0]['user_role_name'];      
        $data['user_role_id'] = $roleInfo[0]['user_role_id'];      
        $data['permission'] = json_decode($roleInfo[0]['user_role_permission']);
        $json = array();
        $json['user_role_name']      =   $data['user_role_name'];
        $json['permission']          =   $data['permission'];
        $json['user_role_id']        =   $data['user_role_id'];
        $json['edit_user_role_div']  =   $this->load->view('admin_views/user/edit_user_role_div', $data, TRUE);
        echo json_encode($json);
    } 


    //*********************
    // render User Role for Edit
    //********************
    
    public function updateUserRole()
    {  
        $this->form_validation->set_rules('user_role_name', 'User Role Name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/user-role');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);
            $user_role_id = $clean['user_role_id'];
            $user_role_name = $clean['user_role_name'];

            $get_role_info = $this->User_model->getRoleName($user_role_id);
            $role_name = $get_role_info[0]['user_role_name'];

            if($role_name != $clean['user_role_name']){
                $checkRoleName = $this->User_model->check_role_name($clean['user_role_name']);
                if($checkRoleName){
                    $this->session->set_flashdata('error_msg', 'This role name already exist!');
                    redirect('admin/user-role');
                }
            }

            $permission = array('add_sermon','view_sermon','edit_sermon','delete_sermon');
            $temp = $clean;
            unset($temp['user_role_name'], $temp['user_role_id']);

            foreach($permission as $access){
                if( $this->input->post($access) ){
                    $temp[$access] = 1;
                }else{
                    $temp[$access] = 0;
                }
            }
            unset($clean);
            $clean['user_role_permission']= json_encode($temp);
            $clean['user_role_name']=$user_role_name; 
            $clean['user_role_updated_at']= date('Y-m-d H:i:s');            
            $clean['user_role_id']= $user_role_id;  
            $result = $this->User_model->updateUserRole($clean);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Succesfully');
                redirect('admin/user-role');
            }
        }
    }


    //************************
    // load Role table by ajax
    //************************
    public function loadRoleTable()
    {
        $list = $this->User_model->get_datatables('user_role');
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $role) {
            $no++;          
            $row = array();
            $row[] = $no;
            $row[] = $role['user_role_name']; 
            $role['action']="<a role-key='{$role["user_role_key"]}' data-toggle='modal' class='edit_role' data-target='#editModal' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            $row[] = $role['action'];            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->count_all($table='user_role'),
            "recordsFiltered" => $this->User_model->count_filtered($table='user_role'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }


    //************************
    // load User table by ajax
    //************************
    public function loadUserTable()
    {
        $list = $this->User_model->get_datatables('user');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_user','delete_user'));
        foreach ($list as $user) {
            $no++;  
            $row = array();
            $row[] = $no;
            if(!empty($user['user_image'])){
                $row[] = "<img src=".$this->config->item('user_source_path').$user['user_image']." style='height: 40px; width: 40px'>";
            }
            if(empty($user['user_image'])){ $row[] = "";}
            $row[] = $user['user_username']; 
            $row[] = $user['user_email']; 
            $role['action']="";
            if($permission['edit_user']==1){
                $role['action'].="<a user-key='{$user["user_key"]}' data-toggle='modal' class='edit_user' data-target='#exampleModal2' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['delete_user']==1){
                $role['action'].="<a href='javascript:void(0)'><i data-delete_user='{$user["user_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_user'></i></a>";
            }

            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->User_model->count_all('user'),
            "recordsFiltered" => $this->User_model->count_filtered('user'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }
    


   
     


}
