<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminGalleryController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Gallery_model');
        $this->load->model('Important_model');
        $loggedUserId = $this->session->userdata('id');
        if (!isset($loggedUserId)) {
            redirect('admin/login');
        }
    }



    //**********************
    // view gallery image
    // *********************
    public function viewGallery()
    { 
        $permission = $this->permission->hasAccess(array('view_gallery_image'));
        if($permission['view_gallery_image']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }   
        
        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']         = 'Gallery Images';
            $data['active_link']        = 'gallery_image';
            $data['headerlink']         = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']             = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']            = $this->load->view('admin_views/templates/sidebar', '', true);            
            //$data['view_gallery']       = $this->Gallery_model->getGalleryImage();            
            $data['view_gallery_category']   = $this->Gallery_model->getAllData('gallery_category');       
            $data['content']        = $this->load->view('admin_views/gallery/gallery', $data, true);
            $dataJS['custom_js']    = array('gallery/gallery');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            //image upload code end

            $key = $this->Important_model->generate_key('gallery', 'image_key', 'img');
            $image = $this->input->post('image');

             if(empty($image)){
                $this->session->set_flashdata('error_msg', 'Please select a photo');
                redirect('admin/gallery-images');
            }

            $data['image_name']          = $image;
            $data['image_description']   = $clean['image_description'];
            $data['is_featured_image']   = $clean['is_featured_image'];
            $data['image_order']         = $clean['image_order'];
            $data['image_description']   = $clean['image_description'];
            $data['image_key']           = $key;
            $data['gallery_category_id'] = $clean['gallery_category_id'];
            $data['image_created_at']    = date('Y-m-d H:i:s');
            $result = $this->Gallery_model->insert('gallery', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/gallery-images');
            }
        }
    }


    //**********************
    // delete gallery image
    // *********************
    public function deleteGalleryImage()
    {
        $permission = $this->permission->hasAccess(array('delete_gallery_image'));
        if($permission['delete_gallery_image']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        $id = $this->input->post('image_key');
        $get_image_info = $this->Gallery_model->getItemInfo($id);
        $image_name = $get_image_info[0]['image_name'];
        $path = $this->config->item('gallery_upload_path').$image_name;

        $result = $this->Gallery_model->deleteGalleryImage($id);
        if (count($result) > 0) {
            if(file_exists($path)){
                unlink($path);
            }            
            echo "Deleted Successfully";
        }
    }

    
    //*******************************
    // render gallery image for edit
    // ******************************
    public function renderEditGallery()
    {
        $gallery_id = $this->input->post('id');
        $data['itemInfo']   =   $this->Gallery_model->getItemInfo($gallery_id);
         $data['view_gallery_category']   = $this->Gallery_model->getAllData('gallery_category');
        $json = array();
        $json['challenge_info']            = $data['itemInfo'];
        $json['view_gallery_category']     = $data['view_gallery_category'];
        $json['edit_gallery_div'] = $this->load->view('admin_views/gallery/edit_gallery_div', $data, TRUE);
        echo json_encode($json);
    }


    //**********************
    // update gallery image
    // *********************
    public function updateGalleryItem()
    {   
        $permission = $this->permission->hasAccess(array('edit_gallery_image'));
        if($permission['edit_gallery_image']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $this->form_validation->set_rules('is_featured_image', 'Feature Image', 'required');
        $this->form_validation->set_rules('gallery_category_id', 'Gallery', 'required');
        $this->form_validation->set_rules('image_order', 'Image Order', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/gallery-images');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post); 

            if(isset($clean['image2'])){                
                $data['image_name']           =   $clean['image2'];
            }
            $data['image_key']          = $clean['image_key'];
            $data['image_description']  = $clean['image_description'];
            $data['is_featured_image']  = $clean['is_featured_image'];
            $data['image_order']        = $clean['image_order'];
            $data['gallery_category_id']= $clean['gallery_category_id'];
            $data['image_updated_at']   = date('Y-m-d H:i:s');
            $result = $this->Gallery_model->updateGalleryItem($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/gallery-images');
            }

        }
    }


//**************************
// image upload by dropzone
// *************************
public function multi() {
       $files = $_FILES;
       if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
           $config['encrypt_name'] = TRUE;
           $config['upload_path'] = $this->config->item('gallery_upload_path');
           $config['allowed_types'] = 'jpg|png|jpeg';
           $config['max_size'] = '0';
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('userfile')) {
               $status = 'error';
               $msg = $this->upload->display_errors('', '');
           } else {
               $fileinfo = $this->upload->data();
               echo $fileinfo['file_name'];
           }
       }
   }




//**********************
// view gallery category
// *********************

  public function viewGalleryCategory()
    {   
        $permission = $this->permission->hasAccess(array('view_gallery'));
        if($permission['view_gallery']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }   

        if ($this->input->server('REQUEST_METHOD') != 'POST') {

            $data['page_title']     = 'Gallery';
            $data['active_link']    = 'gallery';
            $data['headerlink']     = $this->load->view('admin_views/templates/headerlink', $data, true);
            $data['header']         = $this->load->view('admin_views/templates/header', '', true);
            $data['sidebar']        = $this->load->view('admin_views/templates/sidebar', '', true);          
            $data['content']        = $this->load->view('admin_views/gallery/gallery_category', $data, true);
            $dataJS['custom_js']    = array('gallery/gallery_category');
            $data['footer']         = $this->load->view('admin_views/templates/footer', $dataJS, true);
            $this->load->view('admin_views/index', $data);
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);

            $key = $this->Important_model->generate_key('gallery_category', 'gallery_category_key', 'gallery');
            $image = $this->input->post('image');
            $data['gallery_category_name'] = $clean['gallery_category_name'];
            $data['gallery_category_key'] = $key;
            $data['category_created_at'] = date('Y-m-d H:i:s');
            $result = $this->Gallery_model->insert('gallery_category', $data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Added Successfully');
                redirect('admin/gallery');
            }
        }
    }



    //************************
    // update gallery category
    // ***********************
    public function updateGalleryCategory()
    { 
        $permission = $this->permission->hasAccess(array('edit_gallery'));
        if($permission['edit_gallery']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        }  

        $this->form_validation->set_rules('gallery_category_name', 'Gallery Name', 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error_msg', 'Please Fill up the required field');
            redirect('admin/gallery');
        } else {
            $post = $this->input->post();
            $clean = $this->security->xss_clean($post);           
            $data['gallery_category_name']      = $clean['gallery_category_name'];
            $data['gallery_category_key']       = $clean['gallery_category_key'];
            $data['category_updated_at']        = date('Y-m-d H:i:s');

            $result = $this->Gallery_model->updateGalleryCategory($data);
            if ($result) {
                $this->session->set_flashdata('success_msg', 'Updated Successfully');
                redirect('admin/gallery');
            }

        }
    }


    //************************
    // delete gallery category
    //************************
    public function deleteGalleryCategory()
    {
        $permission = $this->permission->hasAccess(array('delete_gallery'));
        if($permission['delete_gallery']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $id = $this->input->post('gallery_key');
        $result = $this->Gallery_model->deleteGalleryCategory($id);
        if (count($result) > 0) {                       
            echo "Deleted Successfully";
        }
    }



    //**********************
    // Mass Category Delete
    // *********************
    public function massGalleryCategoryDelete(){
        $permission = $this->permission->hasAccess(array('delete_gallery'));
        if($permission['delete_gallery']==0){
            $this->session->set_flashdata('error_msg', 'Access Denied');
            redirect('admin/dashboard');
        } 

        $ids = $this->input->post('ids'); 
        $all_id = explode(",",$ids);
        $result = $this->Gallery_model->deleteMassGalleryCategory($all_id);
        if($result){
            $this->session->set_flashdata('success_msg', 'Deleted Successfully');
            echo "Delete Successfully";
        }
    }


    //************************
    // load Gallery Category table by ajax
    //************************
    public function loadGalleryCategoryTable()
    {
        $list = $this->Gallery_model->get_datatables('gallery_category');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_gallery','delete_gallery'));
        foreach ($list as $category) {
            $no++;          
            $row = array();
            $row[]="<input type='checkbox' class='sub_chk' data-id='{$category["gallery_category_key"]}'>";
            $row[] = $no;            
            $row[] = $category['gallery_category_name']; 

            $role['action']="";
            if($permission['edit_gallery']==1){
            $role['action'].="<a gallery-category-key='{$category["gallery_category_key"]}' gallery-category-name='{$category["gallery_category_name"]}' data-toggle='modal' class='edit_gallery_category_item' data-target='#edit_gallery_category' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['delete_gallery']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_gallery_category='{$category["gallery_category_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_gallery_category'></i></a>"; 
            }
            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Gallery_model->count_all('gallery_category'),
            "recordsFiltered" => $this->Gallery_model->count_filtered('gallery_category'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }



    //*********************************
    // load Gallery Image table by ajax
    //*********************************
    public function loadGalleryImageTable()
    {
        $list = $this->Gallery_model->get_datatables('gallery');
        $data = array();
        $no = $_POST['start'];
        $permission = $this->permission->hasAccess(array('edit_gallery_image','delete_gallery_image'));
        foreach ($list as $gallery) {
            $no++;          
            $row = array();
            $row[] = $no;  
            if(!empty($gallery['image_name'])){
                $row[] = "<img src=".$this->config->item('gallery_source_path').$gallery['image_name']." style='height: 40px; width: 40px'>";
            }
            if(empty($gallery['image_name'])){ $row[] = "";}
            $row[] = $gallery['gallery_category_name']; 
            $row[] = $gallery['image_description']; 
            if($gallery['is_featured_image']==1){ $row[] = 'YES'; } 
            if($gallery['is_featured_image']==0){ $row[] = 'NO'; }
            $row[] = $gallery['image_order'];  

            $role['action']="";
            if($permission['edit_gallery_image']==1){
            $role['action'].="<a image-key='{$gallery["image_key"]}' data-toggle='modal' class='edit_gallery_item' data-target='#edit_gallery_item' href='javascript:void(0)'><i style='margin-right:10px; font-size: 16px;'' data-toggle='tooltip' title='Edit' class='fa fa-pencil'></i></a>";
            }
            
            if($permission['delete_gallery_image']==1){
            $role['action'].="<a href='javascript:void(0)'><i data-delete_gallery_item='{$gallery["image_key"]}' style='font-size: 16px' data-toggle='tooltip' title='Delete' class='fa fa-trash btn_delete_gallery_item'></i></a>";
            }
            $row[] = $role['action'];
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Gallery_model->count_all('gallery'),
            "recordsFiltered" => $this->Gallery_model->count_filtered('gallery'),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
        die();
    }
    
    

   


}
