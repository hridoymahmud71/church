<?php
class AuthModel extends CI_Model {

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function checkLogin($post)
	{    
		$this->db->select('user_id, user_username, user_key, user_email, user_image');
		$this->db->where('user_email', $post['email']);
		$this->db->where('user_password', $post['password']);
		$this->db->where('user_status', 1);
		$query = $this->db->get('user');
		$userInfo = $query->row();		
		return $userInfo; 
	}

	public function checkUser($email){
        $this->db->select('*');
        $this->db->from('user'); 
        $this->db->where('user_email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

	

    

}