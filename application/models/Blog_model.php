<?php
class Blog_model extends CI_Model {  



    /* ******************************* FontEnd **************************** */    

    public function getActiveBlogList($limit, $starts){

        $query = $this->__blogListQuery($limit, $starts);

        $blogs = $query->result_array();

        if (!empty($blogs)) {
            $blogs = array_map("self::put_related_items_in_blogs", $blogs);
        }
        return $blogs;
    }

    private function __blogListQuery($limit, $starts)
    {
        $this->db->select('*');
        $this->db->from('blog');

        $this->db->order_by('blog_id', 'desc');

        if ($limit != "ignore" && $starts != "ignore") {
            $this->db->limit($limit, $starts);
        } else if ($limit != "ignore" && $starts == "ignore") {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query;
    }

    public function put_related_items_in_blogs($blog)
    {   
        if (!empty($blog)) {
            $blog['blog_tags'] = $this->getBlogTags($blog['blog_id']);
            $blog['blog_comments'] = $this->getBlogComments($blog['blog_id']);
            
            $blog['prev_blog'] = $this->_getPrevBlog($blog['blog_id']);
            $blog['next_blog'] = $this->_getNextBlog($blog['blog_id']);
        }

        return $blog;

    }

    private function _getPrevBlog($blog_id)
    {
        $ret = array();
        if (!empty($blog_id)) {
            $this->db->select('*');
            $this->db->from('blog');
            $this->db->order_by('blog_id', 'desc');
            $this->db->where('blog_id!=', $blog_id);
            $this->db->where('blog_id >', $blog_id);
            $ret = $this->db->get()->row_array();
        }

        return $ret;
    }

    private function _getNextBlog($blog_id)
    {
        $ret = array();
        if (!empty($blog_id)) {
            $this->db->select('*');
            $this->db->from('blog');
            $this->db->order_by('blog_id', 'desc');
            $this->db->where('blog_id!=', $blog_id);
            $this->db->where('blog_id <', $blog_id);
            $ret = $this->db->get()->row_array();
        }
        return $ret;
    }



    public function getBlogTags($blog_id)
    {
        $this->db->select('*');
        $this->db->from('blog_tag');
        $this->db->where('blog_id', $blog_id);
        $blog_tags = $this->db->get()->result_array();
        return $blog_tags;
    }

    public function getBlogComments($blog_id)
    {
        $this->db->select('*');
        $this->db->from('blog_comment');
        $this->db->where('blog_id', $blog_id);
        $this->db->where('blog_comment_status', 1);
        $blog_tags = $this->db->get()->result_array();
        return $blog_tags;
    }

    public function getRecentPosts($limit){
        $this->db->select('*');
        $this->db->from('blog'); 
        $this->db->order_by('blog_id', 'DESC');
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getTags($limit){
        $this->db->select('*');
        $this->db->from('blog_tag'); 
        $this->db->group_by('blog_tag');
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function getBlogByKey($blog_key)
    {
        $blog = $this->__getBlogByKey($blog_key);

        if (!empty($blog)) {
            $blog = $this->put_related_items_in_blogs($blog);
        }
        return $blog;
    }

    private function __getBlogByKey($blog_key)
    {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('blog_key', $blog_key);
        return $this->db->get()->row_array();
    }

    
    

 /* ******************************* BackEnd **************************** */ 

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    
    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='blog'){
            $table = 'blog';
            $column_order = array('blog_title',null); 
            $column_search = array('blog_title'); 
            $order = array('blog_id' => 'desc');
        }

        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if($order)
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    public function getBlogInfo($blog_key){
        $this->db->select('*');
        $this->db->from('blog'); 
        $this->db->where('blog_key', $blog_key);
        $result = $this->db->get();
        return $result->result_array();
    }    

    public function getBlogTag($blog_id){
        $this->db->select('*');
        $this->db->from('blog_tag'); 
        $this->db->where('blog_id', $blog_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateBlog($data)
    {   
        $blog_id = $data['blog_id'];       
        $this->db->set('blog_title', $data['blog_title']);
        $this->db->set('blog_description', $data['blog_description']);
        $this->db->set('blog_updated_at', $data['blog_updated_at']);
        if(isset($data['blog_feature_image'])){
            $this->db->set('blog_feature_image', $data['blog_feature_image']);
        }       
        $this->db->where('blog_id', $blog_id);
        return  $query=$this->db->update('blog');
    }

    public function deletePreviousTag($blog_id)
    {
        $this->db->where('blog_id', $blog_id);
        return $query = $this->db->delete('blog_tag');
    }

    public function deleteBlog($blog_key)
    {
        $this->db->where('blog_key', $blog_key);
        return $query = $this->db->delete('blog');
    }

    public function deleteMassBlog($all_id)
    {
        $this->db->where_in('blog_id', $all_id);
        return $query = $this->db->delete('blog');
    }
    public function deleteMassTag($all_id)
    {
        $this->db->where_in('blog_id', $all_id);
        return $query = $this->db->delete('blog_tag');
    }

    public function getBlogTotalComment($blog_id){
        $this->db->select('*');
        $this->db->from('blog_comment'); 
        $this->db->where('blog_id', $blog_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBlogActiveComment($blog_id){
        $this->db->select('*');
        $this->db->from('blog_comment'); 
        $this->db->where('blog_id', $blog_id);
        $this->db->where('blog_comment_status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBlogDeactiveComment($blog_id){
        $this->db->select('*');
        $this->db->from('blog_comment'); 
        $this->db->where('blog_id', $blog_id);
        $this->db->where('blog_comment_status', 0);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBlogComment($blog_id){
        $this->db->select('*');
        $this->db->from('blog_comment'); 
        $this->db->where('blog_id', $blog_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_blog_comment($blog_comment_key){
        $this->db->select('*');
        $this->db->from('blog_comment'); 
        $this->db->where('blog_comment_key', $blog_comment_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateCommentStatus($blog_comment_key, $status){
        $this->db->set('blog_comment_status', $status);
        $this->db->where('blog_comment_key', $blog_comment_key);
        return  $query=$this->db->update('blog_comment');

    }

    public function blog_info($blog_id){
        $this->db->select('*');
        $this->db->from('blog'); 
        $this->db->where('blog_id', $blog_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteBlogComment($blog_comment_key)
    {
        $this->db->where('blog_comment_key', $blog_comment_key);
        return $query = $this->db->delete('blog_comment');
    }






    

    







    

    


    
     
	


	
}