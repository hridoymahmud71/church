<?php

class Banner_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    //---------------------------------------------------------Front----------------------------------------------------

    public function getActiveBanners($limit)
    {
        $banners = $this->__getActiveBanners($limit);

        if (!empty($banners)) {
            $banners = array_map("self::put_related_items_in_banners", $banners);
        }

        return $banners;

    }

    public function put_related_items_in_banners($banner)
    {
        $banner_image_name = "";
        $banner_image_name_with_path = $this->config->item("default_image_placeholder");

        if (!empty($banner)) {
            if (!empty($banner['banner_image_name'])) {
                $banner_image_name = $banner['banner_image_name'];
                $banner_image_name_with_path =
                    $this->config->item("banner_source_path") . $banner['banner_image_name'];
            }

            $banner['banner_anchors'] = $this->getBannerAnchors($banner['banner_id']);
        }

        $banner['banner_image_name'] = $banner_image_name;
        $banner['banner_image_name_with_path'] = $banner_image_name_with_path;

        return $banner;
    }

    private function __getActiveBanners($limit)
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('banner_image_name!=', null);
        $this->db->where('banner_image_name!=', '');
        $this->db->where('is_banner_active', 1);
        $this->db->order_by('banner_order', 'asc');
        $this->db->limit($limit);

        return $this->db->get()->result_array();
    }

    public function getBannerAnchors($banner_id)
    {
        $this->db->select('*');
        $this->db->from('banner_anchor');
        $this->db->where('banner_id', $banner_id);

        $this->db->where('banner_anchor_text!=', null);
        $this->db->where('banner_anchor_text!=', '');

        $this->db->where('banner_anchor_url!=', null);
        $this->db->where('banner_anchor_url!=', '');

        return $this->db->get()->result_array();
    }


    //---------------------------------------------------------Backend----------------------------------------------------
    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getBannerInfo($id)
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('banner_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deleteBanner($id)
    {
        $this->db->where('banner_key', $id);
        return $query = $this->db->delete('banner');
    }

    public function deleteBannerAnchor($id)
    {
        $this->db->where('banner_id', $id);
        return $query = $this->db->delete('banner_anchor');
    }

    public function get_banner_info($id)
    {
        $this->db->select('*');
        $this->db->from('banner');
        $this->db->where('banner_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_banner_anchor_info($id)
    {
        $this->db->select('*');
        $this->db->from('banner_anchor');
        $this->db->where('banner_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function updateBanner($data)
    {
        $banner_id = $data['banner_id'];
        $this->db->set('banner_image_title', $data['banner_image_title']);
        $this->db->set('banner_image_subtitle', $data['banner_image_subtitle']);
        $this->db->set('banner_image_description', $data['banner_image_description']);
        if (isset($data['banner_image_name'])) {
            $this->db->set('banner_image_name', $data['banner_image_name']);
        }
        $this->db->set('banner_order', $data['banner_order']);
        $this->db->set('banner_updated_at', $data['banner_updated_at']);
        $this->db->set('is_banner_active', $data['is_banner_active']);
        $this->db->where('banner_id', $banner_id);
        return $query = $this->db->update('banner');
    }

    public function deletePrevAnchor($id)
    {
        $this->db->where('banner_id', $id);
        return $query = $this->db->delete('banner_anchor');
    }


    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='banner'){
            $table = 'banner';
            $column_order = array('banner_image_title',null); 
            $column_search = array('banner_image_title'); 
            $order = array('banner_id' => 'desc');
        }
        
        $this->db->from($table);
        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($order))
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }
}