<?php
class Page_model extends CI_Model
{

/* ********************** FontEnd ************************************* */

public function getPageContent($slug)
    {
        $this->db->select('*');
        $this->db->from('page');
        $this->db->where('page_slug', $slug);
        $this->db->where('page_status', 1);
        $result = $this->db->get();
        return $result->row_array();
    }







/* ********************** Backend ************************************* */
    
    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }
    
    public function getPageInfo($page_key)
    {
        $this->db->select('*');
        $this->db->from('page');
        $this->db->where('page_key', $page_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkSlug($slug)
    {
        $this->db->select('*');
        $this->db->from('page');
        $this->db->where('page_slug', $slug);
        $result = $this->db->get();
        return $result->result_array();
    }

    /**
     * Delete a page by key.
     *
     * @param string $page_key page key
     *
     * @return bool            1=>page deleted, 0=>page not deleted
     */
    public function deletePage($page_key)
    {
        $this->db->where('page_key', $page_key);
        return $query = $this->db->delete('page');
    }

    public function getPrevPageInfo($page_id)
    {
        $this->db->select('*');
        $this->db->from('page');
        $this->db->where('page_id', $page_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updatePage($data, $page_id)
    {
        $this->db->where('page_id', $page_id);
        if ($this->db->update('page', $data)) {
            return true;
        } else {
            return false;
        }
    }
}
