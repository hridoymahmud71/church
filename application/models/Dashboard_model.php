<?php

class Dashboard_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

   
    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function getUser()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSermon()
    {
        $this->db->select('*');
        $this->db->from('sermon');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBlog()
    {
        $this->db->select('*');
        $this->db->from('blog');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getBooking()
    {
        $this->db->select('*');
        $this->db->from('event_booking');
        $this->db->where('event_booking_status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function todayBooking()
    {
        // $this->db->select('*');
        // $this->db->from('event_booking');
        // $this->db->where('event_booking_status', 1);
        // $result = $this->db->get();
        // return $result->result_array();
        $query="SELECT event_booking.*, DATE_FORMAT(event_booking.event_booking_created_at, '%Y-%m-%d') 
        FROM event_booking WHERE (DATE(event_booking_created_at) = CURDATE() AND event_booking_status = 1)";
        $result=$this->db->query($query)->result_array();
    }

    

}