<?php
class Permission_model extends CI_Model {

    /**
    *  Access string example r array('add_user','edit_user')
    **/
    public function hasAccess($access=array()){
        $staff_type_id = $this->session->userdata('role');
        $role = $this->getRoleInfo($staff_type_id);
        if($role){  
            $data['permission'] = json_decode($role['staff_type_permission']);            
            foreach ($data['permission'] as $key => $value) {
                for($i=0; $i<count($access); $i++){
                    if($key==$access[$i]){ $result[$key]=$value; }
                }
            }                
        } 
        return $result;             
    } 

    public function getRoleInfo($id){
      $this->db->where('staff_type_id', $id);
      $role = $this->db->get('staff_type')->result_array();
      return $role[0];
    }


    /* *************************************** */
    
    public function getLoggedUserName($id){
        $this->db->select('*');
        $this->db->from('staff'); 
        $this->db->where('staff_id', $id);
        $result = $this->db->get();
        $result = $result->result_array();
        return isset($result[0])?$result[0]['staff_name']:NULL;      
    }   

    public function getLoggedUserImage($id){
        $this->db->select('*');
        $this->db->from('staff_image'); 
        $this->db->where('staff_id', $id);
        $this->db->where('is_staff_image_featured', 1);
        $result = $this->db->get();
        $result = $result->result_array();
        return isset($result[0])?$result[0]['staff_image_name']:NULL;      
    }



        
	
}