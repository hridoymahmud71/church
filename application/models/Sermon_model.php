<?php
class Sermon_model extends CI_Model {
     /* ******************************* FontEnd **************************** */    

    public function getActiveSermonList($limit, $starts){

        $query = $this->__sermonListQuery($limit, $starts);

        $sermons = $query->result_array();

        if (!empty($sermons)) {
            $sermons = array_map("self::put_related_items_in_sermons", $sermons);
        }
        return $sermons;
    }

    private function __sermonListQuery($limit, $starts)
    {
        $this->db->select('sermon.*, staff.staff_name, staff.staff_key, staff_type.staff_type_name');
        $this->db->from('sermon');
        $this->db->join('staff', 'staff.staff_id=sermon.staff_id', 'LEFT');
        $this->db->join('staff_type', 'staff_type.staff_type_id=staff.staff_type_id', 'LEFT');

        //$this->db->order_by('sermon.sermon_order', 'asc');
        $this->db->order_by('sermon.sermon_id', 'DESC');

        if ($limit != "ignore" && $starts != "ignore") {
            $this->db->limit($limit, $starts);
        } else if ($limit != "ignore" && $starts == "ignore") {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query;
    }

    public function put_related_items_in_sermons($sermon)
    {   
        if (!empty($sermon)) {
            $sermon['sermon_tags']      = $this->getSermonTags($sermon['sermon_id']);
            $sermon['sermon_comments']  = $this->getSermonComments($sermon['sermon_id']);
            $sermon['sermon_video']     = $this->getSermonVideo($sermon['sermon_id']);
            $sermon['sermon_audio']     = $this->getSermonAudio($sermon['sermon_id']);
            $sermon['sermon_file']      = $this->getSermonFiles($sermon['sermon_id']);

            $sermon['prev_sermon'] = $this->_getPrevSermon($sermon['sermon_id'], $sermon['sermon_order']);
            $sermon['next_sermon'] = $this->_getNextSermon($sermon['sermon_id'], $sermon['sermon_order']);
            

        }
        return $sermon;
    }

    private function _getPrevSermon($sermon_id, $sermon_order)
    {
        $ret = array();
        if (!empty($sermon_id)) {
            $this->db->select('*');
            $this->db->from('sermon');
            $this->db->order_by('sermon_id', 'DESC');
            $this->db->where('sermon_id!=', $sermon_id);
            $this->db->where('sermon_id >=', $sermon_id);
            $ret = $this->db->get()->row_array();
        }

        return $ret;
    }

    private function _getNextSermon($sermon_id, $sermon_order)
    {
        $ret = array();
        if (!empty($sermon_id)) {
            $this->db->select('*');
            $this->db->from('sermon');
            $this->db->order_by('sermon_id', 'DESC');
            $this->db->where('sermon_id!=', $sermon_id);
            $this->db->where('sermon_id <=', $sermon_id);
            $ret = $this->db->get()->row_array();

        }
        return $ret;
    }

    public function getSermonTags($sermon_id)
    {
        $this->db->select('*');
        $this->db->from('sermon_tag');
        $this->db->where('sermon_id', $sermon_id);
        $sermon_tags = $this->db->get()->result_array();
        return $sermon_tags;
    }

    public function getSermonComments($sermon_id)
    {
        $this->db->select('*');
        $this->db->from('sermon_comment');
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_comment_status', 1);
        $sermon_comments = $this->db->get()->result_array();
        return $sermon_comments;
    }

    public function getSermonVideo($sermon_id)
    {
        $this->db->select('*');
        $this->db->from('sermon_file');
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', 'video');
        $sermon_comments = $this->db->get()->row_array();
        return $sermon_comments;
    }

    public function getSermonAudio($sermon_id)
    {
        $this->db->select('*');
        $this->db->from('sermon_file');
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', 'audio');
        $sermon_comments = $this->db->get()->row_array();
        return $sermon_comments;
    }

    public function getSermonFiles($sermon_id)
    {
        $this->db->select('*');
        $this->db->from('sermon_file');
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', 'file');
        $sermon_comments = $this->db->get()->row_array();
        return $sermon_comments;
    }

    public function getSermonByKey($sermon_key)
    {
        $sermon = $this->__getSermonByKey($sermon_key);
        if (!empty($sermon)) {
            $sermon = $this->put_related_items_in_sermons($sermon);
        }
        return $sermon;
    }

    private function __getSermonByKey($sermon_key)
    {
        $this->db->select('*');
        $this->db->from('sermon');
        $this->db->where('sermon_key', $sermon_key);
        return $this->db->get()->row_array();
    }

    public function getRecentPosts($limit){
        $this->db->select('*');
        $this->db->from('blog'); 
        $this->db->order_by('blog_id', 'DESC');
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }

     public function getRecentSermons($limit){
        $this->db->select('*');
        $this->db->from('sermon'); 
        $this->db->order_by('sermon.sermon_created_at', 'DESC');
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function get_sermon_file_info($sermon_file_key)
    {
        $this->db->select('*');
        $this->db->from('sermon_file');
        $this->db->where('sermon_file_key', $sermon_file_key);
        $result = $this->db->get()->result_array();
        return $result;
    }


    /* ***************************************************** */

	public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

	public function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }

    public function getAllPastor(){
        $this->db->select('staff_id, staff_name');
        $this->db->from('staff'); 
        $this->db->join('staff_type', 'staff_type.staff_type_id=staff.staff_type_id', 'left');
        $this->db->where('staff_type.staff_type_shortname', 'pastor');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSermonInfo($sermon_key){
        $this->db->select('*');
        $this->db->from('sermon'); 
        $this->db->where('sermon_key', $sermon_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getSermonFile($sermon_id, $sermon_file_type){
        $this->db->select('*');
        $this->db->from('sermon_file'); 
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $sermon_file_type);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function updateSermon($data,$sermon_id){
        $this->db->where('sermon_id', $sermon_id);
        if($this->db->update('sermon', $data)){
            return TRUE;
        }
        else {return FALSE;}
    }


    public function check_sermon_file($sermon_id, $type){
        $this->db->select('*');
        $this->db->from('sermon_file'); 
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $type);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_sermon_files($sermon_id, $sermon_file_link_type, $sermon_file, $type)
    {   
        $this->db->set('sermon_file_link_type', $sermon_file_link_type);
        $this->db->set('sermon_file', $sermon_file);
        $this->db->set('sermon_file_updated_at', date('Y-m-d H:i:s'));
        $this->db->where('sermon_id', $sermon_id);
        $this->db->where('sermon_file_type', $type);
        return  $query=$this->db->update('sermon_file');
    }

    public function getSermonTag($sermon_id){
        $this->db->select('*');
        $this->db->from('sermon_tag'); 
        $this->db->where('sermon_id', $sermon_id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function deletePreviousTag($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_tag');
    }

    public function deleteSermon($sermon_key)
    {
        $this->db->where('sermon_key', $sermon_key);
        return $query = $this->db->delete('sermon');
    }

    public function deleteSermonFile($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_file');
    }
    public function deleteSermonTag($sermon_id)
    {
        $this->db->where('sermon_id', $sermon_id);
        return $query = $this->db->delete('sermon_tag');
    }

    

	
}