<?php

class Staff_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    //---------------------------------------------------------Front----------------------------------------------------

    public function getActiveStaffs($limit)
    {
        $staffs = $this->__getActiveStaffs($limit);

        if (!empty($staffs)) {
            $staffs = array_map("self::put_related_items_in_staffs", $staffs);
        }

        return $staffs;

    }


    public function put_related_items_in_staffs($staff)
    {
        if (!empty($staff)) {
            $staff['staff_type'] = $this->getStaffType($staff['staff_type_id']);
            $staff['staff_social_links'] = $this->getStaffSocialLinks($staff['staff_id']);
            $staff['staff_images'] = $this->getStaffImages($staff['staff_id']);
            $staff['featured_image'] = $this->getStaffFeaturedImage($staff['staff_id']);
        }

        return $staff;
    }

    private function __getActiveStaffs($limit)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('is_staff_active', 1);
        $this->db->order_by('staff_order', 'asc');
        $this->db->limit($limit);

        $staff = $this->db->get()->result_array();

        return $staff;
    }

    public function getStaffByKey($staff_key)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('staff_key', $staff_key);
        $staff = $this->db->get()->row_array();

        $staff = $this->put_related_items_in_staffs($staff);
        return $staff;

    }


    public
    function getStaffType($staff_id)
    {
        $this->db->select('*');
        $this->db->from('staff_type');
        $this->db->where('staff_type_id', $staff_id);
        $this->db->limit(1);

        return $this->db->get()->row_array();
    }

    public
    function getStaffSocialLinks($staff_id)
    {
        $this->db->select('*');
        $this->db->from('staff_social_link');
        $this->db->where('staff_id', $staff_id);

        $this->db->where('staff_social_link_name!=', null);
        $this->db->where('staff_social_link_name!=', '');

        $this->db->where('staff_social_link_url!=', null);
        $this->db->where('staff_social_link_url!=', '');

        return $this->db->get()->result_array();
    }

    public
    function getStaffImages($staff_id)
    {
        $this->db->select('*');
        $this->db->from('staff_image');
        $this->db->where('staff_id', $staff_id);

        $this->db->where('staff_image_name!=', null);
        $this->db->where('staff_image_name!=', '');

        $staff_images = $this->db->get()->result_array();

        if (!empty($staff_images)) {
            $staff_images = array_map("self::put_related_items_in_staff_images", $staff_images);
        }

        return $staff_images;
    }

    public
    function getStaffFeaturedImage($staff_id)
    {
        $this->db->select('*');
        $this->db->from('staff_image');
        $this->db->where('staff_id', $staff_id);

        $this->db->where('staff_image_name!=', null);
        $this->db->where('staff_image_name!=', '');
        $this->db->where('is_staff_image_featured', 1);
        $this->db->limit(1);

        $staff_image = $this->db->get()->row_array();

        $staff_image = $this->put_related_items_in_staff_images($staff_image);

        return $staff_image;
    }

    public
    function put_related_items_in_staff_images($staff_image)
    {

        $staff_image_name = "";
        $staff_image_name_with_path = $this->config->item("default_user_image");
        if (!empty($staff_image)) {
            if (!empty($staff_image['staff_image_name'])) {
                $staff_image_name = $staff_image['staff_image_name'];
                $staff_image_name_with_path = $staff_image['staff_image_name_with_path'] = $this->config->item("staff_source_path") . $staff_image['staff_image_name'];
            }
        }

        $staff_image['staff_image_name'] = $staff_image_name;
        $staff_image['staff_image_name_with_path'] = $staff_image_name_with_path;


        return $staff_image;
    }


    //---------------------------------------------------------Backend----------------------------------------------------

    public
    function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
        return $this->db->insert_id();
    }

    public
    function getAllData($tableName)
    {
        $this->db->select('*');
        $query = $this->db->get($tableName)->result_array();
        return $query;
    }
    public
    function deleteStaffType($staff_type_key)
    {
        $this->db->where('staff_type_key', $staff_type_key);
        return $query = $this->db->delete('staff_type');
    }


    public
    function get_staff_info($id)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('staff_key', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public
    function get_social_link($id)
    {
        $this->db->select('*');
        $this->db->from('staff_social_link');
        $this->db->where('staff_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public
    function updateStaff($data)
    {
        $staff_id = $data['staff_id'];
        $this->db->set('staff_name', $data['staff_name']);
        $this->db->set('staff_type_id', $data['staff_type_id']);
        $this->db->set('staff_description', $data['staff_description']);
        if(isset($data['password'])){
           $this->db->set('password', md5($data['password']));
        }
        $this->db->set('staff_order', $data['staff_order']);
        $this->db->set('is_staff_active', $data['is_staff_active']);
        $this->db->set('staff_updated_at', $data['staff_updated_at']);
        $this->db->where('staff_id', $staff_id);
        return $query = $this->db->update('staff');
    }

    public
    function deletePrevSocialLink($id)
    {
        $this->db->where('staff_id', $id);
        return $query = $this->db->delete('staff_social_link');
    }

    public
    function viewFeatureImage($staff_id)
    {
        $this->db->select('*');
        $this->db->from('staff_image');
        $this->db->where('staff_id', $staff_id);
        $this->db->where('is_staff_image_featured', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public
    function getStaffImage($id)
    {
        $this->db->select('*');
        $this->db->from('staff_image');
        $this->db->where('staff_id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public
    function deletePrevStaffImage($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        return $query = $this->db->delete('staff_image');
    }

    public
    function deleteStaffImage($id)
    {
        $this->db->where('staff_image_id', $id);
        return $query = $this->db->delete('staff_image');
    }


    public
    function getStaffInfo($staff_key)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('staff_key', $staff_key);
        $result = $this->db->get();
        return $result->result_array();
    }


    public
    function deleteStaffSocialLink($staff_id)
    {
        $this->db->where('staff_id', $staff_id);
        return $query = $this->db->delete('staff_social_link');
    }

    public
    function deleteStaff($staff_key)
    {
        $this->db->where('staff_key', $staff_key);
        return $query = $this->db->delete('staff');
    }

    public
    function deleteMassStaffType($all_id)
    {
        $this->db->where_in('staff_type_key', $all_id);
        return $query = $this->db->delete('staff_type');
    }

    public function getAllStaffType(){
        $this->db->select('*');
        $this->db->from('staff_type'); 
        $this->db->where('staff_type_id !=', 0);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getAllStaff(){
        $this->db->select('*');
        $this->db->from('staff'); 
        $this->db->where('staff_type_id !=', 0);
        $this->db->order_by("staff_id", "DESC");
        $result = $this->db->get();
        return $result->result_array();
    }

    public function checkStaffType($StaffTypeName){
        $this->db->select('*');
        $this->db->from('staff_type'); 
        $this->db->where('staff_type_name', $StaffTypeName);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getStaffTypeInfo($staff_type_key){
        $this->db->select('*');
        $this->db->from('staff_type'); 
        $this->db->where('staff_type_key', $staff_type_key);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateStaffType($data)
    {
        $staff_type_key = $data['staff_type_key'];
        $this->db->set('staff_type_permission', $data['staff_type_permission']);
        $this->db->set('staff_type_name', $data['staff_type_name']);
        $this->db->set('staff_type_shortname', $data['staff_type_shortname']);       
        $this->db->set('staff_type_updated_at', $data['staff_type_updated_at']);
        $this->db->where('staff_type_key', $staff_type_key);
        return $query = $this->db->update('staff_type');
    }

    public function checkStaffEmail($email){
        $this->db->select('*');
        $this->db->from('staff'); 
        $this->db->where('email', $email);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getStaffSocialLink($staff_id, $social_link_name){
        $this->db->select('*');
        $this->db->from('staff_social_link'); 
        $this->db->where('staff_id', $staff_id);
        $this->db->where('staff_social_link_name', $social_link_name);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function updateProfile($data)
    {
        $staff_id = $data['staff_id'];
        $this->db->set('staff_name', $data['staff_name']);
        if(isset($data['password'])){
           $this->db->set('password', md5($data['password']));
        }
        $this->db->set('staff_description', $data['staff_description']);
        $this->db->set('staff_updated_at', $data['staff_updated_at']);
        $this->db->where('staff_id', $staff_id);
        return $query = $this->db->update('staff');
    }

    public function getTypeName($staff_type_id){
        $this->db->select('*');
        $this->db->from('staff_type'); 
        $this->db->where('staff_type_id', $staff_type_id);
        $result = $this->db->get();
        return $result->result_array();
    }


    //Code for DataTable
    function get_datatables($table)
    {
        $this->_get_datatables_query($table);
        if($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result_array();
    }

    private function _get_datatables_query($table)
    {
        if($table=='staff_type'){
            $table = 'staff_type';
            $column_order = array('staff_type_name',null); 
            $column_search = array('staff_type_name'); 
            $order = array('staff_type_id' => 'desc');
            $this->db->where('staff_type_id!=', 0);
        }
        if($table=='staff'){
            $table = 'staff';
            $column_order = array('staff_name',null); 
            $column_search = array('staff_name'); 
            $order = array('staff_id' => 'desc');
            $this->db->where('staff_type_id!=', 0);
        }
       
        $this->db->from($table);


        $i = 0;
        foreach ($column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($order))
        {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function count_all($table)
    {
        $this->db->from($table);
        return $this->db->count_all_results();
    }

    function count_filtered($table)
    {
        $this->_get_datatables_query($table);
        $query = $this->db->get();
        return $query->num_rows();
    }

}