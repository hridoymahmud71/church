<div class="tag_post">
  <h3 class="sidebar_title">Tag Post</h3>
  <div class="row">
    <div class="col">
      <ul>
        <?php foreach ($tags as $tag) { ?>
        <?php 
            $blog_by_tag = !empty($tag['blog_tag']) ? base_url() . 'blogs-by-tag/' . $tag['blog_tag'] : '#';
        ?>
        <li><a href="void:javascript(0)"><?php echo $tag['blog_tag'] ?></a></li>
        <?php } ?>
        
      </ul>
    </div>
  </div>
</div>