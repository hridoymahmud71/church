<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
    <div class="banner_content">
        <div class="container text-center">
            <h1>Upcoming  Events</h1>
            <ul class="bradcumb">
                <li><a href="/">Home</a></li>
                <li> / </li>
                <li>Events</li>
            </ul>

        </div>
    </div>
</section>
<style>
    .ajax-load{

        background: #e1e1e1;

        padding: 10px 0px;

        width: 100%;

    }
</style>

 <section id="main_contant">
      <div class="container">
         <div class="site_heading pad50"> 
            <h2>UPCOMING EVENTS</h2>
            <p class="small_titel">Our Church Events Post</p>
          </div>
         
         <div class="events_list">
           
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e1.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>07</h1>
                    <p>Dec</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e6.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>06</h1>
                    <p>Dec</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e5.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>25</h1>
                    <p>Dec</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e4.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>09</h1>
                    <p>Dec</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e2.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>08</h1>
                    <p>Oct</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
            <div class="events_item">
              <div class="row">
                <div class="col-3">
                  <a href="#" class="gradintwhite"><img src="assets/img/e3.jpg" class="img-fluid"></a>
                </div>
                <div class="col-5">
                  <h5><a href="#">Caring About The Community</a></h5>
                  <p class="e_smallt"><i class="fa fa-clock-o" aria-hidden="true"></i> Dec 07 2018 @ 05:31 pm - Dec 31 2020 @ 01:24 pm</p>
                  <p> <i class="fa fa-map-marker" aria-hidden="true"></i> 50 Massachusetts Avenue, Cambridge, MA, United States</p>
                </div>
                <div class="col">
                  <div class="e_date">
                    <h1>12</h1>
                    <p>Feb</p>
                  </div>
                </div>
                <div class="col">
                  <a href="#" class="btn btndark">Register Now</a>
                </div>
              </div>
            </div>
         </div>


      </div>
    </section>

<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path') ?>blog_list_by_tag.js"></script>