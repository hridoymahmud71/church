
<section id="page_title" class="gradintblue">  
       <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid"></div>
      <div class="banner_content">
        <div class="container text-center">
          <h1>Blog</h1>
          <ul class="bradcumb">
            <li><a href="/">Home</a></li>
            <li> / </li>
            <li>Blog</li>
          </ul>
          
        </div>
      </div>  
    </section>

<style>
    .ajax-load{

        background: #e1e1e1;

        padding: 10px 0px;

        width: 100%;

    }
</style>

<section id="main_contant">
      <div class="container">         
          <div class="row">
            <div class="col" id="leftside">
                <?=isset($blog_list_content)?$blog_list_content:""?>

            </div>
            
            <div class="col-4" id="sidebar">    
            <?=isset($recent_post_content)?$recent_post_content:""?>

             <?=isset($tag_content)?$tag_content:""?>
            </div>
          </div>
 
       </div>
    </section>