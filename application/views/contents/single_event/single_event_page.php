<section id="page_title" class="gradintblue">
    <div class="banner_img"><img src="<?= $this->config->item('public_asset_path') ?>img/banner.jpg" class="img-fluid">
    </div>
    <div class="banner_content">
        <div class="container text-center">
            <h1><?= !empty($event) ? $event['event_name'] : '' ?></h1>
            <ul class="bradcumb">
                <li><a href="<?= base_url() ?>">Home</a></li>
                <li> /</li>
                <li>Events</li>
            </ul>

        </div>
    </div>
</section>

<?php if (!empty($event)) { ?>
    <section id="main_contant">
        <div class="container">

            <div class="pad50">


                <div class="row">
                    <div class="col">
                        <div class="gradintblue">
                            <a href="#"><img
                                        src="<?= $event['featured_image']['event_image_name_with_path'] ?>"
                                        class="img-fluid"></a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="gradintwhite">
                            <div class="event_deteils ">


                                <h4>Start Date & Time : </h4>
                                <p>
                                    <?= !empty($event['event_starts']) ? date("M d Y @ h:i:a", strtotime($event['event_starts'])) : 'Unavailable' ?>
                                </p>
                                <h4>End Date & Time :</h4>
                                <p>
                                    <?= !empty($event['event_ends']) ? date("M d Y @ h:i:a", strtotime($event['event_ends'])) : 'Unavailable' ?>

                                </p>
                                <h4>Location : </h4>
                                <p><?= !empty($event['event_location']) ? $event['event_location'] : '' ?></p>
                                <h4>Phone No : </h4>
                                <p><?= !empty($event['event_phone']) ? $event['event_phone'] : '' ?></p>
                                <h4>Website :</h4>
                                <p><?= !empty($event['event_website']) ? $event['event_website'] : '' ?></p>
                                <h4>Email : </h4>
                                <p><?= !empty($event['event_email']) ? $event['event_email'] : '' ?></p>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="text-center">
                <button class="btn btndark" data-toggle="modal" id="register-btn">Register Now</button>
            </div>


            <!--<div class="pad50">
                <div class="row">
                    <div class="col">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14606.947643827823!2d90.42665865000001!3d23.756759450000004!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1543928835257"
                                width="" height="350" frameborder="0" style="border:0; width: 100%;"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>-->
            <div class="pad50">
                <div class="row">
                    <div class="col">
                        <div id="event-map"
                             evt-lat="<?= !empty($event['event_google_map_latitude']) ? $event['event_google_map_latitude'] : '0.000' ?>"
                             evt-lng="<?= !empty($event['event_google_map_longitude']) ? $event['event_google_map_longitude'] : '0.000' ?>"
                             evt-zoom="<?= $event_google_map_zoom_level ?>"
                             evt-title="<?= !empty($event['event_name']) ? $event['event_name'] : 'Unavailable'; ?>"
                        >

                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col">
                    <p><?= !empty($event['event_description']) ? $event['event_description'] : '' ?></p>
                </div>
            </div>

            <div class="row older_post">
                <div class="col">
                    <?php if (!empty($event['prev_event'])) { ?>
                        <a href="<?= !empty($event['prev_event']['event_key']) ? base_url() . 'event/' . $event['prev_event']['event_key'] : '#' ?>"
                           class="btn btndarkborder  pull-left">
                            <?= !empty($event['prev_event']['event_name']) ? $event['prev_event']['event_name'] : 'Previous' ?>
                        </a>
                    <?php } ?>
                    <?php if (!empty($event['next_event'])) { ?>
                        <a href="<?= !empty($event['next_event']['event_key']) ? base_url() . 'event/' . $event['next_event']['event_key'] : '#' ?>"
                           class="btn btndark pull-right ">
                            <?= !empty($event['next_event']['event_name']) ? $event['next_event']['event_name'] : 'Next' ?>
                        </a>
                    <?php } ?>
                </div>

            </div>
        </div>
    </section>
<?php } ?>

<?php

$preload_modal = false;
if (isset($_GET['register'])) {
    if ($_GET['register'] == "yes"){
        $preload_modal = true;
    }
}


?>

<div class="modal fade" preload-modal="<?= $preload_modal ?>" id="bookingform-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="event-booking-form" class="event_booking_form" action="<?= base_url() . '/event_booking' ?>"
                  method="post"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Event Booking</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <h5>Available Seats : 14</h5>
                    <div class="form-group">
                        <label for="exampleInputSpaces">Spaces</label>
                        <input type="number" class="form-control" id="exampleInputSpaces" aria-describedby="SpacesHelp"
                               placeholder="01">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputName">Name</label>
                        <input type="text" class="form-control" id="exampleInputName" aria-describedby="NameHelp"
                               placeholder="User name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPhone">Phone No</label>
                        <input type="tel" class="form-control" id="exampleInputPhone" aria-describedby="PhoneHelp"
                               placeholder="Enter Phone No">
                    </div>


                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Message</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="2"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btndarkborder" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btndark">Send Booking</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?= $google_map_api_key ?>&callback=initMap">
</script>

<script type="text/javascript" src="<?= $this->config->item('public_custom_js_path') ?>single_event.js"></script>