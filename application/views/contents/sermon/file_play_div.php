<div id="append_div">
    <?php 
    $link_type = $sermon_file_info[0]['sermon_file_link_type'];
    $file_type = $sermon_file_info[0]['sermon_file_type'];
    $file = $sermon_file_info[0]['sermon_file']
    ?>

    <?php if($link_type=='local'){ ?>
    <video controls="controls" preload="metadata" width="360" height="280" style="margin: 0 auto; display: block;"><source src="<?php echo $this->config->item('sermon_file_source_path').$file ?>" type="video/mp4"></video>
    <?php } ?>

    <?php if($link_type=='global'){ ?>
        <?php echo $file; ?>
        <iframe src="<?php echo $file; ?>"></iframe>
    <?php } ?>
   
</div>
