<section id="latest_sermon" class=" wow bounceInUp" data-wow-duration="1s" data-wow-delay="1s">
    <div class="container-fluid">

        <div class="site_heading pad50">
            <h2>LATEST SERMON</h2>
            <p class="small_titel">Experience God's Presence</p>
        </div>
        <div class="row no-gutters">
            <div class="col l_post text-center">
                <div class="gradintwhitere">
                    <img src="assets/img/ls1.jpg" class="img-fluid">
                    <div class="ls_contet_l">
                        <h3><a href="#">An Appeal to Charismatic Friends</a></h3>
                        <p>Lorem ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem ipsum ipsum lorem ipsum lorem
                            ipsum ipsum lorem ipsum lorem Lorem ipsum lorem ipsum lorem ...</p>
                        <ul class="sl_link">
                            <li><a href="#"><i class="fa fa-music"></i></a></li>
                            <li><a href="#"><i class="fa fa-film"></i></a></li>
                            <li><a href="#"><i class="fa fa-book"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row no-gutters">
                    <div class="col">
                        <a href="#"><img src="assets/img/ls2.jpg" class="img-fluid"></a>
                    </div>
                    <div class="col">
                        <div class="ls_contet_m">
                            <h3><a href="#">A God Entranced Vision of All Things</a></h3>
                            <ul class="sl_link">
                                <li><a href="#"><i class="fa fa-music"></i></a></li>
                                <li><a href="#"><i class="fa fa-film"></i></a></li>
                                <li><a href="#"><i class="fa fa-book"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col">
                        <div class="ls_contet_m">
                            <h3><a href="#">A God Entranced Vision of All Things</a></h3>
                            <ul class="sl_link">
                                <li><a href="#"><i class="fa fa-music"></i></a></li>
                                <li><a href="#"><i class="fa fa-film"></i></a></li>
                                <li><a href="#"><i class="fa fa-book"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#"><img src="assets/img/ls3.jpg" class="img-fluid"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
