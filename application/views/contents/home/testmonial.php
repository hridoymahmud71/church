<section id="testmonial" class="gradintbluere">
    <div class="container">
        <div class="site_heading pad50">
            <h2>TESTIMONIAL</h2>
            <p class="small_titel">Experience God's Presence</p>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-6 ">
                <div class="ss_testmonial">
                    <?php if (!empty($testimonials)) { ?>
                        <?php foreach ($testimonials as $testimonial) { ?>
                            <div class="testmonial_item">
                                <p>" <?= $testimonial['testimonial_description']?> "</p>
                                <div class="text-center testimonial_image_mr"><img src="<?= $testimonial['testimonial_user_image_with_path']?>" class="rounded" alt="User"></div>
                                <p class="small_titel"><?= $testimonial['testimonial_user_name']?></p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
