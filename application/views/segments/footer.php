<footer>
    <div class="container">
        <div class="suscrible_footer">
            <div class="row justify-content-md-center">
                <div class="col-6 text-center">
                    <h3>SUBSCRIBE NEW LETTER</h3>
                    <form>
                        <div class="form-group">

                            <input type="email" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" placeholder="Your email address...">

                        </div>
                        <button type="submit"><i class="fa fa-long-arrow-right"></i></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h4> QUICK LINK</h4>
                <ul>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">Latest Sermon</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Staff</a></li>
                </ul>
            </div>
            <div class="col">
                <h4> CATEGORIES </h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Donate Now</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Support</a></li>

                </ul>
            </div>
            <div class="col">
                <h4> BRANDS </h4>
                <ul>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Sales</a></li>
                </ul>
            </div>
            <div class="col">
                <div class="footer_contact">
                    <img src="<?= $site_logo ?>" class="img-fluid">
                    <p><i class="fa fa-map-marker"></i> <?= $address?></p>
                    <p><i class="fa fa-envelope-o"></i> <a href="mailto:<?=$email?>"> <?=$email?> </a></p>
                    <p><i class="fa fa-phone"></i> <?=$phone?> </p>
                </div>
            </div>

        </div>
        <div class="copyright">
            <ul class="footer_social">
                <?php
                $social_icons['facebook_link'] = " fa fa-facebook ";
                $social_icons['twitter_link'] = " fa fa-twitter ";
                $social_icons['google_link'] = " fa fa-google-plus ";
                $social_icons['youtube_link'] = " fa fa-youtube-play ";
                ?>
                <?php if (!empty($social_settings)) { ?>
                    <?php foreach ($social_settings as $social_site_link_k => $social_site_link_v) { ?>
                        <?php $icon_class = array_key_exists($social_site_link_k, $social_icons) ? $social_icons[$social_site_link_k] : "" ?>
                        <li><a href="<?= $social_site_link_v ?>"><i class="<?= $icon_class ?>"></i></a></li>
                    <?php } ?>
                <?php } ?>
            </ul>
            <p><?= $footer_text ?></p>
        </div>
    </div>
</footer>