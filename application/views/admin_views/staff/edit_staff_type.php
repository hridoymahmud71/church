
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Staff Type
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="admin/staff-type">Staff Type</a></li>
        <li class="active">Edit Staff Type</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        

            <!-- /.box-header -->
            <div class="box-body">
            <form id="addStaffType"  role="form" action="admin/update-staff-type" method="POST" enctype="multipart/form-data">              
              <div class="row">
                <div class="col-md-6">                  
                        <div class="box-body"> 
                          <div class="form-group">
                            <label for="staff_type_name" class="col-4 col-form-label">Role Name<span class="text-danger">*</span></label>
                            <input type="text" required name="staff_type_name" class="form-control" placeholder="Enter Staff Type" value="<?php echo $staff_type_info[0]['staff_type_name'] ?>">
                          </div>
                           <input type="hidden" name="staff_type_key" value="<?php echo $staff_type_info[0]['staff_type_key'] ?>">                                                 
                        </div>
                        <!-- /.box-body -->      
                </div>                
              </div><hr>
                <div class="box-body">
                 <div class="form-group">
                    <label for="urole_name" class="col-4 col-form-label"> Permission<span class="text-danger"></span></label>
                  </div> 
                

                <!-- user permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_user" name="add_user" <?php if(isset($permission['add_user']) && ($permission['add_user']==1)){echo "checked";} ?>> Add User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_user" name="view_user" <?php if(isset($permission['view_user']) && ($permission['view_user']==1)){echo "checked";} ?>> View User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_user" name="edit_user" <?php if(isset($permission['edit_user']) && ($permission['edit_user']==1)){echo "checked";} ?>> Edit User
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_user" name="delete_user" <?php if(isset($permission['delete_user']) && ($permission['delete_user']==1)){echo "checked";} ?>> Delete User
                  </div>
                </div><br>

                <!-- Gallary permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_gallery" name="add_gallery" <?php if(isset($permission['add_gallery']) && ($permission['add_gallery']==1)){echo "checked";} ?>> Add Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_gallery" name="view_gallery" <?php if(isset($permission['view_gallery']) && ($permission['view_gallery']==1)){echo "checked";} ?>> View Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_gallery" name="edit_gallery" <?php if(isset($permission['edit_gallery']) && ($permission['edit_gallery']==1)){echo "checked";} ?>> Edit Gallery
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_gallery" name="delete_gallery" <?php if(isset($permission['delete_gallery']) && ($permission['delete_gallery']==1)){echo "checked";} ?>> Delete Gallery
                  </div>
                </div><br>

                 <!-- Gallary Image permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_gallery_image" name="add_gallery_image" <?php if(isset($permission['add_gallery_image']) && ($permission['add_gallery_image']==1)){echo "checked";} ?>> Add Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_gallery_image" name="view_gallery_image" <?php if(isset($permission['view_gallery_image']) && ($permission['view_gallery_image']==1)){echo "checked";} ?>> View Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_gallery_image" name="edit_gallery_image" <?php if(isset($permission['edit_gallery_image']) && ($permission['edit_gallery_image']==1)){echo "checked";} ?>> Edit Gallery Image
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_gallery_image" name="delete_gallery_image" <?php if(isset($permission['delete_gallery_image']) && ($permission['delete_gallery_image']==1)){echo "checked";} ?>> Delete Gallery Image
                  </div>
                </div><br>

                <!-- sermon permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_sermon" name="add_sermon" <?php if(isset($permission['add_sermon']) && ($permission['add_sermon']==1)){echo "checked";} ?>> Add Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_sermon" name="view_sermon" <?php if(isset($permission['view_sermon']) && ($permission['view_sermon']==1)){echo "checked";} ?>> View Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_sermon" name="edit_sermon" <?php if(isset($permission['edit_sermon']) && ($permission['edit_sermon']==1)){echo "checked";} ?>> Edit Sermon
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_sermon" name="delete_sermon" <?php if(isset($permission['delete_sermon']) && ($permission['delete_sermon']==1)){echo "checked";} ?>> Delete Sermon
                  </div>
                </div><br>

                <!-- banner permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_banner" name="add_banner" <?php if(isset($permission['add_banner']) && ($permission['add_banner']==1)){echo "checked";} ?>> Add Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_banner" name="view_banner" <?php if(isset($permission['view_banner']) && ($permission['view_banner']==1)){echo "checked";} ?>> View Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_banner" name="edit_banner" <?php if(isset($permission['edit_banner']) && ($permission['edit_banner']==1)){echo "checked";} ?>> Edit Banner
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_banner" name="delete_banner" <?php if(isset($permission['delete_banner']) && ($permission['delete_banner']==1)){echo "checked";} ?>> Delete Banner
                  </div>
                </div><br>

                <!-- blog permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_blog" name="add_blog" <?php if(isset($permission['add_blog']) && ($permission['add_blog']==1)){echo "checked";} ?>> Add Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_blog" name="view_blog" <?php if(isset($permission['view_blog']) && ($permission['view_blog']==1)){echo "checked";} ?>> View Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_blog" name="edit_blog" <?php if(isset($permission['edit_blog']) && ($permission['edit_blog']==1)){echo "checked";} ?>> Edit Blog
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_blog" name="delete_blog" <?php if(isset($permission['delete_blog']) && ($permission['delete_blog']==1)){echo "checked";} ?>> Delete Blog
                  </div>
                </div><br>

                <!-- Staff Type permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_staff_type" name="add_staff_type" <?php if(isset($permission['add_staff_type']) && ($permission['add_staff_type']==1)){echo "checked";} ?>> Add Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_staff_type" name="view_staff_type" <?php if(isset($permission['view_staff_type']) && ($permission['view_staff_type']==1)){echo "checked";} ?>> View Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_staff_type" name="edit_staff_type" <?php if(isset($permission['edit_staff_type']) && ($permission['edit_staff_type']==1)){echo "checked";} ?>> Edit Staff Type
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_staff_type" name="delete_staff_type" <?php if(isset($permission['delete_staff_type']) && ($permission['delete_staff_type']==1)){echo "checked";} ?>> Delete Staff Type
                  </div>
                </div><br>

                 <!-- Staff permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_staff" name="add_staff" <?php if(isset($permission['add_staff']) && ($permission['add_staff']==1)){echo "checked";} ?>> Add Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_staff" name="view_staff" <?php if(isset($permission['view_staff']) && ($permission['view_staff']==1)){echo "checked";} ?>> View Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_staff" name="edit_staff" <?php if(isset($permission['edit_staff']) && ($permission['edit_staff']==1)){echo "checked";} ?>> Edit Staff
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_staff" name="delete_staff" <?php if(isset($permission['delete_staff']) && ($permission['delete_staff']==1)){echo "checked";} ?>> Delete Staff
                  </div>
                </div><br>

                <!-- Testimonial permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_testimonial" name="add_testimonial" <?php if(isset($permission['add_testimonial']) && ($permission['add_testimonial']==1)){echo "checked";} ?>> Add Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_testimonial" name="view_testimonial" <?php if(isset($permission['view_testimonial']) && ($permission['view_testimonial']==1)){echo "checked";} ?>> View Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_testimonial" name="edit_testimonial" <?php if(isset($permission['edit_testimonial']) && ($permission['edit_testimonial']==1)){echo "checked";} ?>> Edit Testimonial
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_testimonial" name="delete_testimonial" <?php if(isset($permission['delete_testimonial']) && ($permission['delete_testimonial']==1)){echo "checked";} ?>> Delete Testimonial
                  </div>
                </div><br>

                <!-- Event permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_event" name="add_event" <?php if(isset($permission['add_event']) && ($permission['add_event']==1)){echo "checked";} ?>> Add Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_event" name="view_event" <?php if(isset($permission['view_event']) && ($permission['view_event']==1)){echo "checked";} ?>> View Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_event" name="edit_event" <?php if(isset($permission['edit_event']) && ($permission['edit_event']==1)){echo "checked";} ?>> Edit Event
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_event" name="delete_event" <?php if(isset($permission['delete_event']) && ($permission['delete_event']==1)){echo "checked";} ?>> Delete Event
                  </div>
                </div><br>

                <!-- Booking permission -->
                <div class="row">                  
                  <div class="col-md-3">
                    <input type="checkbox" id="add_booking" name="add_booking" <?php if(isset($permission['add_booking']) && ($permission['add_booking']==1)){echo "checked";} ?>> Add Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="view_booking" name="view_booking" <?php if(isset($permission['view_booking']) && ($permission['view_booking']==1)){echo "checked";} ?>> View Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="edit_booking" name="edit_booking" <?php if(isset($permission['edit_booking']) && ($permission['edit_booking']==1)){echo "checked";} ?>> Edit Booking
                  </div>

                  <div class="col-md-3">
                    <input type="checkbox" id="delete_booking" name="delete_booking" <?php if(isset($permission['delete_booking']) && ($permission['delete_booking']==1)){echo "checked";} ?>> Delete Booking
                  </div>
                </div><br>

                



                </div>

                <br><div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>                
            </form>

            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })
</script>










  
