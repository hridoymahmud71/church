<form id="editPage"  role="form" action="admin/update-page" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-8">
                              <div class="form-group">
                                <label for="blog_title" class="col-form-label">Page Title<span class="text-danger">*</span></label>
                                <input type="text" name="page_title" class="form-control" required="required" placeholder="Page Title" value="<?php echo $page_info[0]['page_title'] ?>">
                              </div>
                              <input type="hidden" name="page_id" class="form-control" required="required" placeholder="Page ID" value="<?php echo $page_info[0]['page_id'] ?>">

                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="page_slug" class="col-form-label">Page Slug<span class="text-danger">*</span></label>
                                    <input type="text" name="page_slug" class="form-control" required="required" placeholder="Page Slug" value="<?php echo $page_info[0]['page_slug'] ?>">
                                  </div> 
                                  
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="col-form-label">Status<span class="text-danger"></span></label>
                                        <select required="required" name="page_status" class="form-control" style="width: 100%;" aria-invalid="false">
                                              <option value="1" <?php if($page_info[0]['page_status']==1){echo "selected";} ?>>Active</option>                                
                                              <option value="0" <?php if($page_info[0]['page_slug']==0){echo "selected";} ?>>Inactive</option>           
                                          </select>
                                  </div>
                                </div>
                              </div>
                               

                              <div class="form-group">
                                <label for="page_header_image" class="col-form-label">Page Header Image<span class="text-danger"></span></label>
                                <input type="file" onchange="readURL(this);" name="page_header_image" class="form-control" accept=".png, .jpg, .jpeg" placeholder="Page Header Image" >
                              </div>
                            </div>

                            <div class="col-md-4"> <br><br> 

                            <?php if(empty($page_info[0]['page_header_image'])){ ?>
                              <img id="viewHeaderImage_e" src="<?php echo $this->config->item('default_image_placeholder') ?>" style="height: 150px; width: 220px;" alt="your image" />
                              <?php } ?>

                              <?php if(!empty($page_info[0]['page_header_image'])){ ?>
                              <img id="viewHeaderImage_e" src="<?php echo $this->config->item('page_image_source_path').$page_info[0]['page_header_image'] ?>" style="height: 150px; width: 220px;" alt="your image" />
                              <?php } ?>
                            </div>
                          </div>                         

                        <div class="form-group">
                          <label for="page_content" class="col-4 col-form-label">Page Content<span class="text-danger"></span></label>
                          <textarea id="page_content_e" name="page_content" class="form-control" placeholder="Page Content"><?php echo $page_info[0]['page_content'] ?></textarea>
                        </div>
                                     
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                      <script  type="text/javascript" src="custom-admin-javascript/page/page_edit.js"></script>
    