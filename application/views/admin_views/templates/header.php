<header class="main-header">
    <!-- Logo -->
    <a href="admin/dashboard" class="logo">
     <img class="img-responsive" src="<?= $this->settings->site_logo() ?>">
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php  $image_name = $this->permission->getLoggedUserImage($this->session->userdata('id')) ?>
        
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">           

               <?php 
                  if(empty($image_name)){ ?>

                   <img src="<?= $this->config->item('admin_asset_path') ?>dist/img/default_avatar.png" class="user-image" alt="User Image">
                <?php } ?>

                <?php
                  if(!empty($image_name)){ ?>
                   <img src="<?php echo $this->config->item('staff_source_path').$image_name; ?>" class="img-circle" alt="User Image" style="height: 25px; width: 25px;">
                  <?php } ?>


              <span class="hidden-xs"><?= $this->permission->getLoggedUserName($this->session->userdata('id')) ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php                  
                  if(empty($image_name)){ ?>
                    <img src="<?= $this->config->item('admin_asset_path') ?>dist/img/default_avatar.png" class="img-circle" alt="User Image">
                  <?php } ?>

                 <?php
                  if(!empty($image_name)){ ?>
                    <img src="<?php echo $this->config->item('staff_source_path').$image_name; ?>" class="img-circle" alt="User Image">
                  <?php } ?>
                
				        <p>
                  <?= $this->permission->getLoggedUserName($this->session->userdata('id')) ?>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="admin/profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="admin/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->