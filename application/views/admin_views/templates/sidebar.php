    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">


      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION </li>

       <li <?php if(isset($active_link) && ($active_link=='dashboard')){echo 'class="active"';} ?> ><a href="admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

          <li class="treeview <?php if(isset($active_link) && in_array($active_link,array("main_settings","page_settings","payment_settings","social_settings"))){echo "active";} ?>" >
              <a href="#">
                  <i class="fa fa-cog"></i> <span>Settings</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu">
                  <li <?php if(isset($active_link) && ($active_link=='main_settings')){echo 'class="active"';} ?>><a href="admin/main-settings"><i class="fa fa-circle-o"></i>Main Settings</a></li>
                  <li <?php if(isset($active_link) && ($active_link=='page_settings')){echo 'class="active"';} ?>><a href="admin/page-settings"><i class="fa fa-circle-o"></i>Page Settings</a></li>
                  <li <?php if(isset($active_link) && ($active_link=='payment_settings')){echo 'class="active"';} ?>><a href="admin/payment-settings"><i class="fa fa-circle-o"></i>Payment Settings</a></li>
                  <li <?php if(isset($active_link) && ($active_link=='social_settings')){echo 'class="active"';} ?>><a href="admin/social-settings"><i class="fa fa-circle-o"></i>Social Settings</a></li>
              </ul>
          </li>

       <li class="treeview <?php if(isset($active_link) && (($active_link=='user_role') || ($active_link=='user'))){echo "active";} ?>" >
          <a href="#">
            <i class="fa fa-user-o"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!-- <li <?php //if(isset($active_link) && ($active_link=='user_role')){echo 'class="active"';} ?>><a href="admin/user-role"><i class="fa fa-circle-o"></i> User Role</a></li> -->
            <li <?php if(isset($active_link) && ($active_link=='user')){echo 'class="active"';} ?>><a href="admin/user"><i class="fa fa-circle-o"></i>User</a></li>
          </ul>
        </li>


        <li <?php if(isset($active_link) && ($active_link=='gallery')){echo 'class="active"';} ?> ><a href="admin/gallery"><i class="fa fa-file-image-o"></i> <span>Gallery</span></a></li>

        <li <?php if(isset($active_link) && ($active_link=='gallery_image')){echo 'class="active"';} ?>><a href="admin/gallery-images"><i class="fa fa-image"></i> <span>Gallery Images</span></a></li>

          <li <?php if(isset($active_link) && ($active_link=='sermon')){echo 'class="active"';} ?>><a href="admin/sermon"><i class="fa fa-book"></i> <span>Sermons</span></a></li>

        <li <?php if(isset($active_link) && ($active_link=='banner')){echo 'class="active"';} ?> ><a href="admin/slideshow"><i class="fa fa-television"></i> <span>Banner</span></a></li>

         <li <?php if(isset($active_link) && (($active_link=='blog') || ($active_link=='blog_comment'))){echo 'class="active"';} ?> ><a href="admin/blog"><i class="fa fa-comments"></i> <span>Blog</span></a></li>

         <li <?php if(isset($active_link) && (($active_link=='page') || ($active_link=='page'))){echo 'class="active"';} ?> ><a href="admin/page"><i class="fa fa-sticky-note"></i> <span>Pages</span></a></li>

        <li class="treeview <?php if(isset($active_link) && (($active_link=='staff_type') || ($active_link=='staff'))){echo "active";} ?>" >
          <a href="#">
            <i class="fa fa-user-circle-o"></i> <span>Staff</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">           
             <li <?php if(isset($active_link) && ($active_link=='staff_type')){echo 'class="active"';} ?>><a href="admin/staff-type"><i class="fa fa-circle-o"></i> Staff Type</a></li>
            <li <?php if(isset($active_link) && ($active_link=='staff')){echo 'class="active"';} ?>><a href="admin/staff"><i class="fa fa-circle-o"></i>Staff</a></li>
          </ul>
        </li>

         <li <?php if(isset($active_link) && ($active_link=='testimonial')){echo 'class="active"';} ?> ><a href="admin/testimonial"><i class="fa fa-address-book-o"></i> <span>Testimonial</span></a></li>
         <li <?php if(isset($active_link) && (($active_link=='event') || ($active_link=='view_event_booking'))){echo 'class="active"';} ?>><a href="admin/event"><i class="fa fa-calendar"></i> <span>Event</span></a></li>

          <li <?php if(isset($active_link) && ($active_link=='event_booking')){echo 'class="active"';} ?>><a href="admin/event-booking"><i class="fa fa-ticket"></i> <span>Event Booking</span></a></li>

      </ul>
    </section>
    <!-- /.sidebar -->