

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Event Booking
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="admin/event"><i class="fa fa-calendar"></i> Event</a></li>
        <li class="active"> View Event Booking</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
               <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>SL</th>
                  <th>Name</th>
                  <th>Seat Need?</th>
                  <th>Event Name</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
               <tbody>
                    <?php $sl=0; foreach ($view_booking as $booking) { $sl++;?>
                   <tr id="<?php echo $booking['event_booking_key']; ?>">                    

                        <td><?php echo $sl; ?></td>
                       
                        <td><?php echo $booking['event_booking_user_name']; ?></td>
                        <td><?php echo $booking['event_booking_seat']; ?></td>
                        <td><?php echo $booking['event_name']; ?></td>
                        <td>
                          <?php if($booking['event_booking_status']==0){ ?>
                            <span class="btn btn-danger"> Unapproved </span>
                            <?php } ?>
                          <?php if($booking['event_booking_status']==1){ ?> 
                            <span class="btn btn-success"> Approved </span>
                            <?php  } ?>                            
                        </td>

                        <td class="modalOpen"> 
                            <a data-toggle="modal" class="view_event_booking_item" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-eye"></i></a>

                            <a href="javascript:void(0)"><i data-id1="<?php echo $booking['event_booking_key']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_booking"></i></a>
                        </td>
                    </tr>
                   <?php } ?> 
                   
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


              <!-- edit modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Booking Info</h4>
                  </div>
                  <div class="modal-body">
                       <div id="view_booking_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->











  
