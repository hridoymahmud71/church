<form id=""  role="form" action="admin/update-booking-status" method="POST" enctype="multipart/form-data" autocomplete="off">
                        <div class="box-body">
                           <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Name<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_booking_user_name'] ?>
                                </div>
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Phone<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_booking_user_phone'] ?>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Email<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_booking_user_email'] ?>
                                </div>
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Date<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php 
                                  $date=strtotime($booking_info[0]['event_booking_date']);
                                  echo $dateshow=date("M d Y",$date);
                                  ?>
                                </div>
                              </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Seat Need?<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_booking_seat'] ?>
                                </div>
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="form-group">
                                <label class="col-form-label">Event Name<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_name'] ?>
                                </div>
                              </div>
                            </div>
                        </div> 

                        <div class="row">
                            <div class="col-md-9">
                              <div class="form-group">
                                <label class="col-form-label">Message<span class="text-danger"></span></label>
                                <div class="form-control">
                                  <?php echo $booking_info[0]['event_booking_message'] ?>
                                </div>
                              </div>
                            </div>

                             <div class="col-md-3">
                              <div class="form-group">
                                <label class="col-form-label">Status<span class="text-danger"></span></label>
                                
                              <select required="required" name="event_booking_status" class="form-control select2 valid" style="width: 100%;" aria-invalid="false">                                
                                  <option value="0" <?php if($booking_info[0]['event_booking_status']==0){echo "selected";} ?>>Unapproved</option>
                                  <option value="1" <?php if($booking_info[0]['event_booking_status']==1){echo "selected";} ?>>Approved</option>
                              </select>

                              </div>
                              <input name="event_booking_key" type="hidden" value="<?php echo $booking_info[0]['event_booking_key'] ?>">
                            </div>
                        </div>                             

                       
                        
                          </div>

                        <!-- /.box-body -->

                        <div class="box-footer"> 
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>


                     