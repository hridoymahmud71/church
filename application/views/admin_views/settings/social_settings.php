<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Social Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Social Settings</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php if ($this->session->flashdata('validation_errors')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                        <?= $this->session->flashdata('validation_errors') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('image_errors')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i>Image Upload Error!</h4>
                        <?= $this->session->flashdata('image_errors') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('image_successes')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Image Upload Successful!</h4>
                        <?= $this->session->flashdata('image_successes') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('update_success')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Update Successful!</h4>
                        <?= $this->session->flashdata('update_success') ?>
                    </div>
                <?php } ?>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modify Social Settings</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="admin/social-settings"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Facebook Link</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="facebook_link"
                                           placeholder="Facebook Link"
                                           value="<?= isset($social_settings['facebook_link']) ? $social_settings['facebook_link'] : "" ?>"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Twitter Link</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="twitter_link"
                                           placeholder="Twitter Link"
                                           value="<?= isset($social_settings['twitter_link']) ? $social_settings['twitter_link'] : "" ?>"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Google Link</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="google_link"
                                           placeholder="Google Link"
                                           value="<?= isset($social_settings['google_link']) ? $social_settings['google_link'] : "" ?>"
                                    >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Youtube Link</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="youtube_link"
                                           placeholder="Youtube Link"
                                           value="<?= isset($social_settings['youtube_link']) ? $social_settings['youtube_link'] : "" ?>"
                                    >
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-block">Set</button>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<script>


</script>


