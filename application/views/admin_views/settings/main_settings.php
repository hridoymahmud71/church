<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Main Settings
    </h1>
    <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Main Settings</li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <?php if ($this->session->flashdata('validation_errors')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                        <?= $this->session->flashdata('validation_errors') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('image_errors')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i>Image Upload Error!</h4>
                        <?= $this->session->flashdata('image_errors') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('image_successes')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Image Upload Successful!</h4>
                        <?= $this->session->flashdata('image_successes') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('update_success')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i>Update Successful!</h4>
                        <?= $this->session->flashdata('update_success') ?>
                    </div>
                <?php } ?>
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modify Main Settings</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form class="form-horizontal" method="post" action="admin/main-settings"
                          enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Site Name</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="site_name" placeholder="Site Name"
                                           value="<?= isset($main_settings['site_name']) ? $main_settings['site_name'] : "" ?>"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Site Description</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control"
                                              name="site_description"><?= isset($main_settings['site_description']) ? $main_settings['site_description'] : "" ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Address</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="address"
                                              name="address"><?= isset($main_settings['address']) ? $main_settings['address'] : "" ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" name="email" placeholder="email"
                                           value="<?= isset($main_settings['email']) ? $main_settings['email'] : "" ?>"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Phone</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="phone" placeholder="phone"
                                           value="<?= isset($main_settings['phone']) ? $main_settings['phone'] : "" ?>"
                                    >
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="" class="col-sm-4 control-label">Footer Text</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="footer_text"
                                              name="footer_text"><?= isset($main_settings['footer_text']) ? $main_settings['footer_text'] : "" ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group img-form-wrap">
                                <div class="">
                                    <label for="" class="col-sm-4 control-label">Logo</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control image-input" name="site_logo"
                                               id="site_logo">
                                    </div>
                                </div>
                                <div style="clear:both;height: 10px"></div>
                                <div class="">
                                    <label for="" class="col-sm-4 control-label"></label>

                                    <div class="col-sm-8">
                                        <img src="<?= isset($main_settings['site_logo']) ? $main_settings['site_logo'] : "" ?>"
                                             style="border:1px solid #CCCCCC;"
                                             id="site_logo_preview"
                                             class="img-responsive img-preview">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group img-form-wrap">
                                <div class="">
                                    <label for="" class="col-sm-4 control-label">Icon</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control image-input" name="site_icon"
                                               id="site_icon">
                                    </div>
                                </div>
                                <div style="clear:both;height: 10px"></div>
                                <div class="">
                                    <label for="" class="col-sm-4 control-label"></label>

                                    <div class="col-sm-8">
                                        <img src="<?= isset($main_settings['site_icon']) ? $main_settings['site_icon'] : "" ?>"
                                             style="border:1px solid #CCCCCC;"
                                             id="site_icon_preview"
                                             class="img-responsive img-preview">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success btn-block">Set</button>
                                </div>
                            </div>

                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<script>


</script>


