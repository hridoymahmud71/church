<form id="edit_blog"  role="form" action="admin/update-blog" method="POST" enctype="multipart/form-data">
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <div class="form-group">
              <label for="blog_title" class="col-form-label">Blog Title<span class="text-danger">*</span></label>
              <input type="text" name="blog_title" class="form-control" required="required" placeholder="Blog Title" value="<?php echo $blog_info[0]['blog_title'] ?>">
            </div>  
            <input type="hidden" name="blog_id" class="form-control" required="required" placeholder="Blog ID" value="<?php echo $blog_info[0]['blog_id'] ?>">

            <div class="form-group">
              <label for="blog_feature_image" class="col-form-label">Feature Image<span class="text-danger">*</span></label>
              <input type="file" onchange="readURL(this);" name="blog_feature_image" class="form-control" accept=".png, .jpg, .jpeg" placeholder="Blog Feature Image" >
            </div>
          </div>

          <div class="col-md-4">
            <?php if(!empty($blog_info[0]['blog_feature_image'])){ ?>
              <img id="viewFeatureImage_e" src="<?php echo $this->config->item('blog_image_source_path').$blog_info[0]['blog_feature_image'] ?>" style="height: 150px; width: 220px;" alt="your image" />
            <?php } ?>  

            <?php if(empty($blog_info[0]['blog_feature_image'])){ ?>                           
            <img id="viewFeatureImage_e" src="<?php echo $this->config->item('default_image_placeholder') ?>" style="height: 150px; width: 220px;" alt="your image" />
            <?php } ?>
          </div>
        </div>       

      <div class="form-group">
        <label for="blog_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
        <textarea id="blog_description_e" name="blog_description" class="form-control" placeholder="Enter Description"><?php echo $blog_info[0]['blog_description'] ?></textarea>
      </div>

      <div class="form-group">
        <label for="blog_tag" class="col-4 col-form-label">Tag<span class="text-danger"></span></label>
       <select name="blog_tag[]" class="form-control select2" multiple="multiple" data-placeholder="Enter Tag" style="width: 100%;">
         <?php foreach ($all_tag as $tag) { ?>
          <option value="<?php echo $tag['blog_tag'] ?>" selected="selected"><?php echo $tag['blog_tag'] ?></option>
          <?php } ?> 
              </select>
      </div>
                             
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    <script  type="text/javascript" src="custom-admin-javascript/blog/blog_edit.js"></script>
    
     