<form id="edit_comment"  role="form" action="admin/update-blog-comment-status" method="POST" enctype="multipart/form-data">
      <div class="box-body">
            
        <div class="form-group">
          <label class="col-form-label">Name<span class="text-danger"></span></label>
          <div class="form-control">
            <?php echo $comment_info[0]['blog_comment_username'] ?>
          </div>
        </div>
        <input type="hidden" name="blog_comment_key" value="<?php echo $comment_info[0]['blog_comment_key'] ?>">

        <div class="form-group">
          <label class="col-form-label">Email<span class="text-danger"></span></label>
          <div class="form-control">
            <?php echo $comment_info[0]['blog_comment_useremail'] ?>
          </div>
        </div>

        <div class="form-group">
          <label class="col-form-label">Message<span class="text-danger"></span></label>          
            <div style="border: 1px solid #d2d6de; padding: 5px;">
            <?php echo $comment_info[0]['blog_comment_usermessage'] ?>
          </div> 
        </div>

         <div class="form-group">
          <label class="col-form-label">Status<span class="text-danger"></span></label>          
            <div>
              <select required="required" name="blog_comment_status" class="form-control valid" style="width: 100%;" aria-invalid="false">
                    <option value="1" <?php if($comment_info[0]['blog_comment_status']==1){echo "selected";} ?>>Approved</option>                                
                    <option value="0" <?php if($comment_info[0]['blog_comment_status']==0){echo "selected";} ?>>Unapproved</option>
                    
                </select>

            
          </div> 
        </div>





        

      
                             
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
    <script  type="text/javascript" src="custom-admin-javascript/blog/blog_edit.js"></script>
    <script type="text/javascript">
     