
<form id="editSlideshow"  role="form" action="admin/update-banner" method="POST" enctype="multipart/form-data">
    <div class="box-body">                                         

      <div class="row">
            <div class="col-md-8">
               <div class="form-group">
                 <label for="image_name" class="col-form-label">Image<span class="text-danger">*</span></label>
                        <div class="dropzone" id="edit_dropzone">
                            <div class="dz-message" >
                                <h3> Click Here to select images</h3>
                            </div>
                        </div>
              </div>
              <div class="previews2" id="preview2"></div>
            </div>

            <div class="col-md-4" style="margin-top: 5%;">
              <img id="blah" src="<?php echo $this->config->item('banner_source_path').$banner_info[0]['banner_image_name']; ?>" style="height: 100px; width: 100px;">
            </div>
          </div>




      <div class="form-group">
        <label for="banner_image_title" class="col-4 col-form-label">Title<span class="text-danger">*</span></label>
        <input type="text" name="banner_image_title" class="form-control" value="<?php echo $banner_info[0]['banner_image_title'] ?>" placeholder="Enter Title">
      </div>

      <div class="form-group">
        <label for="banner_image_subtitle"  class="col-4 col-form-label">Sub Title<span class="text-danger">*</span></label>
        <input type="text" name="banner_image_subtitle" class="form-control" value="<?php echo $banner_info[0]['banner_image_subtitle'] ?>" placeholder="Enter Subtitle">
      </div>

      <div class="form-group">
        <label for="banner_image_description" class="col-4 col-form-label">Image Description</label>
        <input type="text" name="banner_image_description" class="form-control" value="<?php echo $banner_info[0]['banner_image_description'] ?>" placeholder="Enter Description">
      </div>                          

      <div class="form-group">
        <label for="banner_order" class="col-4 col-form-label">Order<span class="text-danger">*</span></label>
        <input type="number" min="0" name="banner_order" class="form-control" value="<?php echo $banner_info[0]['banner_order'] ?>" placeholder="Enter Order (Ex: 0)">

        <input type="hidden" name="banner_id" class="form-control" value="<?php echo $banner_info[0]['banner_id'] ?>">
      </div>

      <div class="form-group">
        <label for="is_banner_active" class="col-4 col-form-label">Status<span class="text-danger">*</span></label>
        <select required="required" name ="is_banner_active" class="form-control select2" style="width: 100%;">
            <option value="1" <?php if($banner_info[0]['is_banner_active']==1){echo "selected";} ?>>Active</option>
            <option value="0" <?php if($banner_info[0]['is_banner_active']==0){echo "selected";} ?>>Inactive</option>
          </select>
      </div>

      <hr><hr>

      <!-- banner anchor area start -->
      <?php foreach ($banner_anchor_info as $anchorInfo) { ?>

      <div class="form-group2">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_text[]" class="form-control" value="<?php echo $anchorInfo['banner_anchor_text'] ?>" placeholder="Anchor Text">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_url[]" class="form-control" value="<?php echo $anchorInfo['banner_anchor_url'] ?>" placeholder="Anchor URL">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_class[]" class="form-control" value="<?php echo $anchorInfo['banner_anchor_class'] ?>" placeholder="Anchor Class">
            </div>
          </div>

          <div class="col-md-1">
            <p class="delete">x</p>
          </div>
      </div>
      </div>
      <?php } ?>

      <div class="dynamic-element2 form-group2" style="display: none;">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_text[]" class="form-control" placeholder="Anchor Text">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_url[]" class="form-control" placeholder="Anchor URL">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">                                  
              <input type="text" name="banner_anchor_class[]" class="form-control" placeholder="Anchor Class">
            </div>
          </div>

          <div class="col-md-1">
            <p class="delete">x</p>
          </div>
      </div>
      </div>

      <div class="dynamic-anchor2">
        <!-- Dynamic element will be cloned here -->
        <!-- You can call clone function once if you want it to show it a first element-->
      </div>

      <div class="col-md-12">
        <p class="add-one2">+ Add Banner Anchor</p>
      </div>

      <!-- banner anchor area end -->
                           
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>

    <script  type="text/javascript" src="custom-admin-javascript/banner/banner_edit.js"></script>





