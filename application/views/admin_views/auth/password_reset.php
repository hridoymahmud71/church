<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo base_url() ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>:: Church ::.</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="<?= $this->config->item('admin_asset_path') ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="<?= $this->config->item('admin_asset_path') ?>bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet"
          href="<?= $this->config->item('admin_asset_path') ?>bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= $this->config->item('admin_asset_path') ?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= $this->config->item('admin_asset_path') ?>plugins/iCheck/square/blue.css">

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <h4 class="text-center"> Password Reset </h4><img class="img-responsive" style="margin: 0 auto; display: block;"
                                                          src="<?= $this->settings->site_logo() ?>"><br>
        <?php $this->load->view('admin_views/auth/session_msg'); ?>
        <form action="admin/updateresetpassword" method="post" id="updateresetpassword">
            <div class="form-group has-feedback">
               <input class="form-control" type="password"  placeholder="Enter New Password" name="new_pass" id="new_pass" min-length="6" required>

               <input type="hidden" name="email" value="<?php echo $email;?>">
               <input type="hidden" name="authCode" value="<?php echo $authCode;?>">
               
            </div>
            <div class="row">
                <div class="col-xs-8">

                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                </div>
                <!-- /.col -->
            </div>
        </form><br>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?= $this->config->item('admin_asset_path') ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= $this->config->item('admin_asset_path') ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= $this->config->item('admin_asset_path') ?>plugins/iCheck/icheck.min.js"></script>
<!-- jquery validation -->
<script type="text/javascript" src="<?= $this->config->item('admin_asset_path') ?>jQueryValidation/jquery.validate.min.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#updateresetpassword").validate({
            rules: {
                new_pass: {
                    required: true,
                    minlength: 6,
                },
            }
        });
    })

    $('#updateresetpassword').on('submit', function () {
        $("#updateresetpassword").valid();
    });
</script>

</body>
</html>
