
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sermons
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Sermons</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"> Add Sermon</button>
           <!-- <span class="delete_all btn btn-danger"> Delete All </span> -->
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <!-- <th>
                    <input type="checkbox" id="master">
                  </th> -->
                  <th>SL</th>
                  <th>Image</th>
                  <th>Name</th>                  
                  <th>Action</th>
                </tr>
                </thead>
               <tbody>
                    <?php $sl=0; foreach ($all_sermon as $sermon) { $sl++;?>
                   <tr id="<?php echo $sermon['sermon_key']; ?>">
                        <!-- <td>
                          <input type="checkbox" class="sub_chk" data-id="<?php //echo  $sermon['sermon_key']; ?>">
                        </td> -->

                        <td><?php echo $sl; ?></td>
                         <td id="image_name">
                            <img src="<?php echo $this->config->item('sermon_file_source_path').$sermon['sermon_feature_image']; ?>" style="height: 40px; width: 40px;">
                        </td>  
                        <td id="gallery_category_name"><?php echo $sermon['sermon_title']; ?></td>

                        <td class="modalOpen">                          

                            <a data-toggle="modal" class="edit_sermon" data-target="#exampleModal2" href="javascript:void(0)"><i style="margin-right:10px; font-size: 16px;" data-toggle="tooltip" title="Edit" class="fa fa-pencil"></i></a>

                            <a href="javascript:void(0)"><i data-delete_sermon="<?php echo $sermon['sermon_key']; ?>" style="font-size: 16px;" data-toggle="tooltip" title="Delete" class="fa fa-trash btn_delete_sermon"></i></a>
                        </td>
                    </tr>
                   <?php } ?> 
                   
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->


            <!-- add modal start -->

            <div class="modal fade" id="modal-default">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Sermon</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addSermon"  role="form" action="admin/sermon" method="POST" enctype="multipart/form-data">
                        <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_title" class="col-form-label">Sermon Title<span class="text-danger"></span></label>
                                <input type="text" name="sermon_title" class="form-control" required="required" placeholder="Sermon Title">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="staff_id" class="col-form-label">Pastor<span class="text-danger"></span></label>
                                <select required="required" name ="staff_id" class="form-control select2" style="width: 100%;">
                                <option value="">---</option>
                                <?php foreach ($all_pastor as $staff) { ?>
                                <option value="<?php echo $staff['staff_id'] ?>"><?php echo $staff['staff_name'] ?></option>
                                <?php } ?>
                              </select>
                              </div>                            
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="sermon_description" class="col-form-label">Sermon Description<span class="text-danger"></span></label>
                            <textarea type="text" id="sermon_description" name="sermon_description" class="form-control" placeholder="Sermon Description"></textarea>
                        </div>

                         <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_feature_image" class="col-form-label">Feature Image<span class="text-danger"></span></label>
                                <input type="file" name="sermon_feature_image" class="form-control" required="required" accept=".png, .jpg, .jpeg" placeholder="Sermon Feature Image">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="sermon_order" class="col-form-label">Order<span class="text-danger"></span></label>
                                    <input type="number" name="sermon_order" class="form-control" required="required" placeholder="Sermon Order">
                                  </div>    
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="is_sermon_featured" class="col-form-label">Sermon Featurd?<span class="text-danger"></span></label>
                                      
                                      <select required="required" name ="is_sermon_featured" class="form-control" style="width: 100%;">
                                        <option value="">---</option>         
                                        <option value="1">YES</option>
                                        <option value="0">NO</option>           
                                      </select>

                                  </div>  
                                </div>
                              </div>                                                      
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_video" class="col-form-label">Sermon Video<span class="text-danger"></span></label>  
                                
                                <div class="row">
                                  <div class="col-md-4">
                                    <input id="sermonVideoLocal" type="radio" name="sermon_video_type" value="local"> Upload
                                  </div>

                                  <div class="col-md-6">
                                    <input id="sermonVideoGlobal" type="radio" name="sermon_video_type" value="global"> URL
                                  </div>
                                </div>                                
                              </div> 
                              <span id="sermonVideoUploadDiv" style="display: none;">
                               <input type="file" name="sermon_video" class="form-control" accept="video/mp4,video/x-m4v,video/*" placeholder="Sermon Video">                           
                              </span> 
                              <span id="sermonVideoUrlDiv" style="display: none;">
                               <input type="text" name="sermon_video" class="form-control" placeholder="Enter URL">                           
                              </span>                              
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_audio" class="col-form-label">Sermon Audio<span class="text-danger"></span></label>  
                                
                                <div class="row">
                                  <div class="col-md-4">
                                    <input id="sermonAudioLocal" type="radio" name="sermon_audio_type" value="local"> Upload
                                  </div>

                                  <div class="col-md-6">
                                    <input id="sermonAudioGlobal" type="radio" name="sermon_audio_type" value="global"> URL
                                  </div>
                                </div>                                
                              </div> 
                              <span id="sermonAudioUploadDiv" style="display: none;">
                               <input type="file" name="sermon_audio" accept=".mp3, .ogg" class="form-control" placeholder="Sermon Audio">                           
                              </span> 
                              <span id="sermonAudioUrlDiv" style="display: none;">
                               <input type="text" name="sermon_audio" class="form-control" placeholder="Enter URL">                           
                              </span>                              
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group"><br>
                                <label for="sermon_file" class="col-form-label">Sermon File<span class="text-danger"></span></label>
                                <input type="file" name="sermon_file" class="form-control" accept=".doc, .pdf">
                              </div>
                            </div>

                            <div class="col-md-6">
                               <div class="form-group"><br>
                                <label for="sermon_tag" class="col-form-label">Tags<span class="text-danger"></span></label>
                                <select name="sermon_tag[]" class="form-control select2" multiple="multiple" data-placeholder="Enter Tag" style="width: 100%;"> 
                                s</select>
                              </div>                                                    
                            </div>
                        </div>                        

                       <!--  <video controls="controls" preload="metadata" width="360" height="280"><source src="https://www.youtube.com/watch?v=5Peo-ivmupE" type="video/mp4"></video>
                          <iframe src="https://www.youtube.com/watch?v=5Peo-ivmupE"></iframe> -->
                        
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


              <!-- edit modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Sermon</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_sermon_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->





          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->

  <script>
  $(function () {
    $('#example1').DataTable()
   
  })


</script>

<script type="text/javascript">
  $(document).ready(function () {
        //CKEDITOR.replace('sermon_description');
});
</script>

<script type="text/javascript">
  $(document).ready(function(){
      $('.select2').select2({
        tags: true,
          tokenSeparators: [',', ' ']
      })
}); 
</script>










  
