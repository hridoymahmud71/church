<form id="editSermon"  role="form" action="admin/update-sermon" method="POST" enctype="multipart/form-data"> <?php //echo "<pre>"; print_r($sermon_file); die; ?>
                        <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_title" class="col-form-label">Sermon Title<span class="text-danger"></span></label>
                                <input type="text" name="sermon_title" value="<?php echo $sermon_info[0]['sermon_title'] ?>" class="form-control" required="required" placeholder="Sermon Title">
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="staff_id" class="col-form-label">Pastor<span class="text-danger"></span></label>
                                <select required="required" name ="staff_id" class="form-control select2" style="width: 100%;">
                                <option value="">---</option>
                                <?php foreach ($all_pastor as $staff) { ?>
                                <option value="<?php echo $staff['staff_id'] ?>" <?php if($staff['staff_id']==$sermon_info[0]['staff_id']){echo "SELECTED";} ?>><?php echo $staff['staff_name'] ?></option>
                                <?php } ?>
                                </select>
                              </div>                            
                            </div>
                        </div>

                        <div class="form-group">
                          <label for="sermon_description" class="col-form-label">Sermon Description<span class="text-danger"></span></label>
                            <textarea type="text" id="sermon_description" name="sermon_description" class="form-control" placeholder="Sermon Description"><?php echo $sermon_info[0]['sermon_description'] ?></textarea>
                        </div>

                         <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="sermon_feature_image" class="col-form-label">Feature Image<span class="text-danger"></span></label>
                                <input type="file" name="sermon_feature_image" class="form-control" accept=".png, .jpg, .jpeg" placeholder="Sermon Feature Image">
                              </div>
                            </div>
                            <div class="col-md-2"><br>
                              <?php if(!empty($sermon_info[0]['sermon_feature_image'])){ ?>
                               <img src="<?php echo $this->config->item('sermon_file_source_path').$sermon_info[0]['sermon_feature_image']; ?>" style="height: 50px;">
                              <?php } ?>
                            </div>
                            
                             <div class="col-md-6">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="sermon_order" class="col-form-label">Order<span class="text-danger"></span></label>
                                    <input type="number" name="sermon_order" value="<?php echo $sermon_info[0]['sermon_order'] ?>" class="form-control" required="required" placeholder="Sermon Order">
                                  </div>                            
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="is_sermon_featured" class="col-form-label">Sermon Featurd?<span class="text-danger"></span></label>
                                      
                                      <select required="required" name ="is_sermon_featured" class="form-control" style="width: 100%;">
                                        <option value="">---</option>         
                                        <option value="1" <?php if($sermon_info[0]['is_sermon_featured']==1){echo "SELECTED";} ?>>YES</option>
                                        <option value="0" <?php if($sermon_info[0]['is_sermon_featured']==0){echo "SELECTED";} ?>>NO</option>           
                                      </select>

                                  </div>  
                                </div>
                              </div>                                                      
                            </div>






                            
                        </div>

                        


                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_video" class="col-form-label">Sermon Video<span class="text-danger"></span></label>
                                
                                <div class="row">
                                  <div class="col-md-4">
                                    <input id="sermonVideoLocalE" type="radio" <?php if(!empty($sermon_video) && $sermon_video[0]['sermon_file_link_type']=='local'){ echo "checked"; } ?> name="sermon_video_type" value="local"> Upload
                                  </div>

                                  <div class="col-md-6">
                                    <input id="sermonVideoGlobalE" type="radio" name="sermon_video_type" value="global" <?php if(!empty($sermon_video) && $sermon_video[0]['sermon_file_link_type']=='global'){ echo "checked"; } ?> > URL
                                  </div>
                                </div>                                 

                              </div> 
                              <span id="sermonVideoUploadDivE" <?php if((!empty($sermon_video) && $sermon_video[0]['sermon_file_link_type']=='global') || empty($sermon_video)){ ?> style="display: none;" <?php } ?>>
                               <input type="file" name="sermon_video" class="form-control" accept="video/mp4,video/x-m4v,video/*" placeholder="Sermon Video"> 

                                <?php if(!empty($sermon_video) && (!empty($sermon_video[0]['sermon_file'])) && ($sermon_video[0]['sermon_file_link_type']=='local')){ ?>
                                    <a href="<?php echo $this->config->item('sermon_file_source_path').$sermon_video[0]['sermon_file'] ?>" download> Click Here </a>                                     
                                    <?php } ?>                                
                              </span> 


                              <span id="sermonVideoUrlDivE" <?php if((!empty($sermon_video) && $sermon_video[0]['sermon_file_link_type']=='local') || empty($sermon_video)){ ?> style="display: none;" <?php } ?>>
                               <input type="text" name="sermon_video" class="form-control" placeholder="Enter URL" <?php if(!empty($sermon_video) && (!empty($sermon_video[0]['sermon_file'])) && ($sermon_video[0]['sermon_file_link_type']=='global')){ ?> value="<?php echo $sermon_video[0]['sermon_file']; ?>" <?php } ?>>
                              </span>                              
                            </div>                               

                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="sermon_audio" class="col-form-label">Sermon Audio<span class="text-danger"></span></label>
                                <div class="row">
                                  <div class="col-md-4">
                                    <input id="sermonAudioLocalE" type="radio" name="sermon_audio_type" value="local"<?php if(!empty($sermon_audio) && $sermon_audio[0]['sermon_file_link_type']=='local'){ echo "checked"; } ?> <?php if(!empty($sermon_video) && $sermon_video[0]['sermon_file_link_type']=='local'){ echo "checked"; } ?>> Upload
                                  </div>

                                  <div class="col-md-6">
                                    <input id="sermonAudioGlobalE" type="radio" name="sermon_audio_type" value="global" <?php if(!empty($sermon_audio) && $sermon_audio[0]['sermon_file_link_type']=='global'){ echo "checked"; } ?>> URL
                                  </div>
                                </div>                                
                              </div> 
                              <span id="sermonAudioUploadDivE" <?php if((!empty($sermon_audio) && $sermon_audio[0]['sermon_file_link_type']=='global') || empty($sermon_audio)){ ?> style="display: none;" <?php } ?>>
                               <input type="file" name="sermon_audio" accept=".mp3, .ogg" class="form-control" placeholder="Sermon Audio"> 
                               
                               <?php if(!empty($sermon_audio) && (!empty($sermon_audio[0]['sermon_file'])) && ($sermon_audio[0]['sermon_file_link_type']=='local')){ ?>
                                    <a href="<?php echo $this->config->item('sermon_file_source_path').$sermon_audio[0]['sermon_file'] ?>" download> Click Here </a> 
                                    
                                    <?php } ?>   

                              </span> 
                              <span id="sermonAudioUrlDivE" <?php if((!empty($sermon_audio) && $sermon_audio[0]['sermon_file_link_type']=='local') || empty($sermon_audio)){ ?> style="display: none;" <?php } ?>>
                               <input type="text" name="sermon_audio" class="form-control" placeholder="Enter URL" <?php if(!empty($sermon_audio) && (!empty($sermon_audio[0]['sermon_file'])) && ($sermon_audio[0]['sermon_file_link_type']=='global')){ ?> value="<?php echo $sermon_audio[0]['sermon_file']; ?>" <?php } ?>>                           
                              </span>                              
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                              <div class="form-group"><br>
                                <label for="sermon_file" class="col-form-label">Sermon File<span class="text-danger"></span></label>
                                <input type="file" name="sermon_file" class="form-control" accept=".doc, .pdf">
                              </div>

                              <?php if(!empty($sermon_file) && (!empty($sermon_file[0]['sermon_file'])) && ($sermon_file[0]['sermon_file_link_type']=='local')){ ?>                                   
                                    <a href="<?php echo $this->config->item('sermon_file_source_path').$sermon_file[0]['sermon_file'] ?>" download> Click Here </a> 
                                    <?php } ?>   
                            </div>

                            <div class="col-md-6">
                               <div class="form-group"><br>
                                <label for="sermon_tag" class="col-form-label">Tags<span class="text-danger"></span></label>
                                <select name="sermon_tag[]" class="form-control select3" multiple="multiple" data-placeholder="Enter Tag" style="width: 100%;"> 
                                  <?php foreach ($all_tag as $tag) { ?>
                                  <option value="<?php echo $tag['sermon_tag'] ?>" selected="selected"><?php echo $tag['sermon_tag'] ?></option>
                                  <?php } ?>
                                </select>                                                   
                            </div>
                                                    
                            </div>
                        </div>
                                               
                        </div>
                        <!-- /.box-body -->
                        <input type="hidden" name="sermon_id" class="form-control" value="<?php echo $sermon_file[0]['sermon_id']; ?>">
                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                       <script  type="text/javascript" src="custom-admin-javascript/sermon/sermon_edit.js"></script>

                       <script type="text/javascript">
                          $(document).ready(function(){      
                              $("#editSermon").validate({});
                          })


                           $('#editSermon').on('submit', function(){
                             $("#editSermon").valid();
                           });
                       </script>

                       <script type="text/javascript">
                            $(document).ready(function(){
                                $('.select3').select2({
                                  tags: true,
                                    tokenSeparators: [',', ' ']
                                })
                          }); 
                          </script>
