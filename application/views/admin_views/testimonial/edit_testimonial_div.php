<form id="editTestimonial"  role="form" action="admin/update-testimonial" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                         
                          <div class="row">
                            <div class="col-md-8">                            

                          <div class="form-group">
                             <label for="image_name" class="col-form-label">User Image<span class="text-danger"> </span></label>
                                    <div class="dropzone" id="edit_dropzone">
                                        <div class="dz-message" >
                                            <h3> Click Here to select images</h3>
                                        </div>
                                    </div>
                          </div>

                           <div class="previews2" id="preview2"></div>

                            </div>

                            <div class="col-md-4" style="margin-top: 5%;">
                              <?php if(!empty($testimonialInfo[0]['testimonial_user_image'])){ ?>
                              <img src="<?php echo $this->config->item('testimonial_user_source_path').$testimonialInfo[0]['testimonial_user_image']; ?>" style="height: 100px; width: 100px;">
                              <?php } ?>
                            </div>
                          </div>

                          <div class="form-group">
                            <label for="testimonial_user_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="testimonial_user_name" class="form-control" required="required" placeholder="Enter Name" value="<?php echo $testimonialInfo[0]['testimonial_user_name']; ?>">
                          </div>
                          <input type="hidden" name="testimonial_key" class="form-control" required="required" placeholder="Enter key" value="<?php echo $testimonialInfo[0]['testimonial_key']; ?>">

                         <div class="form-group">
                            <label for="testimonial_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
                            <textarea type="text" required="required" name="testimonial_description" class="form-control" placeholder="Enter Description" ><?php echo $testimonialInfo[0]['testimonial_description']; ?></textarea>
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                      <script  type="text/javascript" src="custom-admin-javascript/testimonial/testimonial_edit.js"></script>