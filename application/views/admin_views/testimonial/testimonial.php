

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Testimonial
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Teestimonial</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <?php $this->load->view('admin_views/session_msg'); ?>

      <!-- Default box -->
      <div class="box">
        <div class="box-header">
           <?php $permission = $this->permission->hasAccess(array('add_testimonial','view_testimonial','edit_testimonial','delete_testimonial')); ?>

          <?php if($permission['add_testimonial']==1){ ?> 
           <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-default"> Add Testimonial </button>
          <?php } ?>

          <?php if($permission['delete_testimonial']==1){ ?>
           <span class="delete_all btn btn-danger"> Delete All </span>
           <?php } ?>
          </div>

          <?php if($permission['view_testimonial']==1){ ?> 
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="testimonial_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                 <thead>
                   <tr>                  
                      <th>
                        <input type="checkbox" id="master">
                      </th>
                      <th>SL</th>
                      <th>Image</th>
                      <th>Name</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                 </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <?php } ?>


            <?php if($permission['add_testimonial']==1){ ?> 
            <!-- add modal start -->
            <div class="modal fade" id="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Testimonial</h4>
                  </div>
                  <div class="modal-body">
                     <form id="addTestimonial"  role="form" action="admin/testimonial" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                         
                          <div class="form-group">
                             <label for="image_name" class="col-form-label">User Image<span class="text-danger"></span></label>
                                    <div class="dropzone" >
                                            <div class="dz-message" >
                                                <h3> Click Here to select images</h3>
                                            </div>
                                    </div>
                          </div>

                           <div class="previews" id="preview"></div>

                          <div class="form-group">
                            <label for="testimonial_user_name" class="col-4 col-form-label">Name<span class="text-danger">*</span></label>
                            <input type="text" name="testimonial_user_name" class="form-control" required="required" placeholder="Enter Name">
                          </div>

                         <div class="form-group">
                            <label for="testimonial_description" class="col-4 col-form-label">Description<span class="text-danger">*</span></label>
                            <textarea type="text" required="required" name="testimonial_description" class="form-control" placeholder="Enter Description"></textarea>
                          </div>
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->\
            <?php } ?>

            <?php if($permission['edit_testimonial']==1){ ?> 
            <!-- add modal start -->
            <div class="modal fade" id="exampleModal2">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Testimonial</h4>
                  </div>
                  <div class="modal-body">
                       <div id="edit_testimonial_div">
                       </div>
                  </div>
                
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <?php } ?>
          </div>
          <!-- /.box -->

    </section>
    <!-- /.content -->
