 <form id="editUserRole"  role="form" action="admin/update-user-role" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                           <div class="form-group">
                            <label for="user_role_name" class="col-4 col-form-label">Role Name<span class="text-danger">*</span></label>
                            <input type="text" required name="user_role_name" class="form-control" placeholder="Enter Role Name" value="<?php echo $user_role_name; ?>" >
                          </div>   

                          <input type="hidden" name="user_role_id" value="<?php echo $user_role_id; ?>">  
                          <hr>

                          <div class="form-group">
                            <label for="user_role_name" class="col-4 col-form-label">Permission<span class="text-danger"></span></label>
                          </div> 

                          <div class="row">
                            <div class="col-md-3">
                              <input type="checkbox" id="add_sermon" name="add_sermon" <?php if($permission->add_sermon){echo "checked";} ?>> Add Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="view_sermon" name="view_sermon" <?php if($permission->view_sermon){echo "checked";} ?>> View Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="edit_sermon" name="edit_sermon" <?php if($permission->edit_sermon){echo "checked";} ?>> Edit Sermon
                            </div>

                            <div class="col-md-3">
                              <input type="checkbox" id="delete_sermon" name="delete_sermon" <?php if($permission->delete_sermon){echo "checked";} ?>> Delete Sermon
                            </div>

                          </div>                         
                                               
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </form>

                    <script  type="text/javascript" src="custom-admin-javascript/user/edit_user_role.js"></script>