<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');




$config['google_map_api_key'] = 'AIzaSyC9x8mCn5-P8XUl59uGqwmmcU6Alt1qza8';
$config['public_custom_js_path'] =  BASE_URL . 'custom-javascript/';
$config['admin_custom_js_path'] = BASE_URL . 'custom-admin-javascript/';

$config['public_asset_path'] = BASE_URL . 'assets/';
$config['admin_asset_path'] = BASE_URL . 'admin_assets/';

$config['settings_upload_path'] = FILE_UPLOAD_PATH . '/settings';
$config['settings_source_path'] = FILE_SOURCE_PATH . 'settings/';

$config['banner_upload_path'] = FILE_UPLOAD_PATH . '/banner';
$config['banner_source_path'] = FILE_SOURCE_PATH . 'banner/';

$config['gallery_upload_path'] = FILE_UPLOAD_PATH . '/gallery';
$config['gallery_source_path'] = FILE_SOURCE_PATH . 'gallery/';

$config['event_upload_path'] = FILE_UPLOAD_PATH . '/event';
$config['event_source_path'] = FILE_SOURCE_PATH . 'event/';

$config['staff_upload_path'] = FILE_UPLOAD_PATH . '/staff';
$config['staff_source_path'] = FILE_SOURCE_PATH . 'staff/';

$config['testimonial_user_upload_path'] = FILE_UPLOAD_PATH . '/testimonial_user';
$config['testimonial_user_source_path'] = FILE_SOURCE_PATH . 'testimonial_user/';

$config['admin_upload_path'] = FILE_UPLOAD_PATH . '/admin';
$config['admin_source_path'] = FILE_SOURCE_PATH . 'admin/';

$config['user_upload_path'] = FILE_UPLOAD_PATH . '/user';
$config['user_source_path'] = FILE_SOURCE_PATH . 'user/';

$config['sermon_file_upload_path'] = FILE_UPLOAD_PATH . '/sermon';
$config['sermon_file_source_path'] = FILE_SOURCE_PATH . 'sermon/';

$config['blog_image_upload_path'] = FILE_UPLOAD_PATH . '/blog';
$config['blog_image_source_path'] = FILE_SOURCE_PATH . 'blog/';

$config['page_image_upload_path'] = FILE_UPLOAD_PATH . '/page';
$config['page_image_source_path'] = FILE_SOURCE_PATH . 'page/';

$config['api_key'] = '31the-52sun-65the-71moon-89and-13the-18earth';

$config['default_image_folder'] = BASE_URL . 'default-images/';
$config['default_user_image'] = $config['default_image_folder'] . 'man-user.png';
$config['default_image_placeholder'] = $config['default_image_folder'] . 'default-image.png';