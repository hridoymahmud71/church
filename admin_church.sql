-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2019 at 09:26 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_church`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `admin_key` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '0 for Super Admin, 1 for Admin',
  `status` int(11) NOT NULL DEFAULT '1',
  `authcode` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_key`, `username`, `email`, `name`, `image`, `password`, `role`, `status`, `authcode`, `created_at`, `updated_at`) VALUES
(3, 'admin_1113e0e460371ee99156', 'admin', 'admin@admin.com', 'admin', 'da1a6ce439921d0656be36177ec5a318.jpg', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, '', '2018-12-25 10:33:09', '2018-12-27 14:50:25'),
(4, 'admin_1113e0e460371ee991a7', 'testadmin', 'bipulsarkar7@gmail.com', 'testadmin', 'da1a6ce439921d0656be36177ec5a318.jpg', 'fcea920f7412b5da7be0cf42b8c93759', 1, 1, '', '2018-12-25 10:33:09', '2019-01-10 12:51:52');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` bigint(20) NOT NULL,
  `banner_key` varchar(255) DEFAULT NULL,
  `banner_image_name` text,
  `banner_image_title` text,
  `banner_image_subtitle` text,
  `banner_image_description` text,
  `banner_created_at` timestamp NULL DEFAULT NULL,
  `banner_updated_at` timestamp NULL DEFAULT NULL,
  `is_banner_active` tinyint(4) NOT NULL,
  `banner_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_key`, `banner_image_name`, `banner_image_title`, `banner_image_subtitle`, `banner_image_description`, `banner_created_at`, `banner_updated_at`, `is_banner_active`, `banner_order`) VALUES
(1, 'image_sdfsdf779', '5c0fbb7d6b34e_property21.jpg', 'Whats in a name', 'Whats in a name Shakespeare says', 'Des1', '2018-12-11 08:28:29', NULL, 1, 2),
(3, 'image_df4a358', 'aiopjdioasjdasoidjnasd.jpg', 'Whats in a name', NULL, 'sfdsf', '2018-12-11 09:04:56', NULL, 0, 0),
(4, 'image_733e84a', 'aoodsdoasidnason.jpg', 'Test Title2', 'Whats in a name Shakespeare says', 'fgdfg', '2018-12-11 09:08:56', '2019-01-16 07:16:54', 1, 3),
(5, 'image_3dcde32', 'dasdasdjaodinjoi9878n.jpg', 'Whats in a name', 'Whats in a name Shakespeare says', 'sdfsdf', '2018-12-11 09:09:31', NULL, 0, 0),
(6, 'image_182c52b', 'dhj9d8uasd98ajsdasnjd9asdhyada.jpg', 'Whats in a name', NULL, 'test des', '2018-12-11 23:22:11', NULL, 0, 0),
(7, 'image_44031d3', 'eqw889qqiunqwijub8qw78qw.jpg', 'Whats in a name', 'Whats in a name Shakespeare says', 'abcdef', '2018-12-11 23:31:43', NULL, 1, 5),
(8, 'image_fa3ea75', '5c108fd721590_abandoned-forest-industry-34950.jpg', 'Whats in a name', 'Whats in a name Shakespeare says', 'zzz', '2018-12-11 23:34:31', NULL, 1, 4),
(9, 'image_7ecd7e6', 'hja76yd9aydh3e3edddd.jpg', 'Whats in a name', NULL, 'aaaa', '2018-12-11 23:40:45', NULL, 1, 1),
(10, 'image_81ecab0', 'jiaijasdoijad9ada9osdj9dhj89dhjasjdn.jpg', 'Title Test', 'Whats in a name Shakespeare says', 'rtrtrt', '2018-12-11 23:52:21', '2019-01-16 07:16:39', 0, 0),
(12, 'image_c55e5157acf017579099', '099fe323ff653070a7fee5e17fa6f286.jpg', 'TitleSlidee99', 'subTitle99', 'descrption998', '2018-12-20 04:26:44', NULL, 1, 44);

-- --------------------------------------------------------

--
-- Table structure for table `banner_anchor`
--

CREATE TABLE `banner_anchor` (
  `banner_anchor_id` bigint(20) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `banner_anchor_text` text,
  `banner_anchor_url` text,
  `banner_anchor_class` text,
  `banner_anchor_created_at` timestamp NULL DEFAULT NULL,
  `banner_anchor_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_anchor`
--

INSERT INTO `banner_anchor` (`banner_anchor_id`, `banner_id`, `banner_anchor_text`, `banner_anchor_url`, `banner_anchor_class`, `banner_anchor_created_at`, `banner_anchor_updated_at`) VALUES
(1, 1, 'Go', 'contact', 'light', '2018-12-11 08:28:29', NULL),
(3, 2, 'Then', 'about', 'light', '2018-12-11 09:04:56', NULL),
(4, 2, 'Pray', 'show/details', 'light', '2018-12-11 09:08:56', NULL),
(5, 3, 'Eat', 'about/us', 'light', '2018-12-11 09:09:31', NULL),
(6, 5, 'Pray', 'show/map', 'light', '2018-12-11 23:22:11', NULL),
(7, 5, 'Love', 'go/to/school', 'light', '2018-12-11 23:31:43', NULL),
(8, 5, 'Fly', 'mean', 'light', '2018-12-11 23:34:31', NULL),
(9, 7, 'Swim', 'right', NULL, '2018-12-11 23:40:45', NULL),
(10, 8, 'Show', 'sam', NULL, '2018-12-11 23:52:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `blog_key` varchar(255) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `blog_title` varchar(255) NOT NULL,
  `blog_description` longtext NOT NULL,
  `blog_feature_image` varchar(255) NOT NULL,
  `blog_created_at` timestamp NULL DEFAULT NULL,
  `blog_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `blog_key`, `staff_id`, `blog_title`, `blog_description`, `blog_feature_image`, `blog_created_at`, `blog_updated_at`) VALUES
(1, 'blog_54f353c46596d1c1adf7', 1, 'Blog Title1', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'b3136b46827b8c5c285bee85c714fc97.jpg', '2019-01-15 03:18:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_comment`
--

CREATE TABLE `blog_comment` (
  `blog_comment_id` int(11) NOT NULL,
  `blog_comment_key` varchar(255) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `blog_comment_username` varchar(255) NOT NULL,
  `blog_comment_useremail` varchar(255) NOT NULL,
  `blog_comment_usermessage` varchar(255) NOT NULL,
  `blog_comment_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 for Not Approve / 1 for Approved',
  `blog_comment_created_at` timestamp NULL DEFAULT NULL,
  `blog_comment_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `blog_tag`
--

CREATE TABLE `blog_tag` (
  `blog_tag_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `blog_tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_tag`
--

INSERT INTO `blog_tag` (`blog_tag_id`, `blog_id`, `blog_tag`) VALUES
(1, 1, 'testblog1');

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL,
  `email_template_type` varchar(50) NOT NULL,
  `email_template_subject` text NOT NULL,
  `email_template` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`email_template_id`, `email_template_type`, `email_template_subject`, `email_template`) VALUES
(2, 'reset_password', 'Reset Your Password', '<table class=\"body-wrap\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\" bgcolor=\"#f6f6f6\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>\r\n		<td class=\"container\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\" width=\"600\" valign=\"top\">\r\n			<div class=\"content\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\">\r\n				<table class=\"main\" itemprop=\"action\" itemscope=\"\" itemtype=\"http://schema.org/ConfirmAction\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#fff\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">\r\n							<meta itemprop=\"name\" content=\"Confirm Email\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><table style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">\r\n\r\n<div class=\"panel-heading\"><h4 class=\"text-center\" style=\"display: block; margin-left: auto; margin-right: auto; width: 30%;\"><img src=\"http://wg.rssoft.win/church/admin_assets/dist/img/logo.png\" alt=\"\" class=\"logo-lg\" style=\"color: inherit; font-size: 1.5rem; float: none;\" height=\"55\"><br></h4></div><br><p style=\"font-family: \" noto=\"\" sans\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;\"=\"\">Dear {{username}},</p><p style=\"font-family: \" noto=\"\" sans\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;\"=\"\">We received a request to reset your access. Please click the button below to reset your password.</p></td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" itemprop=\"handler\" itemscope=\"\" itemtype=\"http://schema.org/HttpActionHandler\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">\r\n										<a href=\"{{actual_link}}\" class=\"btn-primary\" itemprop=\"url\" style=\"background-color: #264772 !important; font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #5fbeaa; margin: 0; border-color: #264772; border-style: solid; border-width: 10px 20px;\">Click Here</a>\r\n									</td>\r\n								</tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">Please disregard this email if you did not request a password reset.<br><br><br>Sincerely,&nbsp;<br noto=\"\" sans\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;\"=\"\"><span noto=\"\" sans\",=\"\" \"helvetica=\"\" neue\",=\"\" helvetica,=\"\" arial,=\"\" sans-serif;\"=\"\">Job Recruitment Team</span><br><br></td></tr></tbody></table></td></tr></tbody></table><br></div>\r\n		</td>\r\n		<td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>\r\n	</tr></tbody></table>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` bigint(20) NOT NULL,
  `event_key` varchar(255) DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `event_description` text,
  `event_location` text,
  `event_starts` timestamp NULL DEFAULT NULL,
  `event_ends` timestamp NULL DEFAULT NULL,
  `event_created_at` timestamp NULL DEFAULT NULL,
  `event_updated_at` timestamp NULL DEFAULT NULL,
  `is_event_active` tinyint(4) NOT NULL,
  `is_event_featured` int(11) NOT NULL,
  `event_order` int(11) NOT NULL,
  `event_phone` text,
  `event_website` text,
  `event_email` text,
  `event_seat` int(11) DEFAULT NULL,
  `is_event_booking_enabled` int(11) NOT NULL,
  `event_google_map_location` text,
  `event_google_map_latitude` varchar(255) DEFAULT NULL,
  `event_google_map_longitude` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_key`, `event_name`, `event_description`, `event_location`, `event_starts`, `event_ends`, `event_created_at`, `event_updated_at`, `is_event_active`, `is_event_featured`, `event_order`, `event_phone`, `event_website`, `event_email`, `event_seat`, `is_event_booking_enabled`, `event_google_map_location`, `event_google_map_latitude`, `event_google_map_longitude`) VALUES
(1, 'evt_sdfsdf779', 'Higuain Jack', 'Bacon ipsum dolor amet tri-tip ground round bresaola short ribs ribeye picanha pastrami bacon boudin sirloin chuck. Turkey chicken pork loin shankle frankfurter buffalo beef ribs sirloin burgdoggen shoulder drumstick pancetta bacon. Ham hock jowl jerky pancetta kevin swine. Landjaeger ham venison turkey, sirloin kevin flank pork rump pork belly short ribs prosciutto shankle. Ball tip pork belly capicola, shoulder tri-tip leberkas tenderloin swine frankfurter jerky doner filet mignon bresaola.\n\nCapicola turkey frankfurter sirloin meatloaf, ground round rump kevin. Tongue t-bone burgdoggen prosciutto doner tail salami drumstick ham hock fatback sausage. Turkey bresaola pancetta, pork chop rump landjaeger corned beef ground round spare ribs jowl shank ham hock cow pork loin tri-tip. Jerky filet mignon pastrami shoulder pig kevin frankfurter porchetta picanha burgdoggen turducken swine turkey. Andouille hamburger frankfurter, ham bresaola tri-tip tail burgdoggen fatback pancetta. Shankle brisket porchetta, beef ribs meatloaf picanha jowl ham hock shank ball tip leberkas fatback pig turkey tenderloin. Tri-tip turkey alcatra pork chop brisket spare ribs short loin.', 'some location, 123, city ,statee', NULL, NULL, '2018-12-11 08:28:29', NULL, 1, 0, 2, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(2, 'evt_sdfsd435435345', 'Pafre Jack', 'Bacon ipsum dolor amet tri-tip ground round bresaola short ribs ribeye picanha pastrami bacon boudin sirloin chuck. Turkey chicken pork loin shankle frankfurter buffalo beef ribs sirloin burgdoggen shoulder drumstick pancetta bacon. Ham hock jowl jerky pancetta kevin swine. Landjaeger ham venison turkey, sirloin kevin flank pork rump pork belly short ribs prosciutto shankle. Ball tip pork belly capicola, shoulder tri-tip leberkas tenderloin swine frankfurter jerky doner filet mignon bresaola.\r\n\r\nCapicola turkey frankfurter sirloin meatloaf, ground round rump kevin. Tongue t-bone burgdoggen prosciutto doner tail salami drumstick ham hock fatback sausage. Turkey bresaola pancetta, pork chop rump landjaeger corned beef ground round spare ribs jowl shank ham hock cow pork loin tri-tip. Jerky filet mignon pastrami shoulder pig kevin frankfurter porchetta picanha burgdoggen turducken swine turkey. Andouille hamburger frankfurter, ham bresaola tri-tip tail burgdoggen fatback pancetta. Shankle brisket porchetta, beef ribs meatloaf picanha jowl ham hock shank ball tip leberkas fatback pig turkey tenderloin. Tri-tip turkey alcatra pork chop brisket spare ribs short loin.', 'some location, 123, city ,statee', '2019-01-01 12:31:40', '2018-12-19 20:09:30', '2018-12-11 08:28:29', NULL, 1, 1, 6, NULL, NULL, NULL, NULL, 0, NULL, '23.8103', '90.4125'),
(3, 'evt_df4a358', 'Morphin Black', NULL, 'some location, 123, city ,statee', '2019-01-14 14:30:06', '2018-12-24 00:08:44', '2018-12-11 09:04:56', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(4, 'evt_733e84a', 'Norita Hurego', 'Tongue leberkas turkey, tail capicola ribeye buffalo ground round pastrami ball tip meatball corned beef chicken. Spare ribs shankle ground round sirloin tongue fatback leberkas turducken filet mignon burgdoggen hamburger t-bone. Swine shank cupim spare ribs turducken ball tip salami porchetta buffalo. Drumstick strip steak t-bone chicken sausage ham beef ribs ribeye filet mignon. Pork loin biltong spare ribs salami tongue buffalo pancetta chuck cow swine turducken tenderloin. Ground round beef picanha pork loin tri-tip pork chop tail prosciutto boudin doner leberkas fatback.', 'some location, 123, city ,statee', '2018-12-24 15:55:29', NULL, '2018-12-11 09:08:56', NULL, 1, 0, 3, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(5, 'evt_3dcde32', 'A Drrato-Jimandrew', 'Jowl ribeye venison chuck alcatra frankfurter biltong pancetta doner sausage. Ribeye sausage jerky, picanha chicken pig shank pork loin tenderloin chuck strip steak frankfurter bresaola porchetta buffalo. Tail prosciutto tongue venison pastrami bresaola. Pork loin tail rump pastrami biltong frankfurter, cow corned beef capicola swine short loin drumstick ham. Flank boudin doner turkey pastrami strip steak chuck beef. Tri-tip andouille buffalo turkey pastrami picanha. Chuck pancetta pig porchetta ribeye flank capicola ball tip.', 'some location, 123, city ,statee', NULL, '2018-12-31 21:19:59', '2018-12-11 09:09:31', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(6, 'evt_182c52b', 'Iscemochi Hiaji', NULL, 'some location, 123, city ,statee', '2018-12-30 17:07:10', '2019-01-10 15:13:44', '2018-12-11 23:22:11', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(7, 'evt_44031d3', 'Driro Shongkolpo', 'Bacon ipsum dolor amet beef porchetta ham hock tongue sirloin doner kevin pastrami pancetta. Short ribs kielbasa tail, boudin t-bone prosciutto pork turducken ball tip alcatra flank. Spare ribs jowl meatloaf, pork chop fatback jerky sausage ground round strip steak porchetta bacon filet mignon tenderloin. Landjaeger short loin ribeye salami jowl flank chicken sirloin frankfurter beef jerky prosciutto short ribs doner. Tri-tip ground round ham hock ribeye, short ribs beef drumstick filet mignon fatback pork alcatra. Chicken sirloin sausage shoulder, tongue rump biltong ball tip. Bresaola picanha t-bone, short loin ball tip salami tongue flank filet mignon pastrami pig tri-tip chicken.\n\nJowl ribeye venison chuck alcatra frankfurter biltong pancetta doner sausage. Ribeye sausage jerky, picanha chicken pig shank pork loin tenderloin chuck strip steak frankfurter bresaola porchetta buffalo. Tail prosciutto tongue venison pastrami bresaola. Pork loin tail rump pastrami biltong frankfurter, cow corned beef capicola swine short loin drumstick ham. Flank boudin doner turkey pastrami strip steak chuck beef. Tri-tip andouille buffalo turkey pastrami picanha. Chuck pancetta pig porchetta ribeye flank capicola ball tip.', 'some location, 123, city ,statee', '2018-12-20 07:16:37', '2018-12-30 22:17:29', '2018-12-11 23:31:43', NULL, 1, 1, 5, NULL, NULL, NULL, NULL, 0, '23.4125', '90.4125', NULL),
(8, 'evt_fa3ea75', 'Q.Tarantino', 'Bacon ipsum dolor amet beef porchetta ham hock tongue sirloin doner kevin pastrami pancetta. Short ribs kielbasa tail, boudin t-bone prosciutto pork turducken ball tip alcatra flank. Spare ribs jowl meatloaf, pork chop fatback jerky sausage ground round strip steak porchetta bacon filet mignon tenderloin. ', 'some location, 123, city ,statee', '2019-01-02 18:49:22', NULL, '2018-12-11 23:34:31', NULL, 1, 0, 4, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(10, 'evt_81ecab0', 'Amanada Jensen', 'Jowl buffalo porchetta prosciutto fatback. Ribeye sirloin spare ribs tri-tip beef ribs buffalo meatloaf pastrami cupim pork short ribs porchetta turducken biltong. Shankle jowl beef ribs brisket, rump tenderloin doner. Bacon frankfurter jerky meatloaf. Alcatra chicken fatback ham pastrami rump filet mignon.', 'some location, 123, city ,statee', '2019-01-06 19:18:44', '2019-01-11 21:02:22', '2018-12-11 23:52:21', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(11, 'evt_e7c20f6b19d7480e96c8', 'test event123', 'test des1aaa', 'test location1', '2018-12-27 11:25:00', '2019-01-28 15:25:00', '2018-12-26 05:26:04', '2018-12-29 07:55:33', 1, 1, 0, '5345345', 'www.test1event.com', 'test1event@gmail.com', 15, 1, 'Dhaka, Bangladesh', '23.810332', '90.41251809999994');

-- --------------------------------------------------------

--
-- Table structure for table `event_booking`
--

CREATE TABLE `event_booking` (
  `event_booking_id` int(11) NOT NULL,
  `event_booking_key` varchar(255) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_booking_user_name` varchar(255) NOT NULL,
  `event_booking_user_phone` varchar(255) NOT NULL,
  `event_booking_user_email` varchar(255) NOT NULL,
  `event_booking_date` timestamp NULL DEFAULT NULL,
  `event_booking_seat` int(11) NOT NULL,
  `event_booking_message` text NOT NULL,
  `event_booking_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 for Unapproved / 1 for Approved ',
  `event_booking_created_at` timestamp NULL DEFAULT NULL,
  `event_booking_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_booking`
--

INSERT INTO `event_booking` (`event_booking_id`, `event_booking_key`, `event_id`, `event_booking_user_name`, `event_booking_user_phone`, `event_booking_user_email`, `event_booking_date`, `event_booking_seat`, `event_booking_message`, `event_booking_status`, `event_booking_created_at`, `event_booking_updated_at`) VALUES
(1, 'evt_bkng_7bda4ftya64b99e96732', 11, 'M Nadeem Qazi', '08299550766', 'mnqaziii@gmail.com', '2019-01-06 14:09:46', 10, 'Developing A Spiritual Mentality On Mind', 1, '2018-12-31 21:06:06', '0000-00-00 00:00:00'),
(2, 'evt_bkng_7bda4ftyfsdf99e96765', 11, 'M Nadeem Qazi2', '082995507662', 'mnqazii2i@gmail.com', '2019-01-06 14:09:49', 30, 'Developing A Spiritual Mentality On Mind2', 0, '2018-12-31 21:06:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `event_image`
--

CREATE TABLE `event_image` (
  `event_image_id` bigint(20) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_image_name` text,
  `event_image_created_at` timestamp NULL DEFAULT NULL,
  `event_image_updated_at` timestamp NULL DEFAULT NULL,
  `is_event_image_featured` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_image`
--

INSERT INTO `event_image` (`event_image_id`, `event_id`, `event_image_name`, `event_image_created_at`, `event_image_updated_at`, `is_event_image_featured`) VALUES
(1, 2, '324234234234234234234234pi234ljh234kjh32k4jh234jh234k2ju3gh4i23hyy42jh.jpg', NULL, NULL, 1),
(2, 2, 'erwerwewerwerwer14074.jpg', NULL, NULL, 0),
(3, 1, 'fsdfsdfsdfsdfsdfsdfsdfteyryjhryyhtryt6810.jpg', NULL, NULL, 1),
(4, 1, 'sdfsdfsdfsdfsdf-8242395.jpg', NULL, NULL, 0),
(5, 3, 'treterdfgdfgdfgrtthytyjgjghjgfhj20138.jpg', NULL, NULL, 1),
(6, 4, 'terte345tetertertertert7421.jpg', NULL, NULL, 0),
(7, 6, 'terdfgdfgetetetert1643894.jpg', NULL, NULL, 1),
(8, 7, 'stock-eertert3434345rertret97512.jpg', NULL, NULL, 1),
(9, 2, NULL, NULL, NULL, 1),
(19, 11, '6faea9970b1877be6779d0ea85cf3ee5.jpg', '2018-12-29 07:55:33', NULL, 1),
(20, 11, 'fed1759a6fb4609657dc0d4a1e9c3afe.jpeg', '2018-12-29 07:55:33', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `image_id` bigint(20) NOT NULL,
  `image_key` varchar(255) DEFAULT NULL,
  `image_name` text,
  `image_description` text,
  `image_created_at` timestamp NULL DEFAULT NULL,
  `image_updated_at` timestamp NULL DEFAULT NULL,
  `is_featured_image` tinyint(4) NOT NULL,
  `image_order` int(11) NOT NULL,
  `gallery_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`image_id`, `image_key`, `image_name`, `image_description`, `image_created_at`, `image_updated_at`, `is_featured_image`, `image_order`, `gallery_category_id`) VALUES
(1, 'image_sdfsdf779', '5c0fbb7d6b34e_property21.jpg', 'Des1', '2018-12-11 08:28:29', NULL, 1, 2, 1),
(3, 'image_df4a358', 'aiopjdioasjdasoidjnasd.jpg', 'sfdsf', '2018-12-11 09:04:56', NULL, 0, 0, 1),
(4, 'image_733e84a', 'aoodsdoasidnason.jpg', 'fgdfg', '2018-12-11 09:08:56', NULL, 1, 3, 4),
(5, 'image_3dcde32', 'dasdasdjaodinjoi9878n.jpg', 'sdfsdf', '2018-12-11 09:09:31', NULL, 0, 0, 3),
(6, 'image_182c52b', 'dhj9d8uasd98ajsdasnjd9asdhyada.jpg', 'test des', '2018-12-11 23:22:11', NULL, 0, 0, 5),
(7, 'image_44031d3', 'eqw889qqiunqwijub8qw78qw.jpg', 'abcdef', '2018-12-11 23:31:43', '2019-01-16 07:15:59', 1, 5, 4),
(8, 'image_fa3ea75', '5c108fd721590_abandoned-forest-industry-34950.jpg', 'zzz', '2018-12-11 23:34:31', '2019-01-16 07:15:46', 1, 4, 1),
(9, 'image_7ecd7e6', 'hja76yd9aydh3e3edddd.jpg', 'aaaa', '2018-12-11 23:40:45', NULL, 1, 1, 4),
(10, 'image_81ecab0', 'jiaijasdoijad9ada9osdj9dhj89dhjasjdn.jpg', 'rtrtrt', '2018-12-11 23:52:21', NULL, 0, 0, 1),
(16, 'img_f2b5fb13fa77d4e132df', 'c20724c49014672bb8f58fb6594a1d79.jpg', 'Porepore', '2018-12-20 04:16:05', NULL, 1, 34, 4),
(18, 'img_00901619014e2c6009a6', 'a773c96bfb03f4a9fd8727c76735afbc.jpeg', 'dfsdfsdf', '2018-12-20 04:20:30', NULL, 0, 44, 1),
(19, 'img_1aff8c218579f134607b', '36f9551f2d752f7a25e97271a5ffe16d.jpeg', 'ppppp123', '2018-12-20 04:21:04', NULL, 0, 23, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_category`
--

CREATE TABLE `gallery_category` (
  `gallery_category_id` int(11) NOT NULL,
  `gallery_category_name` varchar(255) NOT NULL,
  `gallery_category_key` varchar(255) NOT NULL,
  `category_created_at` timestamp NULL DEFAULT NULL,
  `category_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_category`
--

INSERT INTO `gallery_category` (`gallery_category_id`, `gallery_category_name`, `gallery_category_key`, `category_created_at`, `category_updated_at`) VALUES
(1, 'Missions', 'gallery_30c0e70a00c49e683e3d', '2018-12-17 01:32:50', '2018-12-17 02:11:23'),
(3, 'Ministry', 'gallery_a3013a5cd1a3fb8a6711', '2018-12-17 02:11:33', NULL),
(4, 'Church', 'gallery_0b28c6b37b894676abe6', '2018-12-17 02:11:44', NULL),
(5, 'test22', 'gallery_1b8e6518bec9bef3a345', '2018-12-17 02:25:24', '2018-12-17 02:26:38');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `page_key` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_header_image` varchar(255) NOT NULL,
  `page_content` longtext NOT NULL,
  `page_status` int(11) DEFAULT '0',
  `page_created_at` timestamp NULL DEFAULT NULL,
  `page_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`page_id`, `page_key`, `page_slug`, `page_title`, `page_header_image`, `page_content`, `page_status`, `page_created_at`, `page_updated_at`) VALUES
(1, 'page_edfslkdlfks43567kljflkj', 'about-us', 'About Us', '4a6bc56e2610adf22ef2fd3fdb531fe7.jpg', '<p>This is About Us page content.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p> </p>\r\n', 1, '2019-01-15 20:12:14', '2019-01-28 04:28:23'),
(3, 'pg_95a674587797c039fc0e', 'term-of-use', 'Terms of Use', '9f9b35a67f6e8870a378c5ad03ad1903.jpeg', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', 1, '2019-01-19 07:15:55', '2019-01-28 04:29:42'),
(4, 'pg_ffcca5c02bdf643456a7', 'faq', 'FAQ', 'fa2ea205f874f2ba98c404c97440da60.jpeg', '<p>This is FAQ page content.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 1, '2019-01-28 04:31:36', NULL),
(5, 'pg_3714df230b7c9c202de8', 'privacy-policy', 'Privacy Policy', 'd92ed68beed99fbe5b3f92c888ec33c7.jpg', '<p>This is Privacy Policy page content.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 1, '2019-01-28 04:32:14', NULL),
(6, 'pg_0e47f41aa8cc8cee9717', 'support', 'Support', '8e463d5dae3c6704681b6146caade5cb.jpg', '<p>This is Support page content.</p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 1, '2019-01-28 04:33:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `provider_id` int(11) NOT NULL,
  `provider_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sermon`
--

CREATE TABLE `sermon` (
  `sermon_id` int(11) NOT NULL,
  `sermon_key` varchar(255) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `sermon_title` varchar(255) NOT NULL,
  `sermon_description` text NOT NULL,
  `sermon_feature_image` varchar(255) NOT NULL,
  `sermon_order` int(11) NOT NULL DEFAULT '0',
  `is_sermon_featured` int(11) NOT NULL DEFAULT '0',
  `sermon_created_at` timestamp NULL DEFAULT NULL,
  `sermon_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sermon`
--

INSERT INTO `sermon` (`sermon_id`, `sermon_key`, `staff_id`, `sermon_title`, `sermon_description`, `sermon_feature_image`, `sermon_order`, `is_sermon_featured`, `sermon_created_at`, `sermon_updated_at`) VALUES
(1, 'sermon_0c1a22a2c5ca8d1e76e2', 2, 'Test Sermon1', 'Test Sermon1 Description', 'c9e6071fbb96acfa454c48b459973a8a.jpg', 11, 0, '2019-01-15 03:16:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sermon_attachments`
--

CREATE TABLE `sermon_attachments` (
  `sermon_attachment_id` bigint(20) NOT NULL,
  `sermon_id` bigint(20) NOT NULL,
  `sermon_attachment_link` int(11) DEFAULT NULL,
  `sermon_file_type` varchar(255) NOT NULL,
  `sermon_attachment_is_file` int(11) NOT NULL,
  `sermon_attachment_created_at` timestamp NULL DEFAULT NULL,
  `sermon_attachment_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sermon_attachment_file`
--

CREATE TABLE `sermon_attachment_file` (
  `sermon_attachment_file_id` bigint(20) NOT NULL,
  `sermon_attachment_id` bigint(20) NOT NULL,
  `sermon_id` bigint(20) NOT NULL,
  `sermon_attachment_file_name` varchar(255) NOT NULL,
  `sermon_attachment_file_created_at` timestamp NULL DEFAULT NULL,
  `sermon_attachment_file_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sermon_attachment_link`
--

CREATE TABLE `sermon_attachment_link` (
  `sermon_attachment_link_id` bigint(20) NOT NULL,
  `sermon_attachment_id` bigint(20) NOT NULL,
  `sermon_id` bigint(20) NOT NULL,
  `sermon_attachment_link` varchar(255) NOT NULL,
  `sermon_attachment_link_created_at` timestamp NULL DEFAULT NULL,
  `sermon_attachment_link_updated_at` timestamp NULL DEFAULT NULL,
  `provider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `sermon_file`
--

CREATE TABLE `sermon_file` (
  `sermon_file_id` int(11) NOT NULL,
  `sermon_file_key` varchar(255) NOT NULL,
  `sermon_id` int(11) NOT NULL,
  `sermon_file` varchar(255) NOT NULL,
  `sermon_file_link_type` varchar(255) NOT NULL,
  `sermon_file_type` varchar(255) NOT NULL,
  `sermon_file_created_at` timestamp NULL DEFAULT NULL,
  `sermon_file_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sermon_file`
--

INSERT INTO `sermon_file` (`sermon_file_id`, `sermon_file_key`, `sermon_id`, `sermon_file`, `sermon_file_link_type`, `sermon_file_type`, `sermon_file_created_at`, `sermon_file_updated_at`) VALUES
(1, 'sermon_file_key_9f6ce4353657459cefbe', 1, '57d4692407852f2ae0855a6d7cbb31f9.mp4', 'local', 'video', '2019-01-15 03:16:12', NULL),
(2, 'sermon_file_key_8d5f46ada821d31c15d1', 1, 'audio.com/aa', 'global', 'audio', '2019-01-15 03:16:12', NULL),
(3, 'sermon_file_key_5b50506db2fdcbeecc4c', 1, '4a7596727b5c570dd8e20e68ae7f261b.pdf', 'local', 'file', '2019-01-15 03:16:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sermon_tag`
--

CREATE TABLE `sermon_tag` (
  `sermon_tag_id` bigint(20) NOT NULL,
  `sermon_id` int(11) NOT NULL,
  `sermon_tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sermon_tag`
--

INSERT INTO `sermon_tag` (`sermon_tag_id`, `sermon_id`, `sermon_tag`) VALUES
(1, 1, 'aa'),
(2, 1, 'bb'),
(3, 1, 'cc');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL,
  `settings_type` varchar(255) DEFAULT NULL,
  `settings_key` varchar(255) DEFAULT NULL,
  `settings_val` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settings_id`, `settings_type`, `settings_key`, `settings_val`) VALUES
(1, 'main', 'site_name', 'Churcjh'),
(2, 'main', 'site_description', 'This is the house of lord'),
(3, 'main', 'site_logo', '4e4cf9db0bfc72a5ca532ff41a77077d.jpg'),
(4, 'social', 'facebook_url', 'http://fb.me'),
(5, 'social', 'twitter_link', 'http://tw.me'),
(6, 'social', 'google_link', 'http://gl.me'),
(7, 'social', 'youtube_link', 'http://yt.me'),
(8, 'social', 'facebook_link', 'http://fb.me'),
(9, 'main', 'site_icon', '1771166e56aadd47e6cacabe9c8566a3.jpg'),
(10, 'main', 'footer_text', '<p><strong>Cpyright &copy; 2014-2019 <a href=\"http://church.loc/admin/dashboard\">Christ Church</a>.</strong> All rights reserved.</p>'),
(11, 'main', 'address', NULL),
(12, 'main', 'email', NULL),
(13, 'main', 'phone', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` bigint(20) NOT NULL,
  `staff_key` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `authcode` varchar(255) NOT NULL,
  `staff_type_id` int(11) NOT NULL,
  `staff_name` varchar(255) DEFAULT NULL,
  `staff_description` text,
  `is_staff_active` tinyint(4) NOT NULL,
  `staff_order` int(11) NOT NULL,
  `staff_created_at` timestamp NULL DEFAULT NULL,
  `staff_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_key`, `email`, `password`, `authcode`, `staff_type_id`, `staff_name`, `staff_description`, `is_staff_active`, `staff_order`, `staff_created_at`, `staff_updated_at`) VALUES
(1, 'stf_sdfsdf779', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', '', 0, 'John Doe', 'Bacon ipsum dolor amet tri-tip ground round bresaola short ribs ribeye picanha pastrami bacon boudin sirloin chuck. Turkey chicken pork loin shankle frankfurter buffalo beef ribs sirloin burgdoggen shoulder drumstick pancetta bacon. Ham hock jowl jerky pancetta kevin swine. Landjaeger ham venison turkey, sirloin kevin flank pork rump pork belly short ribs prosciutto shankle. Ball tip pork belly capicola, shoulder tri-tip leberkas tenderloin swine frankfurter jerky doner filet mignon bresaola.\r\n\r\nCapicola turkey frankfurter sirloin meatloaf, ground round rump kevin. Tongue t-bone burgdoggen prosciutto doner tail salami drumstick ham hock fatback sausage. Turkey bresaola pancetta, pork chop rump landjaeger corned beef ground round spare ribs jowl shank ham hock cow pork loin tri-tip. Jerky filet mignon pastrami shoulder pig kevin frankfurter porchetta picanha burgdoggen turducken swine turkey. Andouille hamburger frankfurter, ham bresaola tri-tip tail burgdoggen fatback pancetta. Shankle brisket porchetta, beef ribs meatloaf picanha jowl ham hock shank ball tip leberkas fatback pig turkey tenderloin. Tri-tip turkey alcatra pork chop brisket spare ribs short loin.', 1, 2, '2018-12-11 08:28:29', '2019-01-10 11:47:48'),
(2, 'stf_sdfsd435435345', '', '', '', 2, 'Pafre Jack', 'Bacon ipsum dolor amet tri-tip ground round bresaola short ribs ribeye picanha pastrami bacon boudin sirloin chuck. Turkey chicken pork loin shankle frankfurter buffalo beef ribs sirloin burgdoggen shoulder drumstick pancetta bacon. Ham hock jowl jerky pancetta kevin swine. Landjaeger ham venison turkey, sirloin kevin flank pork rump pork belly short ribs prosciutto shankle. Ball tip pork belly capicola, shoulder tri-tip leberkas tenderloin swine frankfurter jerky doner filet mignon bresaola.\r\n\r\nCapicola turkey frankfurter sirloin meatloaf, ground round rump kevin. Tongue t-bone burgdoggen prosciutto doner tail salami drumstick ham hock fatback sausage. Turkey bresaola pancetta, pork chop rump landjaeger corned beef ground round spare ribs jowl shank ham hock cow pork loin tri-tip. Jerky filet mignon pastrami shoulder pig kevin frankfurter porchetta picanha burgdoggen turducken swine turkey. Andouille hamburger frankfurter, ham bresaola tri-tip tail burgdoggen fatback pancetta. Shankle brisket porchetta, beef ribs meatloaf picanha jowl ham hock shank ball tip leberkas fatback pig turkey tenderloin. Tri-tip turkey alcatra pork chop brisket spare ribs short loin.', 1, 6, '2018-12-11 08:28:29', NULL),
(3, 'stf_df4a358', '', '', '', 2, 'Morphin Black', NULL, 0, 0, '2018-12-11 09:04:56', NULL),
(6, 'stf_182c52b', '', '', '', 2, 'Iscemochi Hiaji', NULL, 0, 0, '2018-12-11 23:22:11', NULL),
(7, 'stf_44031d3', '', '', '', 3, 'Driro Shongkolpo', 'Bacon ipsum dolor amet beef porchetta ham hock tongue sirloin doner kevin pastrami pancetta. Short ribs kielbasa tail, boudin t-bone prosciutto pork turducken ball tip alcatra flank. Spare ribs jowl meatloaf, pork chop fatback jerky sausage ground round strip steak porchetta bacon filet mignon tenderloin. Landjaeger short loin ribeye salami jowl flank chicken sirloin frankfurter beef jerky prosciutto short ribs doner. Tri-tip ground round ham hock ribeye, short ribs beef drumstick filet mignon fatback pork alcatra. Chicken sirloin sausage shoulder, tongue rump biltong ball tip. Bresaola picanha t-bone, short loin ball tip salami tongue flank filet mignon pastrami pig tri-tip chicken.\n\nJowl ribeye venison chuck alcatra frankfurter biltong pancetta doner sausage. Ribeye sausage jerky, picanha chicken pig shank pork loin tenderloin chuck strip steak frankfurter bresaola porchetta buffalo. Tail prosciutto tongue venison pastrami bresaola. Pork loin tail rump pastrami biltong frankfurter, cow corned beef capicola swine short loin drumstick ham. Flank boudin doner turkey pastrami strip steak chuck beef. Tri-tip andouille buffalo turkey pastrami picanha. Chuck pancetta pig porchetta ribeye flank capicola ball tip.', 1, 5, '2018-12-11 23:31:43', NULL),
(18, 'stf_26d0b02d0eabf04f4bd9', '', '', '', 2, 'CCC', 'ABCD des', 1, 5, '2018-12-22 02:17:51', NULL),
(19, 'stf_60b75106659055d41d94', '', '', '', 7, 'B Pastor1', 'this is test pastor1', 1, 2, '2019-01-07 02:04:13', NULL),
(20, 'stf_53a4d44348f62697fb0c', '', '', '', 7, 'B Pastor2', 'test b pastor2', 1, 3, '2019-01-07 02:09:16', NULL),
(21, 'stf_4f406c05c96c2c7ad8d1', '', '', '', 3, 'B Test1', 'abcdef', 1, 2, '2019-01-09 06:55:04', '2019-01-10 04:47:45'),
(22, 'stf_70394db521b0e96dc004', 'abcd@abcd.com', 'e10adc3949ba59abbe56e057f20f883e', '', 1, 'Name 998', 'description 123', 1, 7, '2019-01-09 07:35:00', '2019-01-09 08:48:49'),
(23, 'stf_a9f0d61ee34d1d1ac48b', 'admin2@admin.com', 'e10adc3949ba59abbe56e057f20f883e', '', 1, 'Staff2', 'test description', 1, 3, '2019-01-09 09:10:16', '2019-01-09 09:19:15'),
(24, 'stf_21df87e98131333abf6d', 'staff3@staff.com', 'e10adc3949ba59abbe56e057f20f883e', '', 2, 'Staff3', 'sdfdf', 1, 33, '2019-01-09 09:35:43', NULL),
(25, 'stf_ebadc6442848b26d3368', 'bipulsarkar7@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'a68f0f279ea5522bab5b3fb2d593a8d5', 2, 'Bipul Sarkar', 'bsbsbsbsbsbsb description', 1, 77, '2019-01-10 03:21:00', NULL),
(26, 'stf_a7fa042da4444ed18c1c', 'bstaff9@bstaff.com', 'e10adc3949ba59abbe56e057f20f883e', '', 3, 'B Staff9999', 'b staff99 des', 1, 33, '2019-01-10 04:53:44', '2019-01-10 05:03:57'),
(27, 'stf_a443563a2c97d071fa55', 'staff99@staff.com', 'e10adc3949ba59abbe56e057f20f883e', '', 3, 'Staff99', 'desss', 1, 22, '2019-01-10 06:53:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_image`
--

CREATE TABLE `staff_image` (
  `staff_image_id` bigint(20) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `staff_image_name` text,
  `staff_image_created_at` timestamp NULL DEFAULT NULL,
  `staff_image_updated_at` timestamp NULL DEFAULT NULL,
  `is_staff_image_featured` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_image`
--

INSERT INTO `staff_image` (`staff_image_id`, `staff_id`, `staff_image_name`, `staff_image_created_at`, `staff_image_updated_at`, `is_staff_image_featured`) VALUES
(1, 2, '324234234234234234234234pi234ljh234kjh32k4jh234jh234k2ju3gh4i23hyy42jh.jpg', NULL, NULL, 1),
(2, 2, 'erwerwewerwerwer14074.jpg', NULL, NULL, 0),
(5, 3, 'treterdfgdfgdfgrtthytyjgjghjgfhj20138.jpg', NULL, NULL, 1),
(6, 4, 'terte345tetertertertert7421.jpg', NULL, NULL, 0),
(7, 6, 'terdfgdfgetetetert1643894.jpg', NULL, NULL, 1),
(8, 7, 'stock-eertert3434345rertret97512.jpg', NULL, NULL, 1),
(9, 2, NULL, NULL, NULL, 1),
(10, 9, NULL, NULL, NULL, 1),
(14, 1, '64ee1824af752f83880d181bdfe95f6c.jpg', '2019-01-10 11:47:48', NULL, 1),
(15, 1, 'fsdfsdfsdfsdfsdfsdfsdfteyryjhryyhtryt6810.jpg', '2019-01-10 11:47:48', NULL, 0),
(16, 1, 'sdfsdfsdfsdfsdf-8242395.jpg', '2019-01-10 11:47:48', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `staff_social_link`
--

CREATE TABLE `staff_social_link` (
  `staff_social_link_id` bigint(20) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `staff_social_link_name` varchar(255) DEFAULT NULL,
  `staff_social_link_url` text,
  `staff_social_link_created_at` timestamp NULL DEFAULT NULL,
  `staff_social_link_updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_social_link`
--

INSERT INTO `staff_social_link` (`staff_social_link_id`, `staff_id`, `staff_social_link_name`, `staff_social_link_url`, `staff_social_link_created_at`, `staff_social_link_updated_at`) VALUES
(4, 2, 'facebook', 'facebook.com', NULL, NULL),
(5, 2, 'twitter', 'twitter.com', NULL, NULL),
(6, 2, 'youtube', 'youtube.com', NULL, NULL),
(7, 2, 'google', 'google.com', NULL, NULL),
(8, 3, 'facebook', 'facebook.com', NULL, NULL),
(9, 3, 'twitter', 'twitter.com', NULL, NULL),
(10, 4, 'facebook', 'facebook.com', NULL, NULL),
(11, 4, 'twitter', 'twitter.com', NULL, NULL),
(12, 5, 'facebook', 'facebook.com', NULL, NULL),
(13, 5, 'twitter', 'twitter.com', NULL, NULL),
(14, 5, 'youtube', 'youtube.com', NULL, NULL),
(15, 5, 'google', 'google.com', NULL, '2018-12-06 18:00:00'),
(20, 1, 'facebook', 'facebook.com', '2019-01-10 11:47:48', NULL),
(21, 1, 'twitter', 'twitter.com', '2019-01-10 11:47:48', NULL),
(22, 1, 'google', '', '2019-01-10 11:47:48', NULL),
(23, 1, 'youtube', 'youtube.com', '2019-01-10 11:47:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_type`
--

CREATE TABLE `staff_type` (
  `staff_type_id` int(11) NOT NULL,
  `staff_type_key` varchar(255) NOT NULL,
  `staff_type_name` varchar(255) NOT NULL,
  `staff_type_permission` text NOT NULL,
  `staff_type_shortname` varchar(255) NOT NULL,
  `is_staff_type_deletable` int(11) NOT NULL DEFAULT '0' COMMENT '0 for Not Deletable / 1 for Deletable',
  `staff_type_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `staff_type_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff_type`
--

INSERT INTO `staff_type` (`staff_type_id`, `staff_type_key`, `staff_type_name`, `staff_type_permission`, `staff_type_shortname`, `is_staff_type_deletable`, `staff_type_created_at`, `staff_type_updated_at`) VALUES
(0, 'staff_type_7155ea23297ad7c5c370', 'Super Admin', '{\"add_user\":1,\"view_user\":1,\"edit_user\":1,\"delete_user\":1,\"add_gallery\":1,\"view_gallery\":1,\"edit_gallery\":1,\"delete_gallery\":1,\"add_gallery_image\":1,\"view_gallery_image\":1,\"edit_gallery_image\":1,\"delete_gallery_image\":1,\"add_sermon\":1,\"view_sermon\":1,\"edit_sermon\":1,\"delete_sermon\":1,\"add_banner\":1,\"view_banner\":1,\"edit_banner\":1,\"delete_banner\":1,\"add_blog\":1,\"view_blog\":1,\"edit_blog\":1,\"delete_blog\":1,\"add_staff_type\":1,\"view_staff_type\":1,\"edit_staff_type\":1,\"delete_staff_type\":1,\"add_staff\":1,\"view_staff\":1,\"edit_staff\":1,\"delete_staff\":1,\"add_testimonial\":1,\"view_testimonial\":1,\"edit_testimonial\":1,\"delete_testimonial\":1,\"add_event\":1,\"view_event\":1,\"edit_event\":1,\"delete_event\":1,\"add_booking\":1,\"view_booking\":1,\"edit_booking\":1,\"delete_booking\":1}', 'superadmin', 0, '2019-01-15 11:49:04', '2018-12-19 03:24:22'),
(1, 'staff_type_68fc470e4af07585ffda', 'Admin', '{\"add_user\":1,\"view_user\":1,\"edit_user\":1,\"delete_user\":1,\"add_gallery\":1,\"view_gallery\":1,\"edit_gallery\":1,\"delete_gallery\":1,\"add_sermon\":1,\"view_sermon\":1,\"edit_sermon\":1,\"delete_sermon\":1,\"add_gallery_image\":0,\"view_gallery_image\":0,\"edit_gallery_image\":0,\"delete_gallery_image\":0,\"add_banner\":0,\"view_banner\":0,\"edit_banner\":0,\"delete_banner\":0,\"add_blog\":0,\"view_blog\":0,\"edit_blog\":0,\"delete_blog\":0,\"add_staff_type\":0,\"view_staff_type\":0,\"edit_staff_type\":0,\"delete_staff_type\":0,\"add_staff\":0,\"view_staff\":0,\"edit_staff\":0,\"delete_staff\":0,\"add_testimonial\":0,\"view_testimonial\":0,\"edit_testimonial\":0,\"delete_testimonial\":0,\"add_event\":0,\"view_event\":0,\"edit_event\":0,\"delete_event\":0,\"add_booking\":0,\"view_booking\":0,\"edit_booking\":0,\"delete_booking\":0}', 'admin', 0, '2019-01-16 08:13:17', '2019-01-16 07:13:17'),
(2, 'staff_type_ae0ac5302e5a65b75ccf', 'Pastor', '{\"add_sermon\":1,\"view_sermon\":1,\"edit_sermon\":1,\"delete_sermon\":1,\"add_user\":0,\"view_user\":0,\"edit_user\":0,\"delete_user\":0,\"add_gallery\":0,\"view_gallery\":0,\"edit_gallery\":0,\"delete_gallery\":0,\"add_gallery_image\":0,\"view_gallery_image\":0,\"edit_gallery_image\":0,\"delete_gallery_image\":0,\"add_banner\":0,\"view_banner\":0,\"edit_banner\":0,\"delete_banner\":0,\"add_blog\":0,\"view_blog\":0,\"edit_blog\":0,\"delete_blog\":0,\"add_staff_type\":0,\"view_staff_type\":0,\"edit_staff_type\":0,\"delete_staff_type\":0,\"add_staff\":0,\"view_staff\":0,\"edit_staff\":0,\"delete_staff\":0,\"add_testimonial\":0,\"view_testimonial\":0,\"edit_testimonial\":0,\"delete_testimonial\":0,\"add_event\":0,\"view_event\":0,\"edit_event\":0,\"delete_event\":0,\"add_booking\":0,\"view_booking\":0,\"edit_booking\":0,\"delete_booking\":0}', 'pastor', 0, '2019-01-16 08:13:43', '2019-01-16 07:13:43'),
(3, 'staff_type_998f2360a892617158aa', 'Volunteer', '{\"view_sermon\":1,\"add_user\":0,\"view_user\":0,\"edit_user\":0,\"delete_user\":0,\"add_gallery\":0,\"view_gallery\":0,\"edit_gallery\":0,\"delete_gallery\":0,\"add_gallery_image\":0,\"view_gallery_image\":0,\"edit_gallery_image\":0,\"delete_gallery_image\":0,\"add_sermon\":0,\"edit_sermon\":0,\"delete_sermon\":0,\"add_banner\":0,\"view_banner\":0,\"edit_banner\":0,\"delete_banner\":0,\"add_blog\":0,\"view_blog\":0,\"edit_blog\":0,\"delete_blog\":0,\"add_staff_type\":0,\"view_staff_type\":0,\"edit_staff_type\":0,\"delete_staff_type\":0,\"add_staff\":0,\"view_staff\":0,\"edit_staff\":0,\"delete_staff\":0,\"add_testimonial\":0,\"view_testimonial\":0,\"edit_testimonial\":0,\"delete_testimonial\":0,\"add_event\":0,\"view_event\":0,\"edit_event\":0,\"delete_event\":0,\"add_booking\":0,\"view_booking\":0,\"edit_booking\":0,\"delete_booking\":0}', 'volunteer', 0, '2019-01-16 08:13:53', '2019-01-16 07:13:53'),
(8, 'staff_type_b0b3c959150b14f92d58', 'New Role 111', '{\"add_user\":1,\"edit_user\":1,\"view_user\":0,\"delete_user\":0,\"add_gallery\":0,\"view_gallery\":0,\"edit_gallery\":0,\"delete_gallery\":0,\"add_gallery_image\":0,\"view_gallery_image\":0,\"edit_gallery_image\":0,\"delete_gallery_image\":0,\"add_sermon\":0,\"view_sermon\":0,\"edit_sermon\":0,\"delete_sermon\":0,\"add_banner\":0,\"view_banner\":0,\"edit_banner\":0,\"delete_banner\":0,\"add_blog\":0,\"view_blog\":0,\"edit_blog\":0,\"delete_blog\":0,\"add_staff_type\":0,\"view_staff_type\":0,\"edit_staff_type\":0,\"delete_staff_type\":0,\"add_staff\":0,\"view_staff\":0,\"edit_staff\":0,\"delete_staff\":0,\"add_testimonial\":0,\"view_testimonial\":0,\"edit_testimonial\":0,\"delete_testimonial\":0,\"add_event\":0,\"view_event\":0,\"edit_event\":0,\"delete_event\":0,\"add_booking\":0,\"view_booking\":0,\"edit_booking\":0,\"delete_booking\":0}', 'newrole111', 1, '2019-01-16 08:13:59', '2019-01-16 07:13:59');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_key` varchar(255) NOT NULL,
  `testimonial_user_name` varchar(255) NOT NULL,
  `testimonial_description` text NOT NULL,
  `testimonial_user_image` text,
  `testimonial_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `testimonial_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`testimonial_id`, `testimonial_key`, `testimonial_user_name`, `testimonial_description`, `testimonial_user_image`, `testimonial_created_at`, `testimonial_updated_at`) VALUES
(12, 'testimonial_7188415fb6c3f9a22e0c', 'Testimonial1', 'testimonial 1 des', 'af23864198f84ed76c5226d9fae7e1e5.jpg', '2019-01-07 08:52:52', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `user_key` varchar(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_gender` varchar(255) NOT NULL,
  `user_role` int(11) DEFAULT NULL,
  `user_image` varchar(255) NOT NULL,
  `user_status` int(11) NOT NULL COMMENT '1 for Active / 0 for Deactive',
  `user_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_key`, `user_username`, `user_email`, `user_password`, `user_phone`, `user_gender`, `user_role`, `user_image`, `user_status`, `user_created_at`, `user_updated_at`) VALUES
(1, 'user_868214cf2c79cc710628', 'testuser1', 'testuser1@user.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 2, '', 1, '2018-12-26 14:07:52', '0000-00-00 00:00:00'),
(2, 'user_6204ef69d8c34b63c8db', 'test2user', 'test2@gmail.com', '77987f2ffe1627bc1eabb68704e86cc0', '4234234', '', 3, '', 1, '2019-01-10 14:29:58', '0000-00-00 00:00:00'),
(4, 'user_44166bf309a88f50efee', 'test23', 'test2399@abc.com', 'e10adc3949ba59abbe56e057f20f883e', '4234234', '', 4, '', 1, '2019-01-10 14:30:43', '0000-00-00 00:00:00'),
(6, 'user_8f7c01df441fc4acf53b', 'abc', 'abc@abc.com', 'e10adc3949ba59abbe56e057f20f883e', '5345345', '', 3, '', 1, '2019-01-10 13:32:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL,
  `user_role_key` varchar(255) NOT NULL,
  `user_role_name` varchar(255) NOT NULL,
  `user_role_permission` text NOT NULL,
  `user_role_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_role_updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `user_role_key`, `user_role_name`, `user_role_permission`, `user_role_created_at`, `user_role_updated_at`) VALUES
(1, 'userrole_bd98314de22024b236c8', 'Test Role1', '{\"add_sermon\":1,\"edit_sermon\":1,\"view_sermon\":0,\"delete_sermon\":0}', '2018-12-26 12:15:43', '0000-00-00 00:00:00'),
(2, 'userrole_0448c7cbe210ba7f8ef6', 'Test Role2', '{\"add_sermon\":1,\"edit_sermon\":1,\"view_sermon\":0,\"delete_sermon\":0}', '2018-12-26 12:17:23', '0000-00-00 00:00:00'),
(3, 'userrole_f871a45d6a8709a3fc97', 'TestRole3', '{\"add_sermon\":1,\"edit_sermon\":1,\"view_sermon\":0,\"delete_sermon\":0}', '2018-12-26 12:15:36', '0000-00-00 00:00:00'),
(4, 'userrole_c5555f363a819718586f', 'NewRole99', '{\"add_sermon\":1,\"view_sermon\":1,\"edit_sermon\":1,\"delete_sermon\":0}', '2018-12-26 13:15:55', '2018-12-26 08:15:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `banner_anchor`
--
ALTER TABLE `banner_anchor`
  ADD PRIMARY KEY (`banner_anchor_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blog_comment`
--
ALTER TABLE `blog_comment`
  ADD PRIMARY KEY (`blog_comment_id`);

--
-- Indexes for table `blog_tag`
--
ALTER TABLE `blog_tag`
  ADD PRIMARY KEY (`blog_tag_id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_booking`
--
ALTER TABLE `event_booking`
  ADD PRIMARY KEY (`event_booking_id`);

--
-- Indexes for table `event_image`
--
ALTER TABLE `event_image`
  ADD PRIMARY KEY (`event_image_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `gallery_category`
--
ALTER TABLE `gallery_category`
  ADD PRIMARY KEY (`gallery_category_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`provider_id`);

--
-- Indexes for table `sermon`
--
ALTER TABLE `sermon`
  ADD PRIMARY KEY (`sermon_id`);

--
-- Indexes for table `sermon_attachments`
--
ALTER TABLE `sermon_attachments`
  ADD PRIMARY KEY (`sermon_attachment_id`);

--
-- Indexes for table `sermon_attachment_file`
--
ALTER TABLE `sermon_attachment_file`
  ADD PRIMARY KEY (`sermon_attachment_file_id`);

--
-- Indexes for table `sermon_attachment_link`
--
ALTER TABLE `sermon_attachment_link`
  ADD PRIMARY KEY (`sermon_attachment_link_id`);

--
-- Indexes for table `sermon_file`
--
ALTER TABLE `sermon_file`
  ADD PRIMARY KEY (`sermon_file_id`);

--
-- Indexes for table `sermon_tag`
--
ALTER TABLE `sermon_tag`
  ADD PRIMARY KEY (`sermon_tag_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settings_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `staff_image`
--
ALTER TABLE `staff_image`
  ADD PRIMARY KEY (`staff_image_id`);

--
-- Indexes for table `staff_social_link`
--
ALTER TABLE `staff_social_link`
  ADD PRIMARY KEY (`staff_social_link_id`);

--
-- Indexes for table `staff_type`
--
ALTER TABLE `staff_type`
  ADD PRIMARY KEY (`staff_type_id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `banner_anchor`
--
ALTER TABLE `banner_anchor`
  MODIFY `banner_anchor_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blog_comment`
--
ALTER TABLE `blog_comment`
  MODIFY `blog_comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_tag`
--
ALTER TABLE `blog_tag`
  MODIFY `blog_tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `event_booking`
--
ALTER TABLE `event_booking`
  MODIFY `event_booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event_image`
--
ALTER TABLE `event_image`
  MODIFY `event_image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `gallery_category`
--
ALTER TABLE `gallery_category`
  MODIFY `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `provider_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sermon`
--
ALTER TABLE `sermon`
  MODIFY `sermon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sermon_attachments`
--
ALTER TABLE `sermon_attachments`
  MODIFY `sermon_attachment_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sermon_attachment_file`
--
ALTER TABLE `sermon_attachment_file`
  MODIFY `sermon_attachment_file_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sermon_file`
--
ALTER TABLE `sermon_file`
  MODIFY `sermon_file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sermon_tag`
--
ALTER TABLE `sermon_tag`
  MODIFY `sermon_tag_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `staff_image`
--
ALTER TABLE `staff_image`
  MODIFY `staff_image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `staff_social_link`
--
ALTER TABLE `staff_social_link`
  MODIFY `staff_social_link_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `staff_type`
--
ALTER TABLE `staff_type`
  MODIFY `staff_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
